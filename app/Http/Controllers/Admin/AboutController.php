<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\About;
use App\Admin\Setting;
use Validator;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = About::Find(1);        
        return view('admin.about.form', compact('info'));
    }

    public function update(Request $request)
    {
         $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'short_description.*' => 'required',
                    'content.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } 
        $info = About::Find(1);
        $NewInfo = $request->all();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = "uploads/AboutPage/";
            $uniqueID = time();
            $file_name = $uniqueID . "." . $file->getClientOriginalExtension();
            if (file_exists($info->image)) {
                unlink($info->image);
            }
            $file->move($path, $file_name);
            $NewInfo['image'] = $path . $file_name;
        }
        $info->update($NewInfo);
        $request->session()->flash('success', 'Success!');
        return redirect('admin/about');
    }

    public function termsIndex()
    {
        $info = About::Find(2);        
        return view('admin.terms.form', compact('info'));
    }

    public function termsUpdate(Request $request)
    {
         $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'content.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } 
        $NewInfo = $request->all();

        $info = About::Find(2);
        if($info){
            $info->update($request->all());
        }
        else{
            $NewInfo['id'] = 2;
            About::create($NewInfo);
        }

        $request->session()->flash('success', 'Success!');
        return redirect('admin/terms');
    }

    public function settingsIndex()
    {
        $info = Setting::Find(1);        
        return view('admin.settings.form', compact('info'));
    }

    public function settingsUpdate(Request $request)
    {
         $validator = Validator::make($request->all(), [
                    'app_name' => 'required',
                    'app_email' => 'required',
                    'app_phone' => 'required',
                    'app_currency' => 'required',
                    'app_fees' => 'required'
                    //'image' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } 
        $info = Setting::Find(1);
        $NewInfo = $request->all();
        $NewInfo['show_cash'] = $request->show_cash == 'on' ? 1:0;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = "uploads/logo/";
            $file_name = "logo." . $file->getClientOriginalExtension();
            if (file_exists($info->image)) {
                unlink($info->image);
            }
            $file->move($path, $file_name);
            $NewInfo['image'] = $path . $file_name;
        }
        $info->update($NewInfo);
        $request->session()->flash('success', 'Success!');
        return redirect('admin/settings');
    }

}
