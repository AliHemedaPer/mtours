<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Integration;
use Validator;

class IntegrationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $integrations = Integration::all();
        return view('admin.integrations.index', compact('integrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $integration = NULL;
        return view('admin.integrations.form', compact('integration'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'image' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/integrations/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name =  $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['image'] = $path . $file_name;
        }       
        Integration::create($data);

        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/integrations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Integration $integration) {
        return view('admin.integrations.form', compact('integration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Integration $integration) {
        $validator = Validator::make($request->all(), [
                    'image' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $Newintegration = $request->all();
        $path = "uploads/integrations/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($integration->image)) {
                unlink($integration->image);
            }
            $Newintegration['image'] = $path . $file_name;
        }        
        $integration->update($Newintegration);
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/integrations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Integration $integration) {
        if (file_exists($integration->image)) {
            unlink($integration->image);
        }        
        $integration->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/integrations');
    }

}
