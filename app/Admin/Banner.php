<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title','description'];
    
     protected $fillable = ['title', 'description','url','image'];
}
