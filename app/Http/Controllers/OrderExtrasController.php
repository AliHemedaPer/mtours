<?php

namespace App\Http\Controllers;

use App\OrderExtras;
use Illuminate\Http\Request;

class OrderExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderExtras  $orderExtras
     * @return \Illuminate\Http\Response
     */
    public function show(OrderExtras $orderExtras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderExtras  $orderExtras
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderExtras $orderExtras)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderExtras  $orderExtras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderExtras $orderExtras)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderExtras  $orderExtras
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderExtras $orderExtras)
    {
        //
    }
}
