<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Transportations;
use App\Admin\TransportationPrices;
use Validator;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransportationsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $transportations = Transportations::all();

        return view('admin.transportations.index', compact('transportations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $transportation = NULL;
        $custome_prices = [ 0 => ['from' => '', 'to' => '', 'price' => '']];
        
        return view('admin.transportations.form', compact('transportation', 'custome_prices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'destination.*' => 'required',
                    'description.*' => 'required',
                    'price' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();

        $transportation = Transportations::create($data);

        //Custome Prices
        if(isset($request->cut_price) && count($request->cut_price) && $transportation->id){
            foreach ($request->cut_price as $key => $price_arr) {
                if($price_arr['from'] && $price_arr['to'] && $price_arr['price']){
                    TransportationPrices::insert([
                        'transportation_id' => $transportation->id,
                        'from'       => $price_arr['from'],
                        'to'         => $price_arr['to'],
                        'price'      => $price_arr['price']
                    ]);
                }
            }
        }
        ////////////////

        $request->session()->flash('success', 'Success!');
        return redirect('admin/transportations/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transportations  $transportation
     * @return \Illuminate\Http\Response
     */
    public function show(Transportations $transportation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transportations  $transportation
     * @return \Illuminate\Http\Response
     */
    public function edit(Transportations $transportation, Request $request)
    {
        $custome_prices = TransportationPrices::where('transportation_id', $transportation->id)->get()->toArray();
        if(!count($custome_prices)){
            $custome_prices = [ 0 => ['from' => '', 'to' => '', 'price' => '']];
        }
        return view('admin.transportations.form', compact('transportation', 'custome_prices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transportations  $transportation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transportations $transportation)
    {
        // print_r($request->all());
        // exit();
        $validator = Validator::make($request->all(), [
                    'destination.*' => 'required',
                    'description.*' => 'required',
                    'price' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        
        $transportation->update($data);

        //Custome Prices
        if(isset($request->cut_price) && count($request->cut_price) && $transportation->id){
            TransportationPrices::where('transportation_id', $transportation->id)->delete();
            foreach ($request->cut_price as $key => $price_arr) {
                if($price_arr['from'] && $price_arr['to'] && $price_arr['price']){
                    TransportationPrices::insert([
                        'transportation_id' => $transportation->id,
                        'from'       => $price_arr['from'],
                        'to'         => $price_arr['to'],
                        'price'      => $price_arr['price']
                    ]);
                }
            }
        }
        ////////////////

        $request->session()->flash('success', 'Success!');
        return redirect('admin/transportations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transportations  $transportation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transportations $transportation)
    {
        $transportation->delete();
        \Session::flash('success', 'Success!');
        return redirect('admin/transportations');
    }
}
