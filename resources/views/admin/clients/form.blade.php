@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li class="active">
        Send Mail to Clients
    </li>

</ol>
<div class="page-header">
    <h1>Send Mail to Client/s: [ {{ $client ? $client->email : 'All' }} ]</h1>

</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">

                {!! Form::open(["url"=>"admin/clients/mail","class"=>"form-horizontal","method"=>"post"]) !!}
                

                {!! Form::hidden('client_id',($client) ? $client->id : null,['class'=>'form-control' ,'placeholder'=>'']) !!}

                <div class="col-lg-12"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Subject
                        </label>
                        {!! Form::text('subject',null,['class'=>'form-control' ,'placeholder'=>'','required'=>'true']) !!}
                    </div>
                </div>

                <div class="col-lg-12"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Message
                        </label>
                        {!! Form::textarea('message',null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'content']) !!}
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-lg-12" style="text-align: right;">       
                        <a class="btn btn-danger" href="{{url("admin/clients")}}"><i class="icon-plus2 mr-2"></i>Cancel <i class="fa fa-minus-circle"></i></a>
                        <button type="submit" class="btn btn-success">Send <i class="fa fa-envelope"></i></button>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
@endsection