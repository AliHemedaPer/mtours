@extends('front.front')

@section('content')
<div class="main-cont">   
  
  <div class="inner-page">
    <div class="inner-breadcrumbs">
      <div class="content-wrapper">
        <div class="page-title">@lang('fnt.blog')</div>
        <div class="breadcrumbs">
              <!-- <a href="#">Home</a> /  <span>Blog with sidebar</span> -->
            </div>
            <div class="clear"></div>
          </div>    
    </div>
    
  </div>
  
  <div class="blog-page">
    <div class="content-wrapper">



    <div class="blog-sidebar">
      <div class="blog-sidebar-l">
          <div class="blog-sidebar-lb">
            <div class="blog-sidebar-p">
            
            <div class="blog-row">
              <!-- // -->
                <div class="blog-post single-post">
                <div class="blog-post-i">
                  <div class="blog-post-l">
                    <div class="blog-post-date">
                      <b>{{ Carbon\Carbon::parse($post->created_at)->format('d') }}</b>
                      <span>{{ Carbon\Carbon::parse($post->created_at)->format('M Y') }}</span>
                    </div>
                    <div class="blog-post-info">
                      <!-- <div>by Admin</div> -->
                      <!-- <div>posted in business</div> -->
                      <!-- <div>5 comments</div> -->
                      <!-- <div class="shareholder">
                        <span>share</span>
                        <div class="share-popup">
                          <a href="#"><img alt="" src="img/share-popup-01.png"></a>
                          <a href="#"><img alt="" src="img/share-popup-02.png"></a>
                          <a href="#"><img alt="" src="img/share-popup-03.png"></a>
                        </div>
                      </div> -->
                    </div>
                  </div>
                  <div class="blog-post-c">
                      <div class="blog-post-cb">
                        <div class="blog-post-p">
                        <div class="blog-post-title"><a href="#">{{ $post->translate($share_locale)->title }}</a></div>
                        @if($post->image)
                          <div class="blog-post-preview">
                            <div class="blog-post-img">
                              <a href="#"><img alt="" src="{{url($post->image)}}"></a>
                            </div>
                          </div>
                        @endif
                          <div class="blog-post-txt">
                            {!! $post->translate($share_locale)->description !!}
                          </div>
                        </div>
                      </div>
                      <div class="clear"></div>
                  </div>
                </div>
                <div class="clear"></div>
                </div>
              <!-- \\ -->
            
            </div>        

            
            </div>
          </div>
          <br class="clear" />
      </div>
    </div>
    <div class="blog-sidebar-r">
        
       
        
        <!-- // widget // -->
          <div class="blog-widget tweeter-widget">
            <h2>@lang('fnt.rec_posts')</h2>
            <div class="tweets-row">
              @foreach($posts as $post)
              <!-- // -->
              <div class="tweeter-item">
                <div class=""></div>
                <div class="tweeter-item-r">
                  <a href="{{ url('post/'.$post->id) }}"><span>{{ $post->translate($share_locale)->title }}</span></a>
                  <b>{{ Carbon\Carbon::parse($post->created_at)->format('d M Y') }}</b>
                </div>
                <div class="clear"></div>
              </div>
              <!-- \\ -->
              @endforeach
            </div>
          </div>
        <!-- \\ widget \\ -->
        
    </div>
    <div class="clear"></div>

    </div>
  </div>
  
</div>
@stop