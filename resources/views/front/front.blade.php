<!DOCTYPE html>
<html lang="en">
<head>
  <title> | {{ $app_settings['app_name'] }} | </title>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta charset="utf-8" /><link rel="icon" href="favicon.png" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" href='{{ asset("public/front")}}/css/jquery-ui.css'>
  <link rel="stylesheet" href='{{ asset("public/front")}}/css/idangerous.swiper.css'>
  <link rel="stylesheet" href='{{ asset("public/front")}}/css/owl.carousel.css'>
  <link rel="stylesheet" href='{{ asset("public/front")}}/css/style.css?v=1.9' />
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Raleway:300,400,500,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  @yield("style")
</head>
<body class="{{ isset($inner_body) ? 'inner-body' : '' }}">
<!-- // authorize // -->
  <div class="overlay"></div>
  <div class="autorize-popup">
    <div class="autorize-tabs">
      <a href="#" class="autorize-tab-a current">Sign in</a>
      <a href="#" class="autorize-tab-b">Register</a>
      <a href="#" class="autorize-close"></a>
      <div class="clear"></div>
    </div>
    <section class="autorize-tab-content">
      {!! Form::open(array('url' => '','class'=>'','role'=>'form' ,'id' =>'login_form' )) !!}
      <div class="autorize-padding">
        <div class="login_msg" style="padding: 0 0 10px 0; color: #f14141;display: none;"></div>

        <h6 class="autorize-lbl">Welocome! Login in to Your Accont</h6>
        <input type="text" value="" name="email" class="login_mail" placeholder="Name">
        <p style="display: none;" id="login_name_error"></p>
        <input type="password" value="" name="password" class="login_pass" placeholder="Password">
        <p style="display: none;" id="login_pwd_error"></p>
        <footer class="autorize-bottom">
          <button class="authorize-btn" type="button" onclick="login()">Login</button>
          <a href="#" class="authorize-forget-pass">Forgot your password?</a>
          <div class="clear"></div>
          <br/>
          <p id="login_wait" style="display: none;padding-left: 12px;color: #2aaeb3;">@lang('fnt.pleasewait')</p>
        </footer>
      </div>
      {!! Form::close() !!}
    </section>
    <section class="autorize-tab-content">
      <div class="autorize-padding">
        <form class="form-horizontal addPersonal form-group-sm form-control" role="form" id="user_reg">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <h6 class="autorize-lbl">Register for Your Account</h6>

        <div class="success_msg" style="padding: 0 0 10px 0; color: rgb(78, 169, 156);display: none;"></div>
        <div class="failure_msg" style="padding: 0 0 10px 0; color: #f14141;display: none;"></div>

        <input type="text" value="" placeholder="Name" name="user_name" id="user_name">
        <p style="display: none;" id="user_name_error"></p>

        <input type="text" value="" placeholder="E-mail" name="email" id="email">
        <p style="display: none;" id="user_mail_error"></p>

        <input type="password" value="" class="paswd" placeholder="Password" name="password">
        <input type="password" value="" class="paswd" placeholder="Confirm Password" name="password_confirmation">
        <p style="display: none;" id="user_pass_error"></p>

        <input type="text" value="" placeholder="Phone" name="phone" id="phone">

        <footer class="autorize-bottom">
          <button class="authorize-btn" type="button" onclick="user_register();">Sign Up</button>
          <div class="clear"></div>
          <br/>
          <p id="regis_wait" style="display: none;padding-left: 12px;color: #07b58e;">@lang('fnt.pleasewait')</p>
        </footer>
      </form>
      </div>
    </section>
  </div>
<!-- \\ authorize \\-->

<header id="top">
  <div class="header-a">
    <div class="wrapper-padding" style="max-width: 1250px;">
{{--      <div class="header-phone"><span>{{ $app_settings['app_phone'] }}</span></div>--}}
        <div class="header-phone">
            <span><a href="#"></a></span>
            <div class="phones-drop">
                <div><a href="#" class="langs-item en" >{{ $app_settings['app_phone'] }}</a></div>
            </div>
        </div>
      <!-- <div class="header-account">
        @if (auth()->check() && auth()->user()->isClient())
           <div class="header-user">
            <a href="#">Hi: {{ auth()->user()->name }}</a>
            <div class="user-drop">
              <div><a href="#" class="langs-item en">My Booking</a></div>
              <div>
                <form id="logout-form" action="{{ route('clinet.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                  </form>
                <a href="#" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="langs-item fr">logout</a></div>
            </div>
          </div>

        @else
          <a class="account" href="#">My account</a>
        @endif
      </div> -->
      <div class="header-social">
        @if( $app_settings['app_tw'] )
        <a href="{{ $app_settings['app_tw'] }}" class="social-twitter" target="_blank"></a>
        @endif

        @if( $app_settings['app_fb'] )
        <a href="{{ $app_settings['app_fb'] }}" class="social-facebook" target="_blank"></a>
        @endif

        @if( $app_settings['app_in'] )
        <a href="{{ $app_settings['app_in'] }}" class="social-instagram" target="_blank"></a>
        @endif
      </div>
      <div class="header-viewed">
        <a href="#" class="header-viewed-btn">@lang('fnt.recently')</a>
        <!-- // viewed drop // -->
          <div class="viewed-drop">
            <div class="viewed-drop-a">
              @foreach($recent_vi as $tour)
              <!-- // -->
              <div class="viewed-item">
                <div class="viewed-item-l">
                  <a href="{{ url('tour/'.$tour->id)}}"><img alt="" src="{{ url('/').'/'.$tour->image }}" style="max-width: 80px;cursor: pointer;" /></a>
                </div>
                <div class="viewed-item-r">
                  <div class="viewed-item-lbl"><a href="{{ url('tour/'.$tour->id)}}">{{ $tour->translate($share_locale)->title }}</a></div>
                   @if($tour->categories)
                  <div class="viewed-item-cat">@lang('fnt.location'): <a href="{{ url('tours/?c='.$tour->cat_id)}}">
                      {{ $tour->categories->translate($share_locale)->title }}
                    </a></div>
                  @endif
                  <div class="offer-slider-location">@lang('fnt.views'):
                    {{ $tour->views }}
                  </div>
                  <div class="viewed-price">{{ $tour->displayPrice() }} {{ $app_settings['app_currency'] }}</div>
                </div>
                <div class="clear"></div>
              </div>
              <!-- \\ -->
              @endforeach
            </div>
          </div>
        <!-- \\ viewed drop \\ -->
      </div>
      <div class="header-lang">
        <a href="#"><img alt="" src="{{ asset('public/front')}}/img/{{ $share_locale }}.png" /></a>
        <div class="langs-drop">
          <div><a href="#" class="langs-item en" onclick="changeLang('en')">English</a></div>
          <div><a href="#" class="langs-item sp" onclick="changeLang('sp')">Spanish</a></div>
        </div>
      </div>
      <!-- <div class="header-curency">
        <a href="#">USD</a>
        <div class="curency-drop">
          <div><a href="#">usd</a></div>
          <div><a href="#">Eur</a></div>
          <div><a href="#">GBR</a></div>
        </div>
      </div> -->
      <div class="clear"></div>
    </div>
  </div>
  <div class="header-b">
    <!-- // mobile menu // -->
      <div class="mobile-menu">
        <nav>
          <ul>
            <li><a href="{{ url('/')}}">@lang('fnt.home')</a></li>
            <li><a href="{{ url('/about')}}">@lang('fnt.about')</a></li>
            @foreach($share_categories as $category)
                @if($category->parent_id == 0)
                <li><a class="has-child" href="{{url('tours?p='.$category->id)}}">{{ $category->translate($share_locale)->title }}</a>
                  @php
                      $subs = 0
                  @endphp
                  @foreach($share_categories as $subcat)
                        @if($subcat->parent_id == $category->id)
                          {!! $subs == 0 ? '<ul>' : '' !!}
                            @if($subcat->full_half)
                              <li><a href="{{url('tours?typ=full')}}">@lang('fnt.full')</a>
                              </li>
                              <li><a href="{{url('tours?typ=half')}}">@lang('fnt.half')</a>
                              </li>
                            @else
                              <li><a href="{{url('tours?c='.$subcat->id)}}">{{ $subcat->translate($share_locale)->title }}</a>
                              </li>
                            @endif
                          @php
                            $subs = $subs+1
                          @endphp

                        @endif
                  @endforeach
                  @if($subs > 0)
                    </ul>
                  @endif
                </li>
                @endif
              @endforeach
              <li><a href="{{ url('tours?h_offer=1')}}">@lang('fnt.hotoffers')</a></li>
            <li><a href="{{ url('airport-transportations')}}">@lang('fnt.transportations')</a></li>
            <li><a href="{{url('/blog')}}">@lang('fnt.blog')</a></li>
            <li><a href="{{url('/contact')}}">@lang('fnt.contact')</a></li>
          </ul>
        </nav>
      </div>
    <!-- \\ mobile menu \\ -->

    <div class="wrapper-padding"  style="max-width: 1250px;">
      <div class="header-logo"><a href="{{ url('/')}}">
        <input type="hidden" id="hidden_logo_1" value="{{ url($app_settings['image'].'?t='.time()) }}">
        <input type="hidden" id="hidden_logo_2" value="{{ url($app_settings['image'].'?t='.time()) }}">
        <img alt="" src="{{ url($app_settings['image'].'?t='.time()) }}" /></a></div>
      <div class="header-right">
        <!-- <div class="hdr-srch">
          <a href="#" class="hdr-srch-btn"></a>
        </div> -->
        <!-- <div class="hdr-srch-overlay">
          <form method="get" action="{{ url('/serach') }}">
            <div class="hdr-srch-overlay-a">
              <input type="text" name="key" value="" placeholder="Start typing...">
              <a href="#" class="srch-close"></a>
              <div class="clear"></div>
            </div>
          </form>
        </div>
        <div class="hdr-srch-devider"></div>-->
        <a href="#" class="menu-btn"></a>
        <nav class="header-nav" style="margin-right: 25px;">
          <ul>
            <li><a href="{{ url('/')}}">@lang('fnt.home')</a></li>
            <li><a href="{{ url('/about')}}">@lang('fnt.about')</a></li>
            @foreach($share_categories as $category)
                @if($category->parent_id == 0)
                <li><a href="{{url('tours?p='.$category->id)}}">{{ $category->translate($share_locale)->title }}</a>
                  @php
                      $subs = 0
                  @endphp
                  @foreach($share_categories as $subcat)
                        @if($subcat->parent_id == $category->id)

                          {!! $subs == 0 ? '<ul>' : '' !!}

                            @if($subcat->full_half)
                              <li><a href="{{url('tours?typ=full')}}">@lang('fnt.full')</a>
                              </li>
                              <li><a href="{{url('tours?typ=half')}}">@lang('fnt.half')</a>
                              </li>
                            @else
                              <li><a href="{{url('tours?c='.$subcat->id)}}">{{ $subcat->translate($share_locale)->title }}</a>
                              </li>
                            @endif
                          @php
                            $subs = $subs+1
                          @endphp

                        @endif
                    @endforeach
                    @if($subs > 0)
                      </ul>
                    @endif
                </li>
                @endif
              @endforeach
              <li><a href="{{ url('tours?h_offer=1')}}">@lang('fnt.hotoffers')</a></li>
            <li><a href="{{ url('airport-transportations')}}">@lang('fnt.transportations')</a></li>
            <li><a href="{{url('/blog')}}">@lang('fnt.blog')</a></li>
            <li><a href="{{url('/contact')}}">@lang('fnt.contact')</a></li>
          </ul>
        </nav>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</header>
<!-- main-cont -->
@yield("content")
<!--/ main-cont -->
<footer class="footer-a">
  <div class="wrapper-padding">
    <div class="section">
      <div class="footer-lbl">@lang('fnt.get_in_tch')</div>
      <div class="footer-adress">{{ $app_settings['app_address'] ?? ''}}</div>
      <div class="footer-phones">{{ $app_settings['app_phone'] }}</div>
      <div class="footer-email">{{ $app_settings['app_email'] }}</div>
      <!-- <div class="footer-skype">Skype: angelotours</div> -->
    </div>

    <div class="section">
      <div class="footer-lbl">@lang('fnt.quick_lk')</div>
      <!-- // -->
      <div><a href="{{ url('/')}}">@lang('fnt.home')</a></div>
      <!-- \\ -->
      <!-- // -->
      <div><a href="{{ url('/about')}}">@lang('fnt.about')</a></div>
      <!-- \\ -->
      <!-- // -->
      <div><a href="{{url('/contact')}}">@lang('fnt.contact')</a></div>
      <!-- \\ -->
    </div>
    <div class="section">
      <div class="footer-lbl">&nbsp;</div>
      <!-- // -->
      <div><a href="{{ url('tours?h_offer=1')}}">@lang('fnt.hotoffers')</a></div>
      <!-- \\ -->
      <!-- // -->
      <div><a href="{{url('/blog')}}">@lang('fnt.blog')</a></div>
      <!-- \\ -->
      <div><a href="{{url('/terms')}}">@lang('fnt.terms')</a></div>
    </div>
    <div class="section">
      <div class="footer-lbl">@lang('fnt.contact')</div>
      <div class="footer-social" style="float: left;">
        @if( $app_settings['app_tw'] )
        <a href="{{ $app_settings['app_tw'] }}" class="footer-twitter" target="_blank"></a>
        @endif

        @if( $app_settings['app_fb'] )
        <a href="{{ $app_settings['app_fb'] }}" class="footer-facebook" target="_blank"></a>
        @endif

        @if( $app_settings['app_in'] )
        <a href="{{ $app_settings['app_in'] }}" class="footer-instagram" target="_blank"></a>
        @endif
      </div>
    </div>
  </div>
  <div class="clear"></div>
</footer>

<footer class="footer-b">
  <div class="wrapper-padding">
    <div class="footer-left">@lang('fnt.rights')</div>
    <!-- <div class="footer-social">
      <a href="#" class="footer-twitter"></a>
      <a href="#" class="footer-facebook"></a>
      <a href="#" class="footer-instagram"></a>
    </div> -->
    <div class="clear"></div>
  </div>
</footer>

<!-- // scripts // -->
  <script src='{{ asset("public/front")}}/js/jquery.min.js'></script>
  <script src='{{ asset("public/front")}}/js/jqeury.appear.js'></script>
  <script src='{{ asset("public/front")}}/js/jquery-ui.min.js'></script>
  <script src='{{ asset("public/front")}}/js/owl.carousel.min.js'></script>
  <script src='{{ asset("public/front")}}/js/bxSlider.js'></script>

  <script src='{{ asset("public/front")}}/js/custom.select.js'></script>
  <script src='{{ asset("public/front")}}/js/idangerous.swiper.js'></script>
  <!-- <script type="text/javascript" src='{{ asset("public/front")}}/js/twitterfeed.js'></script> -->
  <script src='{{ asset("public/front")}}/js/script.js?v2'></script>
<!-- \\ scripts \\ -->

@if($app_settings['app_wats'])
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "{{ $app_settings['app_wats'] }}", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "left", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
@endif
<!-- /WhatsHelp.io widget -->

<script type="text/javascript">

function changeLang(lang){
  $.ajax({
    url: "{{url('/')}}/locale/"+lang,
    type: 'GET',
    success: function (data) {
       location.reload();
      }
    });
}

$( document ).ready(function() {
    $('#cat_id').change(function() {
        getSubcats($(this).val(), false);
    });
});

@if(isset(request()->query()['p']))
  getSubcats({{ request()->query()['p'] }}, true)
@endif

function getSubcats(catid, setSubCat){
    var selected_c = '';
    @if(isset(request()->query()['c']))
      selected_c = {{ request()->query()['c'] }};
    @endif


    let url = '{{ url('') }}' + '/subcats/' + catid;
    $('#subcat_id').html('<option value="">--</option>');
    //$('#loader').fadeIn();
    $.get(url, function(data) {
        $.each(data,function(key, value) {
            let selected = '';

            if(setSubCat){
              if( value.id == selected_c){
                selected = 'selected';
                $('#subcategory_trs .customSelectInner').text(value.value);
              }
            }else{
                $('#subcategory_trs .customSelectInner').text('--');
            }

            $('#subcat_id').append('<option value="' + value.id + '"'+ selected +' >' + value.value + '</option>');
        });
    });
}

function user_register()
{
    var pass=$('.paswd').val();
    var email=$('#email').val();
    var user_name=$('#user_name').val();

     if(user_name=="")
      {
       $("#user_name_error").css({"display":"block", "color":"red"});
       $("#user_name_error").html('Please Enter your Name!');
       return false;
      }
    if(email=="")
      {
       $("#user_name_error").hide();
       $("#user_mail_error").css({"display":"block", "color":"red"});
       $("#user_mail_error").html('Please Enter your Email!');
       return false;
      }

    if(pass.length<=0)
      {
       $("#user_name_error").hide();
       $("#user_mail_error").hide();
       $("#user_type_error").hide();
       $("#user_pass_error").css({"display":"block", "color":"red"});
       $("#user_pass_error").html('Please Enter a password!');
       return false;
      }

      $('#regis_wait').fadeIn(100);

      $.ajax({
        type: "POST",
        url: "{{url('create-user' )}}",
        data: $( "#user_reg" ).serialize(),
        datatype: 'json',
        statusCode:{
            500:function(data){
                $(".failure_msg").css({"display":"block"});
                $(".failure_msg").html('Oops ! Some Technical Failure has occured');
                $(".failure_msg").delay(10000).fadeOut("slow");
                $('#regis_wait').fadeOut(100);
            },
            401:function(data){
                $(".failure_msg").css({"display":"block"});
                $(".failure_msg").html('Sorry ! Invalid request made to server');
                $(".failure_msg").delay(10000).fadeOut("slow");
                $('#regis_wait').fadeOut(100);
            },
            422:function(data){
                $(".failure_msg").css({"display":"block"});
                $(".failure_msg").html(data.responseJSON.msg);
                $(".failure_msg").delay(10000).fadeOut("slow");
                $('#regis_wait').fadeOut(100);
            }
        },
        success: function (data) {

                $(".success_msg").css({"display":"block"});
                $(".success_msg").html('Successfully registered...<br> Please check your mail for Activation link.');
                $('#regis_wait').fadeOut(100);
        }
      });
}

/**
 * Login Functionality
 */
function login()
{
    $("#login_name_error").hide();
    $("#login_pwd_error").hide();
    var activate_form = "";
        var uname=$(".login_mail").val();
        var pwd=$('.login_pass').val();

        if(uname=="")
        {
            $("#login_name_error").css({"display":"block", "color":"red"});
            $("#login_name_error").html('Please enter user name');
            return false;
        }
        if(pwd=="")
        {
            $("#login_name_error").hide();
            $("#login_pwd_error").css({"display":"block", "color":"red"});
            $("#login_pwd_error").html('Please enter password');
            return false;
        }else{
            $("#login_name_error").hide();
            $("#login_pwd_error").hide();
        }

        $('#login_wait').fadeIn(100);

    $.ajax({
            type: "POST",
            url: "{{url('user-login')}}",
            data: $( "#login_form" ).serialize(),
            datatype: 'json',
            complete:function(data){

            },
            success: function (data) {
                $('#login_wait').fadeOut(100);
                var status=data.status;

                if(status=='failure')
                {
                $(".login_msg").html('Invalid username or password');
                $(".login_msg").css({"display":"block"});
                $(".login_msg").delay(5000).fadeOut("slow");
                }

                if(status=='success')
                {
                    location.reload();
                }

                if(status == 'notactive'){
                  $(".login_msg").html('Activate your Account or Contact support team!');
                  $(".login_msg").fadeIn();
                }

            }
          });

}
</script>
 @yield("javascript")
</body>
</html>