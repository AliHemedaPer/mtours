@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li class="active">
        <i class="clip-home-3"></i>
            Home
    </li>
</ol>
<div class="page-header">
    
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-4">
		<div class="core-box">
			<div class="heading">
				<i class="clip-tree-2 circle-icon circle-bricky"  style="    background-color: #007aff;
border-color: #155faf;"></i>
				<h2>Total Tours: <span style="color: #007aff;">{{ $tours ?? 0 }}</span></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="core-box">
			<div class="heading">
				<i class="clip-cart circle-icon circle-teal" style="background-color: #fb6830;"></i>
				<h2>Total Reservations: <span style="color: #fb6830;">{{ $resrv ?? 0 }}</span></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="core-box">
			<div class="heading">
				<i class="clip-banknote circle-icon circle-green"></i>
				<h2>Total Profit: <span style="color: #3d9400;">{{ number_format($profit, 2) }} $</span></h2>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="clip-users-2"></i>
				Recent Reservations
			</div>
			<div class="panel-body " style="height:auto;">
				<table class="table" id="sample-table-1">
					<thead>
						<tr>
							<th >#</th>    
							<th >Name</th>
                            <th >Hotel</th>   
                            <th >Date</th>   
                            <th >Type</th>         
                            <th >Tour/Airport Transportation</th>   
                            <th >Guide</th>  
                            <th class="text-right">Total</th>          
                            <th class="text-center" style="width: 150px">View</th>
						</tr>
					</thead>
					<tbody>
						@foreach($reservations as $reservation)
                        <tr @if(!$reservation->is_read) style="background-color: #b9cbd4" @endif>     
                        	<td>{{$reservation->id}}</td>                
                            <td> 
                            	@if($reservation->booking_type == 'tour')
                            	<a href="{{url('admin/reservations/tours/'.$reservation->id)}}" class="tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank">{{$reservation->name}}</a>
                            	@else
                            	<a href="{{url('admin/reservations/transportations/'.$reservation->id)}}" class="tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank">{{$reservation->name}}</a>
                            	@endif
                            </td>      
                            <td> {{$reservation->hotel ?? ''}}</td>       
                            <td> 
                            	@if($reservation->booking_type == 'tour' && $reservation->date_to)
                            		From {{ Carbon\Carbon::parse($reservation->date_from)->format('d M Y') }} To {{ Carbon\Carbon::parse($reservation->date_to)->format('d M Y') }}
                            	@else
                            		{{ Carbon\Carbon::parse($reservation->date_from)->format('d M Y') }}
                            	@endif
                            </td>
                            <td> {{$reservation->booking_type == 'tour' ? 'Tour' : 'Airport Transportation'}}</td>           
                            <td> 
                            	@if($reservation->booking_type == 'tour')
                            	<a href="{{url('tour/'.$reservation->product_id)}}" class="tooltips" data-placement="top" data-original-title="View main Tour" target="_blank">{{$reservation->products->translate('en')->title }}</a>
                            	@else
                            	{{$reservation->transportations->translate('en')->destination }}
                            	@endif
                            </td>  
                            <td> {{$reservation->guide}}</td>       
                            <td style="text-align: right;"> {{ number_format($reservation->total_price,2) }}$</td>                            
                            <td class="center">
                                <div class="visible-md visible-lg hidden-sm hidden-xs">
                                	@if($reservation->booking_type == 'tour')
                                    <a href="{{url('admin/reservations/tours/'.$reservation->id)}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank"><i class="fa fa-eye"></i></a>
                                    @else
	                            	<a href="{{url('admin/reservations/transportations/'.$reservation->id)}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank"><i class="fa fa-eye"></i></a>
	                            	@endif
                            </div>
                            </td>                          
                        </tr>
                        @endforeach 
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop