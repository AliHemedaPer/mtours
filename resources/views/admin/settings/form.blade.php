@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li class="active">
        About {{ config('app.name', 'MTours') }}
    </li>

</ol>
<div class="page-header">
    <h1>Settings</h1>

</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">

                {!! Form::open(["url"=>"admin/settings","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
                <div class="col-lg-6"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            App Name
                        </label>
                        {!! Form::text('app_name',($info)?$info->app_name:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-6"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            App Email
                        </label>
                        {!! Form::text('app_email',($info)?$info->app_email:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-6"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            App Phone
                        </label>
                        {!! Form::text('app_phone',($info)?$info->app_phone:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-6"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            WhatsApp Number
                        </label>
                        {!! Form::text('app_wats',($info)?$info->app_wats:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-6"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Address
                        </label>
                        {!! Form::text('app_address',($info)?$info->app_address:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-3"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Currency Symbole
                        </label>
                        {!! Form::text('app_currency',($info)?$info->app_currency:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-3"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Fees Percentage
                        </label>
                        {!! Form::number('app_fees',($info)?$info->app_fees:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div style="clear: both;"></div>
                <hr/>
                <div class="col-lg-4"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Facebook Page
                        </label>
                        {!! Form::text('app_fb',($info)?$info->app_fb:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-4"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Twitter
                        </label>
                        {!! Form::text('app_tw',($info)?$info->app_tw:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-4"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Instagram
                        </label>
                        {!! Form::text('app_in',($info)?$info->app_in:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="col-lg-8"> 
                        <div class="form-group">
                        <label for="form-field-select-3">
                            Fawaterak VendorKey
                        </label>
                        {!! Form::text('fawaterak_key',($info)?$info->fawaterak_key:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-4"> 
                    <div class="form-group" style="text-align: center;padding-top: 25px">
                        <label for="form-field-select-3">
                            Show Pay Cash on Arrival?
                        </label>
                        @if($info->show_cash)
                            <input type="checkbox" name="show_cash" checked="">
                        @else
                            <input type="checkbox" name="show_cash">
                        @endif
                    </div>
                </div>
                <div style="clear: both;"></div>
                <hr>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>
                            Logo (230 X 65)
                        </label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                @if($info && file_exists($info->image))
                                <img src="{{url($info->image.'?t='.time())}}" alt=""/>
                                @else
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
                                @endif
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                    <input type="file" name="image">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>                        
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-lg-12" style="text-align: right;">       
                        <a class="btn btn-danger" href="{{url("admin/dashboard")}}"><i class="icon-plus2 mr-2"></i>Cancel <i class="fa fa-minus-circle"></i></a>
                        <button type="submit" class="btn btn-success">Save <i class="fa fa-save"></i></button>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
@endsection