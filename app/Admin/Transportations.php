<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Transportations extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['destination', 'description'];
    protected $fillable = [ 
                            'destination', 
                            'description', 
                            'price'
                        ];
    protected $table = 'transportations';

    public function transportationPrices()
    {
        return $this->hasMany(TransportationPrices::class, 'transportation_id', 'id')->orderBy('price', 'ASC');
    }
}
