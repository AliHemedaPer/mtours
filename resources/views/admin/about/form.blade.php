@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li class="active">
        About {{ config('app.name', 'MTours') }}
    </li>

</ol>
<div class="page-header">
    <h1>About {{ config('app.name', 'MTours') }}</h1>

</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">

                {!! Form::open(["url"=>"admin/about","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
                <div class="tabbable">
                    <ul id="myTab" class="nav nav-tabs tab-bricky">
                        <li class="active">
                            <a href="#panel_tab1_example1" data-toggle="tab">
                                Title (EN)
                            </a>
                        </li>
                        <li class="">
                            <a href="#panel_tab2_example1" data-toggle="tab">
                                Title (SP)
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="panel_tab1_example1">
                            {!! Form::text('title[en]',($info)?$info->translate('en')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
                        </div>
                        <div class="tab-pane " id="panel_tab2_example1">
                            {!! Form::text('title[sp]',($info)?$info->translate('sp')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
                        </div>      
                    </div>
                </div>
                <div class="tabbable">
                    <ul id="myTab1" class="nav nav-tabs tab-bricky">
                        <li class="active">
                            <a href="#panel_tab1_example2" data-toggle="tab">
                                Home Text (EN)
                            </a>
                        </li>
                        <li class="">
                            <a href="#panel_tab2_example2" data-toggle="tab">
                                Home Text (SP)
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="panel_tab1_example2">
                            {!! Form::textarea('short_description[en]',($info)?$info->translate('en')->short_description:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
                        </div>
                        <div class="tab-pane " id="panel_tab2_example2">
                            {!! Form::textarea('short_description[sp]',($info)?$info->translate('sp')->short_description:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
                        </div>      
                    </div>
                </div>
                <div class="tabbable">
                    <ul id="myTab1" class="nav nav-tabs tab-bricky">
                        <li class="active">
                            <a href="#panel_tab1_example3" data-toggle="tab">
                                Details (EN)
                            </a>
                        </li>
                        <li class="">
                            <a href="#panel_tab2_example3" data-toggle="tab">
                                Details (SP)
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="panel_tab1_example3">
                            {!! Form::textarea('content[en]',($info)?$info->translate('en')->content:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'content']) !!}
                        </div>
                        <div class="tab-pane " id="panel_tab2_example3">
                            {!! Form::textarea('content[ar]',($info)?$info->translate('ar')->content:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'content']) !!}
                        </div>      
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>
                            Home Image
                        </label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                @if($info && file_exists($info->image))
                                <img src="{{url($info->image)}}" alt=""/>
                                @else
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
                                @endif
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                    <input type="file" name="image">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-lg-12" style="text-align: right;">       
                        <a class="btn btn-danger" href="{{url("admin/dashboard")}}"><i class="icon-plus2 mr-2"></i>Cancel <i class="fa fa-minus-circle"></i></a>
                        <button type="submit" class="btn btn-success">Save <i class="fa fa-save"></i></button>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
@endsection