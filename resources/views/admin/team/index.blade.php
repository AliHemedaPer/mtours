@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            الرئيسية
        </a>
    </li>
    <li class="active">
        فريق العمل
    </li>

</ol>
<div class="page-header">
    <h1 class="col-md-6">فريق العمل</h1>
    <div class="col-md-6">
        <a class="btn btn-success pull-left" href="{{url("admin/team/create")}}"><i class="icon-plus2 mr-2"></i> إضافة عضو <i class="fa fa-plus"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">

                    <thead>
                        <tr>            
                            <th class="text-right">الإسم</th>  
                            <th class="text-right">الوظيفه</th>  
                            <th class="text-center" style="width: 150px"></th>   
                            <th class="text-center" style="width: 150px">الخيارات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($team as $mmbr)
                        <tr>                  
                            <td> {{ $mmbr->translate('ar')->name }}</td>    
                            <td> {{ $mmbr->translate('ar')->position }}</td>
                            <td class="text-center"> 
                                @if($mmbr && file_exists($mmbr->image))
                                    <img src="{{url($mmbr->image)}}" alt="" width="100" />
                                @else
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="" width="100"/>
                                @endif
                            </td>
                            <td class="center">
                                <div class="visible-md visible-lg hidden-sm hidden-xs">
                                    <a href="{{url('admin/team/'.$mmbr->id.'/edit')}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                    {{ Form::open(array('url' => 'admin/team/' . $mmbr->id, 'style' => 'display: inline-block;')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <button type="submit" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove" onclick="return confirm('are you sure?')" ><i class="fa fa-times fa fa-white"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </td>                           
                    </tr>
                    @endforeach         
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="{{url('public/admin/')}}/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
jQuery(document).ready(function () {
    TableData.init();
});
</script>
@endsection