@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li class="active">
        Reports
    </li>
</ol>
<div class="page-header">
    <h1 class="col-md-6">Reports</h1>
    
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- start: PAGE CONTENT -->
<div class="row">
    <form method="GET" >
        <div class="row" style="padding:0 20px">

        	<div class="col-lg-3"> 
			        <div class="form-group">
			        <label for="form-field-select-3">
			            Day
			        </label>
			        {!! Form::select('day',$days , $day, ['class' => 'form-control border-right-0 form-control-select2']) !!}
			    </div>
			</div>
			<div class="col-lg-3"> 
			        <div class="form-group">
			        <label for="form-field-select-3">
			            Month
			        </label>
			        {!! Form::select('month',$monthes , $month, ['class' => 'form-control border-right-0 form-control-select2']) !!}
			    </div>
			</div>
			<div class="col-lg-4"> 
			        <div class="form-group">
			        <label for="form-field-select-3">
			            Year
			        </label>
			        {!! Form::select('year',$years , $year, ['class' => 'form-control border-right-0 form-control-select2']) !!}
			    </div>
			</div>
			<div class="col-lg-2" style="    padding-top: 23px;"> 
	            <div class="input-group">
	                <span class="input-group-append">
	                    <button class="btn btn-blue" type="submit"><i class="fa fa-search"></i> Show</button>
	                </span>
	            </div>
	        </div>
        </div>
    </form>
  </div>      
  <hr/>
<div class="row">
	<div class="col-sm-6">
		<div class="core-box">
			<div class="heading">
				<i class="clip-cart circle-icon circle-teal" style="background-color: #fb6830;"></i>
				<h2>Reservations: <span style="color: #fb6830;">{{ $resrv ?? 0 }}</span></h2>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="core-box">
			<div class="heading">
				<i class="clip-banknote circle-icon circle-green"></i>
				<h2>Profits: <span style="color: #3d9400;">{{ number_format($profit, 2, '.', '') }} $</span></h2>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="clip-users-2"></i>
				Reservations
			</div>
			<div class="panel-body " style="height:auto;">
				<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">

                    <thead>
                        <tr>      
                        	<th >#</th>      
                            <th >Name</th>
                            <th >Hotel</th>   
                            <th >Date</th>       
                            <th >Type</th>         
                            <th >Tour/Airport Transportation</th>  
                            <th >Guide</th>  
                            <th class="text-right">Total</th>   
                            <th class="text-right">Status</th>          
                            <th class="text-center" style="width: 150px">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations as $reservation)
                        <tr>    
                            <td>{{$reservation->id}}</td>                 
                            <td> 
                                @if($reservation->booking_type == 'tour')
                                <a href="{{url('admin/reservations/tours/'.$reservation->id)}}" class="tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank">{{$reservation->name}}</a>
                                @else
                                <a href="{{url('admin/reservations/transportations/'.$reservation->id)}}" class="tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank">{{$reservation->name}}</a>
                                @endif
                            </td>      
                            <td> {{$reservation->hotel ?? ''}}</td>       
                            <td> 
                                @if($reservation->booking_type == 'tour' && $reservation->date_to)
                                    From {{ Carbon\Carbon::parse($reservation->date_from)->format('d M Y') }} To {{ Carbon\Carbon::parse($reservation->date_to)->format('d M Y') }}
                                @else
                                    {{ Carbon\Carbon::parse($reservation->date_from)->format('d M Y') }}
                                @endif
                            </td>
                            <td> {{$reservation->booking_type == 'tour' ? 'Tour' : 'Airport Transportation'}}</td>        
                            <td> 
                                @if($reservation->booking_type == 'tour')
                                <a href="{{url('tour/'.$reservation->product_id)}}" class="tooltips" data-placement="top" data-original-title="View main Tour" target="_blank">{{$reservation->products->translate('en')->title }}</a>
                                @else
                                {{$reservation->transportations->translate('en')->destination }}
                                @endif
                            </td>  
                            <td> {{$reservation->guide}}</td>       
                            <td style="text-align: right;"> {{ number_format($reservation->total_price,2) }}$</td>
                            <td style="text-align: center;"> 
                            	@if($reservation->order_done == 1)
                            	<span class="label label-success">Done</span>
                            	@else
                            	<span class="label label-danger">Pending</span>
                            	@endif

                            </td>                        
                            <td class="center">
                                <div class="visible-md visible-lg hidden-sm hidden-xs">
                                    @if($reservation->booking_type == 'tour')
                                    <a href="{{url('admin/reservations/tours/'.$reservation->id)}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank"><i class="fa fa-eye"></i></a>
                                    @else
                                    <a href="{{url('admin/reservations/transportations/'.$reservation->id)}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank"><i class="fa fa-eye"></i></a>
                                    @endif
                            </div>
                            </td>                          
                        </tr>
                        @endforeach         
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>
@stop
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="{{url('public/admin/')}}/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
jQuery(document).ready(function () {

    TableData.init();
});
</script>
@endsection