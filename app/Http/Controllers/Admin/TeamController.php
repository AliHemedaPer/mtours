<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Team;
use Validator;
use DB;

class TeamController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $team = Team::all();
        return view('admin.team.index', compact('team'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $team = NULL;
        return view('admin.team.form', compact('team'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/team/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['image'] = $path . $file_name;
        }
        Team::create($data);

        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team) {
        return view('admin.team.form', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team) {
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/team/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($team->image)) {
                unlink($team->image);
            }
            $data['image'] = $path . $file_name;
        }
        $team->update($data);
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team) {
        if (file_exists($team->image)) {
            unlink($team->image);
        }
        $team->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/team');
    }

}
