
{!! $data['availMessage'] !!}

<div><strong>#Booking ID:</strong> {{ $data['order']->id }}</div>
<div><strong>{{ $data['booking_type'] }}:</strong> {{ $data['tour'] }}</div> 
<div><strong>Name:</strong> {{ $data['order']->name }}</div> 
<div><strong>Email:</strong> {{ $data['order']->email }}</div>
<div><strong>Phone:</strong> {{ $data['order']->phone }}</div>
@if($data['date_type'] == 'date')
	<div><strong>Date:</strong> {{ Carbon\Carbon::parse($data['date_from'])->format('d M Y') }}</div>
@else
	@if($data['date_to'] == 'date')
		<div><strong>Date From:</strong> {{ Carbon\Carbon::parse($data['date_from'])->format('d M Y') }}</div>
		<div><strong>Date To:</strong> {{ Carbon\Carbon::parse($data['date_to'])->format('d M Y') }}</div>
	@else
		<div><strong>Date:</strong> {{ Carbon\Carbon::parse($data['date_from'])->format('d M Y') }}</div>
	@endif
@endif
<div><strong>Adults:</strong> {{ $data['order']->adults }}</div> 
<div><strong>Children:</strong> {{ $data['order']->childs ?? 0 }}</div> 
<div><strong>Passport ID:</strong> {{ $data['order']->passport }}</div> 
<div><strong>Nationality:</strong> {{ $data['order']->nationality }}</div> 
<div><strong>Guide Language:</strong> {{ $data['order']->guide }}</div> 
@if($data['order']->hotel)
	<div><strong>Hotel:</strong> {{ $data['order']->hotel }}</div> 
@endif
<div><strong>Total:</strong> {{ $data['order']->total_price }} USD</div> 
@if($data['extras'] !== '')
	<div><strong>Optional Activities:</strong></div>
	<div>{!! $data['extras'] !!}</div>
@endif
<div><strong>Note:</strong> {{ $data['order']->note }}</div>

<br/>
<br/>
M-Tours Team.
