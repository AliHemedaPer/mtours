@extends('front.front')

@section('content')
<div class="main-cont">   
  
  <div class="inner-page">
    <div class="inner-breadcrumbs">
      <div class="content-wrapper">
        <div class="page-title">@lang('fnt.blog')</div>
        <div class="breadcrumbs">
              <!-- <a href="#">Home</a> / <span>Blog Masonry</span> -->
            </div>
            <div class="clear"></div>
          </div>    
    </div>
    
     <div class="blog-wrapper">
      <div class="blog-masonry">
        <!-- // -->
        @foreach($posts as $post)
          <div class="blog-masonry-i" style="opacity: 1;float: left;">
            <div class="blog-masonry-preview">
              <div class="blog-masonry-img"><a href="{{ url('post/'.$post->id) }}"><img alt="" src="{{url($post->image)}}" /></a></div>
            </div>
            <div class="blog-masonry-lbl"><a href="{{ url('post/'.$post->id) }}">{{ $post->translate($share_locale)->title }}</a></div>
            <div class="blog-masonry-txt">{!! \Illuminate\Support\Str::words($post->translate($share_locale)->description, 20, ' ...') !!}</div>
            <div class="blog-masonry-info">{{ Carbon\Carbon::parse($post->created_at)->format('d M Y') }}</div>
          </div>
          @endforeach
        <!-- \\ -->
        <div class="clear"></div>
        {{ ($posts->count() > 9) ? $posts->links('front.pagination') : '' }}
        <div class="clear"></div>
      </div>
      
      </div>

    
  </div>
</div>
@stop