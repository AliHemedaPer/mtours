@extends('front.front')

@section('content')
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
   

  <div class="sp-page">
    <div class="sp-page-a">
      <div class="">
          <div class="sp-page-lb">
            <div class="sp-page-p">
            <div class="booking-left">
              <h2>@lang('fnt.b_succ')</h2>
              
              <div class="comlete-alert">
                <div class="comlete-alert-a">
                  <b>@lang('fnt.thanks')</b>
                  <span>@lang('fnt.will_con')</span>
                </div>
              </div>
              
              
              
            </div>
            </div>
          </div>
          <div class="clear"></div>
      </div>
    </div>

    <div class="clear"></div>
  </div>

    </div>  
  </div>  
</div>
@stop