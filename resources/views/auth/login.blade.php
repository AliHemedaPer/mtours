@extends('admin.layouts.admin_auth')

@section('content')
<!-- start: LOGIN BOX -->
<div class="box-login">
    <h3>لوحة التحكم</h3>
    <form class="form-login" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <fieldset>
            <div class="form-group {{($errors->has('email'))?'has-error':''}}">
                <span class="input-icon input-icon-right">
                    <input type="text" class="form-control" name="email" placeholder="البريد الإليكتروني">
                    <i class="fa fa-user"></i> </span>
                <!-- To mark the incorrectly filled input, you must add the class "error" to the input -->
                <!-- example: <input type="text" class="login error" name="login" value="Username" /> -->
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group form-actions {{($errors->has('password'))?'has-error':''}}">
                <span class="input-icon input-icon-right">
                    <input type="password" class="form-control password" name="password" placeholder="الرقم السري">
                    <i class="fa fa-lock"></i>
                    <a class="forgot" href="{{ route('password.request') }}">
                        نسيت كلمة السر؟
                    </a> </span>
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-actions">
                <!-- <label for="remember" class="checkbox-inline">
                    <input type="checkbox" class="grey remember" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                           Keep me signed in
                </label> -->
                <button type="submit" class="btn btn-bricky pull-left">
                    دخول <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </fieldset>
    </form>
</div>

@endsection
