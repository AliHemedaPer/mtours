<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['app_name', 'app_email', 'app_phone', 'app_address', 'app_currency', 'app_fees', 'app_fb', 'app_tw', 'app_in', 'app_wats', 'fawaterak_key', 'show_cash', 'image'];
     
    protected $table = 'settings';
}
