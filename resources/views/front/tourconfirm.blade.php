@extends('front.front')

@section('style')
<style type="text/css">
  .req{
    position: absolute;
    color: #ff0b0b;
    top: -18px;
    right: 0px;
    font-size: 13px;
    display: none;
  }
</style>
@endsection

@section('content')
<!-- main-cont -->
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
        <div class="page-head">
          <div class="page-title">@lang('fnt.tours') - <span>@lang('fnt.booking')</span></div>
          <div class="breadcrumbs">
            <!-- <a href="#">Home</a> / <a href="#">Tours</a> / <span>booking</span> -->
          </div>
          <div class="clear"></div>
        </div>

         @if (Request::session()->has('error')) 
          <div class="message-box-c">{{Request::session()->get('error')}}</div>
         @endif

      <form id="booking_form" method="POST" action="{{ url('/orderaction/').'/'.$order->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="booking_type" value="tour"> 
        <div class="sp-page">
        

        <div class="sp-page-r sp-page-r-book">
          
          <div class="checkout-coll">
            <div class="checkout-head">
              <div class="checkout-headl">
                <a href="{{ url('tour/'.$tour->id) }}"><img alt="" src="../{{ $tour->image }}" style="max-width: 100px"></a>
              </div>
              <div class="checkout-headr">
                  <div class="checkout-headrb">
                    <div class="checkout-headrp">
                      <div class="chk-left">
                        <div class="chk-lbl"><a href="{{ url('tour/'.$tour->id) }}">{{ $tour->translate($share_locale)->title }}</a></div>
                        <div class="chk-lbl-a"></div>
                        <!-- <nav class="chk-stars">
                          <ul>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-a.png"></li>
                          </ul>
                          <div class="clear"></div>
                        </nav> -->
                        
                      </div>
                      <div class="chk-right">
                        <!-- <a href="#"><img alt="" src="{{ asset('public/front/img') }}/chk-edit.png"></a> -->
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="clear"></div>
              </div>
            </div>

            <div class="chk-lines">
              <div class="chk-line">
                @lang('fnt.adults'): <span class="chk-persons"><span id="total_adults_num">{{ $order->adults }}</span> @lang('fnt.persons')</span>
              </div>
              <div class="chk-line">
                @lang('fnt.child'): <span class="chk-persons"><span id="total_childs_num">{{ $order->childs }}</span></span>
              </div>
            </div>
            
            <div class="chk-details">
              <div class="chk-detais-row">
                
                @if($orderExtras->count())
                    <h2>@lang('fnt.opts')</h2>
                
                    @foreach($orderExtras as $extra)
                    <div class="chk-line">
                      <span class="chk-l">
                        {{ $extra->productExtras->translate($share_locale)->title }}</span>
                      <div class="clear"></div>
                    </div>
                    @endforeach
                @endif  
                
              </div>
              <div class="chk-total">
                <div class="chk-total-l">@lang('fnt.total')</div>
                <div class="chk-total-r" ><span id="total">{{ number_format($order->total_price, 2, '.', '') }}</span> {{ $app_settings['app_currency'] }}</div>
                <div class="clear"></div>
              </div>          
            </div>
            
          </div>
        </div>

        
        <div class="sp-page-l sp-page-l-book">
            <div class="sp-page-lb">
              <div class="sp-page-p">
              {{-- @if (auth()->check() && auth()->user()->isClient()) --}}
              <div class="booking-left"> 
                <h2>@lang('fnt.tour_info')</h2>
                
                <div class="booking-form" style="line-height: 25px">
                  <div><strong>@lang('fnt.b_id'):</strong> {{ $order['id'] }}</div>
                  <div><strong>@lang('fnt.name'):</strong> {{ $order['name'] }}</div> 
                  <div><strong>@lang('fnt.email'):</strong> {{ $order['email'] }}</div>
                  <div><strong>@lang('fnt.phone'):</strong> {{ $order['phone'] }}</div>
                  <div><strong>@lang('fnt.adults'):</strong> {{ $order->adults }}</div> 
                  <div><strong>@lang('fnt.child'):</strong> {{ $order->childs ?? 0 }}</div> 
                  <div><strong>@lang('fnt.pssprt'):</strong> {{ $order->passport }}</div> 
                  <div><strong>@lang('fnt.natio'):</strong> {{ $order->nationality }}</div> 
                  <div><strong>@lang('fnt.guide'):</strong> {{ $order->guide }}</div> 

                  <div><strong>{{ $order['date_to'] ? __('fnt.date_f') : __('fnt.date') }} :</strong> {{ Carbon\Carbon::parse($order['date_from'])->format('d M Y') }}</div>
                  
                  @if($order['date_to'])
                  <div><strong>@lang('fnt.date_t'):</strong> {{ Carbon\Carbon::parse($order['date_to'])->format('d M Y') }}</div>
                  @endif
                  

                  @if($order['hotel'])
                    <div><strong>@lang('fnt.hotel'):</strong> {{ $order['hotel'] }}</div> 
                  @endif
                
                  <div><strong>@lang('fnt.note'):</strong> {{ $order['note'] }}</div>
                </div>
                <div class="booking-devider no-margin"></div> 
                <!-- <h2>Payament</h2> -->
                
                <div class="payment-wrapper">
                  <div class="payment-tabs">
                    <a href="#" class="active">
                        @lang('fnt.payments')
                      <span></span></a>
                    <!-- <a href="#">Other<span></span></a> -->
                  </div>
                  <div class="clear"></div>
                  <div class="payment-tabs-content">
                    <!-- // -->
                    <div class="payment-tab">
                      <div class="payment-type">
                        <input type="radio" value="visa" name="payment_type" checked="" />
                        <img src="{{ asset('public/front/img/') }}/paymentt-01.png">
                        <img src="{{ asset('public/front/img/') }}/paymentt-02.png">
                      </div>
                      <div class="clear"></div>
                      @if($app_settings['show_cash'])
                      <div class="payment-type">
                        <input type="radio" value="cash" name="payment_type"/>
                        @lang('fnt.cash')
                      </div>
                      @endif
                      <div class="checkbox">
                        <div class="booking-devider no-margin"></div>
                        <label>
                            <input type="checkbox" value="1" name="tour_accept" id="tour_accept" />
                            @lang('fnt.accept') <a href="{{ url('/terms') }}">@lang('fnt.terms')</a>
                        </label>
                      </div> 
                      <div class="req" style="position: relative;padding-top: 10px;font-weight: bold;" id="tour_accept_req">@lang('fnt.accept') @lang('fnt.terms')!</div>
                    </div>
                    <!-- \\ -->
                    <!-- // -->
                    <!-- <div class="payment-tab">
                      <div class="payment-alert">
                        <span>You will be redirected to website to securely complete your payment.</span>
                        <div class="payment-alert-close"><a href="#"><img alt="" src="{{ asset('public/front/img/') }}/alert-close.png"></a></div>
                      </div>
                      <a href="#" class="paypal-btn">proceed</a>
                    </div> -->
                    <!-- \\ -->
                  </div>
                </div>
                
                <div class="booking-complete">
                  
                  <h2>@lang('fnt.review')</h2>
                  <p>
                    @lang('fnt.sure')
                  </p> 
                  
                  <button class="booking-complete-btn" type="button" onclick="booking_action()" style="float: right;">@lang('fnt.book')</button>

                  <img id="loading" src="{{ asset('public/front/img/') }}/89.gif" style="float: right; margin: 30px 10px 0 0px;display: none;" />

                  <div id="error_message" style="float: right; margin: 35px 10px 0 0px;color: rgb(253, 48, 48);display: none;font-weight: bold;"></div>

                  <div class="clear"></div>
                </div>
                
              </div>
              </div>
            </div>
            <div class="clear"></div>
        </div>
        

        
        <div class="clear"></div>
        </div>
      </form>
    </div>  
  </div>  
</div>
<!-- /main-cont -->
@endsection
@section('javascript')
  <script>

    function booking_action(){
      
      if (!$('#tour_accept').is(":checked")){
        $('#tour_accept_req').fadeIn();
        validate = false;
      }else{
        validate = true;
        $('#tour_accept_req').fadeOut();
      }
      
      if(validate){
        $('#loading').fadeIn();
        $('#error_message').fadeOut();

        var form=$("#booking_form").submit();

        // $.ajax({
        //     type:"POST",
        //     url:form.attr("action"),
        //     headers: {
        //         'X-CSRF-TOKEN': '{{ csrf_token() }}'
        //     },
        //     data: $("#booking_form").serializeArray(),//only input
        //     success: function(response){
        //       $('#loading').fadeOut();
        //       if(response.status == 'success'){
        //         window.location.href = "{{ url('/bookingsuccess') }}"
        //       }else{
        //         $('#error_message').text(response.message);
        //         $('#error_message').fadeIn();
        //       }
        //     }
        // });
      }else{
        $('#error_message').text('Please check required fields!');
        $('#error_message').fadeIn();
      }
    }
  </script>
  @endsection