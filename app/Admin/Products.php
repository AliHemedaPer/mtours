<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title', 'description'];
    protected $fillable = [ 'cat_id', 
                            'title', 
                            'description', 
                            'price', 
                            'min_price', 
                            'child_from', 
                            'child_to', 
                            'child_price', 
                            'day_type', 
                            'days', 
                            'date_from', 
                            'date_to', 
                            'views', 
                            'booked', 
                            'image', 
                            'hot_offer',
                            'check_avail'
                        ];
    protected $table = 'products';

    public function categories()
    {
        return $this->belongsTo(Categories::class, 'cat_id', 'id');
    }

    public function productPrices()
    {
        return $this->hasMany(ProductPrices::class, 'product_id', 'id')->orderBy('price', 'ASC');
    }

    public function displayPrice()
    {
        $min_price = $this->min_price;
        if($min_price)
            return $min_price;
        else
            return $this->productPrices()->min('price');
    }

    public function productImages()
    {
        return $this->hasMany(ProductImages::class, 'product_id', 'id');
    }

    public function productExtras()
    {
        return $this->hasMany(ProductExtras::class, 'product_id', 'id');
    }
}
