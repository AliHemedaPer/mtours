<div class="pagination">
    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
        <a class="page-link" rel="prev">&laquo;</a>
    @else
        <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a>
    @endif

    <!-- Pagination Elements -->
    @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
        @if (is_string($element))
            <span>{{ $element }}</span>
        @endif

        <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <a class="active" >{{ $page }}</a>
                @else
                    <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                @endif
            @endforeach
        @endif
    @endforeach

    <!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a>
    @else
        <a class="page-link" rel="prev">&raquo;</a>
    @endif
</div>