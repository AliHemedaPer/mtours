-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 28, 2022 at 09:57 AM
-- Server version: 10.3.37-MariaDB-log-cll-lve
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropdxfh_mummiestours`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `short_description`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, '5', '6', '7', 'uploads/AboutPage/1658862101.jpeg', NULL, '2022-07-26 23:01:41'),
(2, '101', '', '102', NULL, '2019-07-04 17:30:17', '2019-07-04 17:30:17');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` text DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `description`, `url`, `image`, `created_at`, `updated_at`) VALUES
(1, '33', '34', NULL, 'uploads/banner/image_1559190884.jpg', '2019-05-25 09:20:26', '2019-05-30 08:34:44'),
(2, '42', '43', NULL, 'uploads/banner/image_1559190969.jpg', '2019-05-30 08:36:10', '2019-05-30 08:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL,
  `in_home` tinyint(4) NOT NULL DEFAULT 0,
  `full_half` tinyint(4) NOT NULL DEFAULT 0,
  `image` varchar(100) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `title`, `in_home`, `full_half`, `image`, `created_at`, `updated_at`) VALUES
(1, 0, '13', 0, 0, '0', '2019-05-25 05:21:14', '2019-05-25 05:21:14'),
(2, 1, '14', 1, 1, 'uploads/categories/image_1559450496.jpg', '2019-05-25 05:23:43', '2019-06-02 08:41:36'),
(4, 1, '16', 1, 0, 'uploads/categories/image_1559009807.jpg', '2019-05-25 05:30:40', '2019-05-25 05:30:40'),
(5, 1, '17', 1, 0, 'uploads/categories/image_1559009808.jpg', '2019-05-25 05:31:20', '2019-05-25 05:31:20'),
(6, 1, '18', 1, 0, 'uploads/categories/image_1559009809.jpg', '2019-05-25 05:31:48', '2019-05-25 05:31:48'),
(7, 1, '19', 1, 0, 'uploads/categories/image_15590098010.jpg', '2019-05-25 05:32:19', '2019-05-25 05:32:19'),
(8, 0, '20', 0, 0, '0', '2019-05-25 05:32:56', '2019-05-25 05:32:56');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `passport` varchar(191) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `email`, `phone`, `passport`, `nationality`, `created_at`, `updated_at`) VALUES
(1, 'Ali Hemeda', 'admin@mummiestours.com', '01111111111111', '111', 'armenian', '2022-07-15 19:06:58', '2022-07-15 19:06:58'),
(2, 'Ali Hemeda', 'ali@mummiestours.com', '01111111111111', '111', 'spanish', '2022-07-15 19:30:20', '2022-07-15 19:30:20'),
(3, 'Ali Hemeda', 'senjider@gmail.com', '01111111111111', '111', 'bahraini', '2022-07-15 19:58:49', '2022-07-15 19:58:49'),
(4, 'Ahmed Mohsen', 'Amohsen7621@gmail.com', '01110266196', '1234558', 'american', '2022-08-18 01:30:56', '2022-08-18 01:30:56'),
(5, 'Salah Ehab', 'Salah70708080@gmail.com', '01100393295', '1020644788543367', 'albanian', '2022-08-18 01:38:09', '2022-08-18 01:38:09'),
(6, 'Tim Crowder', 'tlcws6@yahoo.com', '3047109905', '556324688', 'american', '2022-08-30 04:37:44', '2022-08-30 04:37:44'),
(7, 'Salah salah', 'info@highertravelegypt.com', '01507004505', 'Gshsgshshshsh', 'american', '2022-11-24 07:02:36', '2022-11-24 07:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_06_232652_create_about_table', 1),
(4, '2019_02_06_232652_create_banners_table', 1),
(5, '2019_02_06_232652_create_categories_table', 1),
(6, '2019_02_06_232652_create_posts_table', 1),
(7, '2019_02_06_232652_create_services_table', 1),
(8, '2019_02_06_232652_create_settings_table', 1),
(9, '2019_02_08_171058_translations', 1),
(10, '2019_03_08_194639_create_products_table', 1),
(11, '2019_05_24_232503_create_product_images_table', 1),
(12, '2019_05_24_232611_create_product_prices_table', 1),
(13, '2019_05_24_232628_create_product_extras_table', 1),
(14, '2019_05_24_232824_create_orders_table', 1),
(15, '2019_05_24_232846_create_order_extras_table', 1),
(16, '2019_06_28_002636_create_transportaions_table', 2),
(17, '2019_06_28_002851_create_transportaion_prices_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childs` int(11) DEFAULT NULL,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `passport` varchar(191) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `guide` varchar(100) NOT NULL,
  `note` text DEFAULT NULL,
  `hotel` varchar(255) DEFAULT NULL,
  `total_price` double(10,2) NOT NULL DEFAULT 0.00,
  `payment_type` varchar(5) NOT NULL DEFAULT 'cash',
  `invoice_id` varchar(191) DEFAULT NULL,
  `payment_status` tinyint(1) NOT NULL DEFAULT 0,
  `booking_type` enum('tour','transportation') NOT NULL DEFAULT 'tour',
  `order_done` tinyint(1) NOT NULL DEFAULT 0,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `is_available` tinyint(1) DEFAULT NULL,
  `code` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `product_id`, `adults`, `childs`, `date_from`, `date_to`, `name`, `email`, `phone`, `passport`, `nationality`, `guide`, `note`, `hotel`, `total_price`, `payment_type`, `invoice_id`, `payment_status`, `booking_type`, `order_done`, `is_read`, `is_available`, `code`, `created_at`, `updated_at`) VALUES
(3, NULL, 1, 1, NULL, '2022-07-18', NULL, 'Ali Hemeda', 'ali@mummiestours.com', '01111111111111', '111', 'spanish', 'Spanish', 'test', 'Sphotel', 61.80, 'cash', NULL, 1, 'tour', 1, 1, NULL, 's3zOkEFctK165789902061W3SbUcFa', '2022-07-15 19:30:20', '2022-07-15 20:00:23'),
(4, NULL, 1, 1, NULL, '2022-07-18', NULL, 'Ali Hemeda', 'admin@mummiestours.com', '01111111111111', '111', 'spanish', 'Spanish', 'test', 'Sphotel', 61.80, 'cash', NULL, 1, 'tour', 0, 1, NULL, 'kNMsBNw1Od1657899502EV6428hYaE', '2022-07-15 19:38:22', '2022-07-17 02:27:28'),
(7, NULL, 6, 1, NULL, '2022-07-30', NULL, 'ghj fty', 'admin@mummiestours.com', '01110266196', '34567890', 'australian', 'English', NULL, 'sddss', 103.00, 'visa', NULL, 0, 'tour', 0, 1, NULL, '89MulRANqN16580873433FWS1NcnhM', '2022-07-17 23:49:03', '2022-07-25 01:23:03'),
(8, NULL, 2, 2, NULL, '2022-07-29', NULL, 'ahmed mohsen', 'admin@mummiestours.com', '01110266196', '123445667', 'australian', 'English', NULL, 'sdfgghj', 123.60, 'visa', NULL, 0, 'transportation', 0, 0, NULL, NULL, '2022-07-26 23:46:26', '2022-07-26 23:46:26'),
(9, NULL, 7, 2, 0, '2022-08-31', NULL, 'Ahmed Mohsen', 'Amohsen7621@gmail.com', '01110266196', '1234558', 'american', 'English', NULL, 'Hilton', 100.00, 'visa', NULL, 0, 'tour', 0, 1, NULL, 'Pkncnbq1Rb1660771856cJUJ5G91NI', '2022-08-18 01:30:56', '2022-08-18 01:36:59'),
(10, NULL, 8, 1, 1, '2022-08-31', NULL, 'Salah Ehab', 'Salah70708080@gmail.com', '01100393295', '1020644788543367', 'albanian', 'English', NULL, 'Skara', 150.00, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'xx5KlHVSgP1660772289LQiJAds4cy', '2022-08-18 01:38:09', '2022-08-18 01:38:09'),
(16, NULL, 9, 1, 0, '2022-09-22', NULL, 'Ahmed Mohsen', 'Amohsen7621@gmail.com', '01110266196', '12345678', 'american', 'English', NULL, 'Hilto', 5.00, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'iFklZuu9e41661964671cHgIzxLIJ1', '2022-08-31 20:51:12', '2022-08-31 20:51:12'),
(17, NULL, 9, 1, 0, '2022-09-15', NULL, 'Tim Crowder', 'tlcws6@yahoo.com', '3047109905', '556324688', 'american', 'English', 'Thank you', 'HiLton', 5.00, 'visa', NULL, 0, 'tour', 0, 1, NULL, '2w5aoL1xBK1661990222nc8qrQ4maQ', '2022-09-01 03:57:02', '2022-09-01 04:03:06'),
(18, NULL, 7, 8, 5, '2022-10-26', NULL, 'Salah Ehab', 'salah70708080@gmail.com', '01507004505', 'Gshsgshshshsh', 'afghan', 'English', NULL, 'Jsjsjshjxhxhsh', 525.00, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'nACIOpAYqU1665226912zLttyffpVI', '2022-10-08 15:01:52', '2022-10-08 15:01:52'),
(19, NULL, 8, 8, 0, '2022-10-18', NULL, 'Hhhdhj Chjh', 'salah70708080@gmail.com', '01507004505', 'Gshsgshshshsh', 'angolan', 'English', NULL, 'Ggddhjk', 800.00, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'YJUlYGIeKQ1665227193MrFooyrNip', '2022-10-08 15:06:33', '2022-10-08 15:06:33'),
(20, NULL, 9, 1, 1, '2022-10-11', NULL, 'Salah Chjh', 'salah70708080@gmail.com', '01507004505', 'Gshsgshshshsh', 'barbudans', 'English', NULL, 'Ggddhjk', 6.00, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'dW8LM1EoUW1665228033UcuKrogXQr', '2022-10-08 15:20:33', '2022-10-08 15:20:33'),
(21, NULL, 10, 6, 5, '2022-11-30', NULL, 'Salah salah', 'info@highertravelegypt.com', '01507004505', 'Gshsgshshshsh', 'american', 'English', NULL, 'Jsjsjshjxhxhsh', 1210.00, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'cA2uRUn7LV1669255355QTTGkoaqPw', '2022-11-24 07:02:36', '2022-11-24 07:02:36'),
(22, NULL, 10, 6, 0, '2022-11-30', NULL, 'Salah salah', 'info@highertravelegypt.com', '01507004505', 'Gshsgshshshsh', 'american', 'English', NULL, 'Jsjsjshjxhxhsh', 810.00, 'visa', NULL, 0, 'tour', 0, 0, NULL, '7iARkzaqiY1669255465Evz4ZhcZVl', '2022-11-24 07:04:25', '2022-11-24 07:04:25');

-- --------------------------------------------------------

--
-- Table structure for table `order_extras`
--

CREATE TABLE `order_extras` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_extras_id` int(11) NOT NULL,
  `adults` int(11) DEFAULT NULL,
  `childs` int(11) DEFAULT NULL,
  `total_price` double(10,2) DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` double(10,2) NOT NULL,
  `min_price` double(10,2) NOT NULL DEFAULT 0.00,
  `child_from` int(11) DEFAULT NULL,
  `child_to` int(11) DEFAULT NULL,
  `child_price` double(10,2) DEFAULT NULL,
  `day_type` enum('half','full','days') NOT NULL,
  `days` int(11) NOT NULL DEFAULT 0,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `booked` int(11) NOT NULL DEFAULT 0,
  `image` varchar(255) DEFAULT NULL,
  `hot_offer` tinyint(1) DEFAULT 0,
  `check_avail` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `cat_id`, `title`, `description`, `price`, `min_price`, `child_from`, `child_to`, `child_price`, `day_type`, `days`, `date_from`, `date_to`, `views`, `booked`, `image`, `hot_offer`, `check_avail`, `created_at`, `updated_at`) VALUES
(7, 2, '104', '105', 50.00, 50.00, 5, 11, 25.00, 'full', 1, NULL, NULL, 39, 0, 'uploads/products/image_1660771716.jpeg', 1, 0, '2022-08-18 01:28:36', '2022-11-26 14:53:38'),
(8, 2, '106', '107', 100.00, 100.00, 1, 50, 50.00, 'full', 1, NULL, NULL, 41, 0, 'uploads/products/image_1660771997.jpg', 1, 0, '2022-08-18 01:33:17', '2022-11-26 14:31:49'),
(9, 2, '108', '109', 5.00, 1.00, 5, 12, 1.00, 'full', 1, NULL, NULL, 54, 0, 'uploads/products/image_1661808877.jpeg', 1, 0, '2022-08-30 01:34:37', '2022-11-26 12:23:00'),
(10, 2, '110', '111', 240.00, 135.00, 1, 10, 80.00, 'full', 1, NULL, NULL, 3, 0, 'uploads/products/image_1669255029.jpeg', 0, 0, '2022-11-24 06:57:09', '2022-11-26 13:26:53');

-- --------------------------------------------------------

--
-- Table structure for table `product_extras`
--

CREATE TABLE `product_extras` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_extras`
--

INSERT INTO `product_extras` (`id`, `product_id`, `title`, `price`, `created_at`, `updated_at`) VALUES
(54, 4, '79', 10.00, '2019-06-21 06:12:09', '2019-06-21 06:12:09'),
(55, 3, '80', 10.00, '2019-06-21 06:12:34', '2019-06-21 06:12:34'),
(56, 3, '81', 10.00, '2019-06-21 06:12:34', '2019-06-21 06:12:34'),
(57, 3, '82', 10.00, '2019-06-21 06:12:34', '2019-06-21 06:12:34'),
(58, 2, '83', NULL, '2019-06-21 06:12:52', '2019-06-21 06:12:52'),
(59, 2, '84', NULL, '2019-06-21 06:12:52', '2019-06-21 06:12:52'),
(60, 2, '85', 20.00, '2019-06-21 06:12:53', '2019-06-21 06:12:53'),
(61, 2, '86', 10.00, '2019-06-21 06:12:53', '2019-06-21 06:12:53'),
(62, 2, '87', 20.00, '2019-06-21 06:12:53', '2019-06-21 06:12:53'),
(63, 2, '88', 10.00, '2019-06-21 06:12:54', '2019-06-21 06:12:54'),
(64, 1, '89', NULL, '2019-06-21 06:13:08', '2019-06-21 06:13:08'),
(65, 1, '90', NULL, '2019-06-21 06:13:08', '2019-06-21 06:13:08'),
(66, 1, '91', 20.00, '2019-06-21 06:13:08', '2019-06-21 06:13:08'),
(67, 1, '92', 10.00, '2019-06-21 06:13:08', '2019-06-21 06:13:08'),
(68, 1, '93', 20.00, '2019-06-21 06:13:09', '2019-06-21 06:13:09'),
(69, 1, '94', 10.00, '2019-06-21 06:13:09', '2019-06-21 06:13:09'),
(70, 1, '95', NULL, '2019-06-21 06:13:09', '2019-06-21 06:13:09'),
(71, 1, '96', NULL, '2019-06-21 06:13:10', '2019-06-21 06:13:10'),
(73, 5, '103', 30.00, '2019-07-13 20:32:06', '2019-07-13 20:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(11, 1, 'uploads/products/images/image_1560903826.jpg', NULL, NULL),
(14, 1, 'uploads/products/images/image_1560903907.jpg', NULL, NULL),
(15, 1, 'uploads/products/images/image_1560903919.jpg', NULL, NULL),
(24, 2, 'uploads/products/images/image_156090476724.jpg', '2019-06-19 04:39:27', '2019-06-19 04:39:27'),
(25, 2, 'uploads/products/images/image_156090476725.jpg', '2019-06-19 04:39:27', '2019-06-19 04:39:27'),
(26, 2, 'uploads/products/images/image_156090476826.jpg', '2019-06-19 04:39:28', '2019-06-19 04:39:28'),
(27, 1, 'uploads/products/images/image_156107314127.jpg', '2019-06-21 03:25:41', '2019-06-21 03:25:41'),
(28, 5, 'uploads/products/images/image_157868847128.jpg', '2020-01-11 01:34:31', '2020-01-11 01:34:31'),
(34, 10, 'uploads/products/images/image_166925512334.jpeg', '2022-11-24 06:58:43', '2022-11-24 06:58:43'),
(35, 10, 'uploads/products/images/image_166925512335.jpeg', '2022-11-24 06:58:43', '2022-11-24 06:58:43'),
(36, 10, 'uploads/products/images/image_166925512436.jpeg', '2022-11-24 06:58:44', '2022-11-24 06:58:44'),
(37, 10, 'uploads/products/images/image_166925512537.jpeg', '2022-11-24 06:58:45', '2022-11-24 06:58:45'),
(38, 10, 'uploads/products/images/image_166925512638.jpeg', '2022-11-24 06:58:46', '2022-11-24 06:58:46'),
(39, 10, 'uploads/products/images/image_166925512739.jpeg', '2022-11-24 06:58:47', '2022-11-24 06:58:47'),
(40, 10, 'uploads/products/images/image_166925512740.jpeg', '2022-11-24 06:58:47', '2022-11-24 06:58:47'),
(41, 10, 'uploads/products/images/image_166925512941.jpeg', '2022-11-24 06:58:49', '2022-11-24 06:58:49'),
(42, 10, 'uploads/products/images/image_166925512942.jpeg', '2022-11-24 06:58:49', '2022-11-24 06:58:49'),
(43, 10, 'uploads/products/images/image_166925513043.jpeg', '2022-11-24 06:58:50', '2022-11-24 06:58:50'),
(44, 10, 'uploads/products/images/image_166925513144.jpeg', '2022-11-24 06:58:51', '2022-11-24 06:58:51'),
(45, 10, 'uploads/products/images/image_166925513245.jpeg', '2022-11-24 06:58:52', '2022-11-24 06:58:52'),
(46, 10, 'uploads/products/images/image_166925513346.jpeg', '2022-11-24 06:58:53', '2022-11-24 06:58:53'),
(47, 10, 'uploads/products/images/image_166925513447.jpeg', '2022-11-24 06:58:54', '2022-11-24 06:58:54'),
(48, 10, 'uploads/products/images/image_166925513448.jpeg', '2022-11-24 06:58:54', '2022-11-24 06:58:54'),
(49, 10, 'uploads/products/images/image_166925513549.jpeg', '2022-11-24 06:58:55', '2022-11-24 06:58:55'),
(50, 10, 'uploads/products/images/image_166925513650.jpeg', '2022-11-24 06:58:56', '2022-11-24 06:58:56'),
(51, 10, 'uploads/products/images/image_166925513851.jpeg', '2022-11-24 06:58:58', '2022-11-24 06:58:58'),
(52, 10, 'uploads/products/images/image_166925513852.jpeg', '2022-11-24 06:58:58', '2022-11-24 06:58:58'),
(53, 10, 'uploads/products/images/image_166925514053.jpeg', '2022-11-24 06:59:00', '2022-11-24 06:59:00'),
(54, 10, 'uploads/products/images/image_166925514054.jpeg', '2022-11-24 06:59:00', '2022-11-24 06:59:00'),
(55, 10, 'uploads/products/images/image_166925514155.jpeg', '2022-11-24 06:59:01', '2022-11-24 06:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `product_prices`
--

CREATE TABLE `product_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_prices`
--

INSERT INTO `product_prices` (`id`, `product_id`, `from`, `to`, `price`, `created_at`, `updated_at`) VALUES
(78, 4, 2, 3, 370.00, NULL, NULL),
(79, 4, 4, 9, 345.00, NULL, NULL),
(80, 3, 2, 3, 45.00, NULL, NULL),
(81, 3, 4, 6, 35.00, NULL, NULL),
(82, 2, 2, 3, 50.00, NULL, NULL),
(83, 2, 4, 6, 40.00, NULL, NULL),
(84, 1, 2, 3, 50.00, NULL, NULL),
(85, 1, 4, 6, 40.00, NULL, NULL),
(88, 5, 2, 3, 125.00, NULL, NULL),
(89, 5, 4, 9, 105.00, NULL, NULL),
(92, 6, 2, 2, 50.00, NULL, NULL),
(93, 6, 3, 5, 30.00, NULL, NULL),
(94, 7, 2, 3, 50.00, NULL, NULL),
(95, 8, 1, 1, 100.00, NULL, NULL),
(97, 9, 1, 2, 1.00, NULL, NULL),
(106, 10, 1, 1, 240.00, NULL, NULL),
(107, 10, 2, 2, 160.00, NULL, NULL),
(108, 10, 3, 5, 145.00, NULL, NULL),
(109, 10, 6, 10, 135.00, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `home_image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `description`, `home_image`, `created_at`, `updated_at`) VALUES
(1, '10', '11', 'uploads/services/home_image_1549665773.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_name` varchar(255) NOT NULL,
  `app_email` varchar(255) NOT NULL,
  `app_phone` varchar(255) NOT NULL,
  `app_address` varchar(255) DEFAULT NULL,
  `app_currency` varchar(255) NOT NULL,
  `app_fees` int(2) NOT NULL,
  `app_fb` varchar(255) DEFAULT NULL,
  `app_tw` varchar(255) DEFAULT NULL,
  `app_in` varchar(255) DEFAULT NULL,
  `app_wats` varchar(255) DEFAULT NULL,
  `fawaterak_key` varchar(255) DEFAULT NULL,
  `show_cash` tinyint(4) NOT NULL DEFAULT 0,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `app_name`, `app_email`, `app_phone`, `app_address`, `app_currency`, `app_fees`, `app_fb`, `app_tw`, `app_in`, `app_wats`, `fawaterak_key`, `show_cash`, `image`, `created_at`, `updated_at`) VALUES
(1, 'mummiestours', 'Info@mummiestours.com', '+201110266196 , +20100 0233964', '5th St. Cairo, Egypt.', 'USD', 0, '#', '#', '#', '0106666666', '8b3b6c23cefc616a902820862056f70739e40fdaf0518e7b1d', 0, 'uploads/logo/logo.jpg', NULL, '2022-08-30 01:23:58');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `value` text DEFAULT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `group_id`, `value`, `locale`) VALUES
(4, 3, 'Permanent design service', 'en'),
(5, 3, 'Permanent design service', 'sp'),
(6, 4, 'Sign up now with our Instant Design service and save your monthly expenses...', 'en'),
(7, 4, 'Sign up now with our Instant Design service and save your monthly expenses...', 'sp'),
(8, 5, 'About MTours', 'en'),
(9, 5, 'MTours', 'sp'),
(10, 6, 'We are very excited to introduce M TOURS as a provider for all your travel needs and requirements in Egypt. We label ourselves as innovative and highly efficient travel agency and we look forward to demonstrating these qualities to let you have an unforgettable and a unique experience in Egypt. Our primary aim is to provide quality travel services to our travelers and guests. \r\nIf you would like a tour that is tailored exactly like the way you want, you found the right company. Send us a request and we would be happy to assist! Also, if this is your first time to Egypt and you want a designed-by-experts tour to make the most of your time, without all the thinking and planning, have a look at some our all-time popular and available itineraries! \r\n\r\n\r\n***Please have a look at our FAQs page for more information on our service. Check out our Gallery page for some of the awesome sights you may get to see on our tours as well!', 'en'),
(11, 6, 'We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .', 'sp'),
(12, 7, '<p>We are very excited to introduce&nbsp;<strong>Mummies &nbsp;TOURS</strong>&nbsp;as a provider for all your travel needs and requirements in Egypt. We label ourselves as innovative and highly efficient travel agency and we look forward to demonstrating these qualities to let you have an unforgettable and a unique experience in Egypt.&nbsp;<br />\r\nOur primary aim is to provide quality travel services to our travelers and guests.</p>\r\n\r\n<p>If you would like a tour that is tailored exactly like the way you want, you found the right company. Send us a request and we would be happy to assist!</p>\r\n\r\n<p>Also, if this is your first time to Egypt and you want a designed-by-experts tour to make the most of your time, without all the thinking and planning, have a look at some our all-time popular and available itineraries!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>***Please have a look at our FAQs page for more information on our service. Check out our Gallery page for some of the awesome sights you may get to see on our tours as well!</p>\r\n\r\n<p>&nbsp;</p>', 'en'),
(13, 7, '<p>We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .</p>', 'sp'),
(14, 8, 'Marketing', 'en'),
(15, 8, 'Marketing', 'sp'),
(16, 9, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'en'),
(17, 9, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'sp'),
(18, 10, 'Marketing', 'en'),
(19, 10, 'Marketing', 'sp'),
(20, 11, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'en'),
(21, 11, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'sp'),
(22, 12, 'Day Tour', 'en'),
(23, 12, 'Dia de paseo', 'sp'),
(24, 13, 'Luxor Day Tours', 'en'),
(25, 13, 'Dia de paseo', 'sp'),
(26, 14, 'Cairo', 'en'),
(27, 14, 'El Cairo', 'sp'),
(30, 16, 'Alexandira', 'en'),
(31, 16, 'Alejandría', 'sp'),
(32, 17, 'Aswan', 'en'),
(33, 17, 'Aswan', 'sp'),
(34, 18, 'Hurghada', 'en'),
(35, 18, 'Hurghada', 'sp'),
(36, 19, 'Sharm El-SHiekh', 'en'),
(37, 19, 'Sharm El-SHiekh', 'sp'),
(38, 20, 'Packages', 'en'),
(39, 20, 'Paquetes Multi Días', 'sp'),
(52, 27, 'Cairo Tours to Giza Pyramids, Egyptian Museum and Khan EL-Khalili Bazaar', 'en'),
(53, 27, 'Cairo Tours to Giza Pyramids, Egyptian Museum and Khan EL-Khalili Bazaar', 'sp'),
(54, 28, '<p>Eight-hour tour starts every day from 8 am to 4 pm. Thetour includes pick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local restaurant, service charges, taxes and snacks bag (A bottle of water, a can of soft drink, chips and a cake)</p>\r\n\r\n<p>All Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The tour excludes personal items, tipping and any Optional Activities.</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>GIZA PYRAMIDS, EGYPTIAN MUSEUM AND KHAN EL-KHALILI BAZAAR PROGRAM:</h3>\r\n\r\n<p>The tour starts at 08:00 am.&nbsp;<strong>M Tours</strong>&nbsp;guide will pick you up from your hotel whether in Cairo or Giza to start the full day tour. It begins with&nbsp;<strong>Giza Pyramid Complex</strong>&nbsp;where you can visit the&nbsp;<strong>Three Great Pyramids</strong>; Cheops, Chephren and Mykerinos,&nbsp;<strong>The Queens&#39; Pyramids</strong>,&nbsp;<strong>The Valley Temple&nbsp;</strong>and&nbsp;<strong>The Sphinx</strong>, the mythical creature with a body of a lion and a human face, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that, your day tour continues to&nbsp;<strong>The Egyptian Museum</strong>, one of the largest museums in the region. The Egyptian Museum of Antiquities contains many important pieces of ancient Egyptian history. It hosts the world&#39;s largest collection of Pharaonic antiquities. It has 120,000 items, with a representative amount on display. It also includes&nbsp;<strong>King Tutankhamon</strong>&#39;s fascinating gold collection.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Then, the tour proceeds with&nbsp;<strong>Khan El-Khalili</strong>, which is the major Souk in the historic center of Islamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike. You can shop from there some memorable souvenirs as copper handicrafts, perfumes, leather, silver and gold products, antiques and many other special products.</p>\r\n\r\n<p><strong>Lunch is included</strong>&nbsp;during any of the tours at a Local Restaurant. (Full Meal for each person; beverages excluded)</p>\r\n\r\n<p>The Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The Tour Includes:</strong></p>\r\n\r\n<ul>\r\n	<li>A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)</li>\r\n	<li>A short camel ride (15 minutes)</li>\r\n	<li>Lunch at a Local Restaurant (beverages excluded)</li>\r\n	<li>Pick-up and drop off</li>\r\n	<li>Expert Tour guide</li>\r\n</ul>', 'en'),
(55, 28, '<p>Eight-hour tour starts every day from 8 am to 4 pm. Thetour includes pick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local restaurant, service charges, taxes and snacks bag (A bottle of water, a can of soft drink, chips and a cake)</p>\r\n\r\n<p>All Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The tour excludes personal items, tipping and any Optional Activities.</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>GIZA PYRAMIDS, EGYPTIAN MUSEUM AND KHAN EL-KHALILI BAZAAR PROGRAM:</h3>\r\n\r\n<p>The tour starts at 08:00 am. <strong>M Tours</strong> guide will pick you up from your hotel whether in Cairo or Giza to start the full day tour. It begins with <strong>Giza Pyramid Complex</strong> where you can visit the <strong>Three Great Pyramids</strong>; Cheops, Chephren and Mykerinos, <strong>The Queens&#39; Pyramids</strong>, <strong>The Valley Temple </strong>and <strong>The Sphinx</strong>, the mythical creature with a body of a lion and a human face, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that, your day tour continues to <strong>The Egyptian Museum</strong>, one of the largest museums in the region. The Egyptian Museum of Antiquities contains many important pieces of ancient Egyptian history. It hosts the world&#39;s largest collection of Pharaonic antiquities. It has 120,000 items, with a representative amount on display. It also includes <strong>King Tutankhamon</strong>&#39;s fascinating gold collection.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Then, the tour proceeds with <strong>Khan El-Khalili</strong>, which is the major Souk in the historic center of Islamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike. You can shop from there some memorable souvenirs as copper handicrafts, perfumes, leather, silver and gold products, antiques and many other special products.</p>\r\n\r\n<p><strong>Lunch is included</strong> during any of the tours at a Local Restaurant. (Full Meal for each person; beverages excluded)</p>\r\n\r\n<p>The Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The Tour Includes:</strong></p>\r\n\r\n<ul>\r\n	<li>A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)</li>\r\n	<li>A short camel ride (15 minutes)</li>\r\n	<li>Lunch at a Local Restaurant (beverages excluded)</li>\r\n	<li>Pick-up and drop off</li>\r\n	<li>Expert Tour guide</li>\r\n</ul>', 'sp'),
(64, 33, 'The Best Realx for Us', 'en'),
(65, 33, 'The Best Realx for Us', 'sp'),
(66, 34, 'Make your life Better', 'en'),
(67, 34, 'Make your life Better', 'sp'),
(120, 42, 'the best relax with us', 'en'),
(121, 42, 'the best relax with us', 'sp'),
(122, 43, 'the best relax with us', 'en'),
(123, 43, 'the best relax with us', 'sp'),
(134, 5, 'About MTours', 'ar'),
(135, 6, 'We are very excited to introduce M TOURS as a provider for all your travel needs and requirements in Egypt. We label ourselves as innovative and highly efficient travel agency and we look forward to demonstrating these qualities to let you have an unforgettable and a unique experience in Egypt. Our primary aim is to provide quality travel services to our travelers and guests. If you would like a tour that is tailored exactly like the way you want, you found the right company. Send us a request and we would be happy to assist! Also, if this is your first time to Egypt and you want a designed-by-experts tour to make the most of your time, without all the thinking and planning, have a look at some our all-time popular and available itineraries! ***Please have a look at our FAQs page for more information on our service. Check out our Gallery page for some of the awesome sights you may get to see on our tours as well!', 'ar'),
(136, 7, '<p>Terms &amp; Conditions</p>', 'ar'),
(209, 76, 'ww', 'en'),
(210, 76, 'ww', 'sp'),
(211, 77, '<p>ww</p>', 'en'),
(212, 77, '<p>ww</p>', 'sp'),
(217, 79, 'Visiting King Tutankhamun Tomb at The Valley of The Kings', 'en'),
(218, 79, 'Visiting King Tutankhamun Tomb at The Valley of The Kings', 'sp'),
(219, 80, 'Coptic Museum', 'en'),
(220, 80, 'Coptic Museum', 'sp'),
(221, 81, 'Islamic Museum', 'en'),
(222, 81, 'Islamic Museum', 'sp'),
(223, 82, 'A Felucca boat ride on the Nile', 'en'),
(224, 82, 'A Felucca boat ride on the Nile', 'sp'),
(225, 83, 'Getting inside any of the Pyramids', 'en'),
(226, 83, 'Getting inside any of the Pyramids', 'sp'),
(227, 84, 'Visiting the Solar Boat', 'en'),
(228, 84, 'Visiting the Solar Boat', 'sp'),
(229, 85, 'One-hour camel ride', 'en'),
(230, 85, 'One-hour camel ride', 'sp'),
(231, 86, 'Half-hour camel ride', 'en'),
(232, 86, 'Half-hour camel ride', 'sp'),
(233, 87, 'Quad Bike at the Wide Desert', 'en'),
(234, 87, 'Quad Bike at the Wide Desert', 'sp'),
(235, 88, 'A Felucca boat ride on the Nile', 'en'),
(236, 88, 'A Felucca boat ride on the Nile', 'sp'),
(237, 89, 'Getting inside any of the Pyramids', 'en'),
(238, 89, 'Getting inside any of the Pyramids', 'sp'),
(239, 90, 'Visiting the Solar Boat', 'en'),
(240, 90, 'Visiting the Solar Boat', 'sp'),
(241, 91, 'One-hour camel ride', 'en'),
(242, 91, 'One-hour camel ride', 'sp'),
(243, 92, 'Half-hour camel ride', 'en'),
(244, 92, 'Half-hour camel ride', 'sp'),
(245, 93, 'Quad Bike at the Wide Desert', 'en'),
(246, 93, 'Quad Bike at the Wide Desert', 'sp'),
(247, 94, 'A Felucca boat ride on the Nile', 'en'),
(248, 94, 'A Felucca boat ride on the Nile', 'sp'),
(249, 95, 'Mummies Room at The Egyptian Museum', 'en'),
(250, 95, 'Mummies Room at The Egyptian Museum', 'sp'),
(251, 96, 'Photography Fees at The Egyptian Museum – (Photography is prohibited inside Tutankhamon\'s room)', 'en'),
(252, 96, 'Photography Fees at The Egyptian Museum – (Photography is prohibited inside Tutankhamon\'s room)', 'sp'),
(253, 97, 'From Cairo Airport to Alexanderia', 'en'),
(254, 97, 'From Cairo Airport to Alexanderia', 'sp'),
(255, 98, '<p>Cairo Airport to Alexanderia</p>', 'en'),
(256, 98, '<p>Cairo Airport to Alexanderia</p>', 'sp'),
(257, 99, 'From Cairo Airport to Cairo', 'en'),
(258, 99, 'From Cairo Airport to Cairo', 'sp'),
(259, 100, '<p>From Cairo Airport to Cairo</p>', 'en'),
(260, 100, '<p>From Cairo Airport to Cairo</p>', 'sp'),
(266, 101, 'Terms & Conditions', 'en'),
(267, 101, 'Terms & Conditions', 'sp'),
(268, 102, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'en'),
(269, 102, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'sp'),
(272, 103, 'Jet-skiing for 1 Hour', 'en'),
(273, 103, 'Jet-skiing for 1 Hour', 'sp'),
(280, 104, 'Giza pyramids', 'en'),
(281, 104, 'Giza pyramids', 'sp'),
(282, 105, '<p>Test&nbsp;</p>', 'en'),
(283, 105, '<p>Test&nbsp;</p>', 'sp'),
(284, 106, 'DAY TOUR TO GIZA PYRAMIDS MEMPHIS CITY DAHSHUR AND SAQQARA PYRAMIDS', 'en'),
(285, 106, 'DAY TOUR TO GIZA PYRAMIDS MEMPHIS CITY DAHSHUR AND SAQQARA PYRAMIDS', 'sp'),
(286, 107, 'starts At 8:00 am Pickup from your hotel in Cairo/Giza By Emo Tours guide to Start your full day tour visiting Giza Pyramids where you Visit the Great Pyramids - Cheops, Chephren & Mykerinos then Visit the Valley Temple where the Priests Mummified the dead body of King Chephren Also there you will have a close-up look at The Sphinx - The legendary guardian that stands by the huge funeral complex with its lion body and the head of king Chephren.(2:30 hrs time Spent)\r\n\r\nLunch is included in between visits at Local Restaurant Based on Full Meal for each Person but Beverages are not included (1 Hr time Spent)\r\n\r\nThen Visit Sakkara, located only 27 km southwest away from Cairo. Visit the Step Pyramid (Built for king Zoser), it is considered an important part of the process of the pyramid evolution, which were made from the simple mastaba to its widely known form.(2 hrs Time Spent)\r\n\r\nThen Visit Dahshur Pyramids (Bent and Red Pyramids) Dahshur area is a royal necropolis located in the desert on the West Bank of the Nile approximately 40 kilometers (25 mi) south of Cairo. It is known chiefly for several Pyramids, two of which are Among the oldest, largest and best preserved in Egypt, built from 2613–2589 BCE.(1 hr Time Spent)\r\n\r\nThen Visit Memphis City, the ancient capital of Egypt, where the colossal statue of Ramses II and the great Alabaster Sphinx are. this City Dates back to 3100 B.C .(1 hr time Spent)\r\n\r\nAt the end of the Tour you will be transferred back to your Hotel', 'en'),
(287, 107, 'starts At 8:00 am Pickup from your hotel in Cairo/Giza By Emo Tours guide to Start your full day tour visiting Giza Pyramids where you Visit the Great Pyramids - Cheops, Chephren & Mykerinos then Visit the Valley Temple where the Priests Mummified the dead body of King Chephren Also there you will have a close-up look at The Sphinx - The legendary guardian that stands by the huge funeral complex with its lion body and the head of king Chephren.(2:30 hrs time Spent)\r\n\r\nLunch is included in between visits at Local Restaurant Based on Full Meal for each Person but Beverages are not included (1 Hr time Spent)\r\n\r\nThen Visit Sakkara, located only 27 km southwest away from Cairo. Visit the Step Pyramid (Built for king Zoser), it is considered an important part of the process of the pyramid evolution, which were made from the simple mastaba to its widely known form.(2 hrs Time Spent)\r\n\r\nThen Visit Dahshur Pyramids (Bent and Red Pyramids) Dahshur area is a royal necropolis located in the desert on the West Bank of the Nile approximately 40 kilometers (25 mi) south of Cairo. It is known chiefly for several Pyramids, two of which are Among the oldest, largest and best preserved in Egypt, built from 2613–2589 BCE.(1 hr Time Spent)\r\n\r\nThen Visit Memphis City, the ancient capital of Egypt, where the colossal statue of Ramses II and the great Alabaster Sphinx are. this City Dates back to 3100 B.C .(1 hr time Spent)\r\n\r\nAt the end of the Tour you will be transferred back to your Hotel', 'sp'),
(288, 108, 'Test tour', 'en'),
(289, 108, 'Test', 'sp'),
(290, 109, '<p>Test</p>', 'en'),
(291, 109, '<p>Test</p>', 'sp'),
(292, 110, 'DAY TRIP TO BAHARIYA OASIS VISIT BLACK AND WHITE DESERT FROM CAIRO', 'en'),
(293, 110, 'VIAJE DE UN DÍA AL OASIS DE BAHARIYA VISITA EL DESIERTO BLANCO Y NEGRO DESDE EL CAIRO', 'sp'),
(294, 111, '<p>Around 15-hour tour starts from 6 am to 10 pm. The tour includes pick-up and drop off transfers from customers&prime; location in Cairo/Giza, expert tour guide, private Jeep tour in the desert, breakfast and Bedouin lunch, entry fees, and Taxes.</p>\r\n\r\n<p>All Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The tour excludes personal items, tipping and any Optional Activities.</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Prices Quoted&nbsp;Per Person&nbsp;in U.S.D&nbsp;($)</strong></p>\r\n\r\n<p>Single Person = 240&nbsp;$</p>\r\n\r\n<p>From 2 to 2 People = 160 $</p>\r\n\r\n<p>From 3 to 5 People = 145 $&nbsp;</p>\r\n\r\n<p>From 6 to 10 People = 135 $&nbsp;</p>\r\n\r\n<p>Child from (6 to10.99&nbsp;Y )&nbsp; = 80 $&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>DAY TRIP TO BAHARIYA OASIS FROM CAIRO VISIT WHITE &amp; BLACK DESERT PROGRAM</h2>\r\n\r\n<p>The tour starts at 6 am. our tour guide and the driver will pick you up from the hotel in Cairo/Giza and transfer you to Bahariya Oasis. The Oasis is about 370 KM away from Cairo. It takes about 4 hours to reach there. Once arriving, you will change the vehicle and continue your journey by a 4X4 car to the Black Desert (40 KM away)</p>\r\n\r\n<h1>After that lunch will be served at a traditional restaurant where you can enjoy the view of the Valley of El Heize. Also you can swim and relax at the natural hot spring of Valley of El Haize before or after lunch.</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Then you will visit the quartz crystal at Crystal Mountain, and the rocky formations in the Valley of Agabat.</p>\r\n\r\n<p>And then the drive continues to the White Desert National Park the most well-known desert destination in Egypt &ndash; and for a good reason. The quantity of unearthly and beautiful wind-carved rock formations shaped in the form of giant mushrooms or pebbles is unequalled to in any desert in the world.</p>\r\n\r\n<p>Here the tour comes to an end and you will be transferred back to Cairo.</p>', 'en'),
(295, 111, '<p>El recorrido de alrededor de 15 horas comienza de 6 am a 10 pm. El recorrido incluye traslados de ida y vuelta desde la ubicaci&oacute;n de los clientes en El Cairo/Giza, gu&iacute;a tur&iacute;stico experto, recorrido privado en jeep por el desierto, desayuno y almuerzo beduino, tarifas de entrada e impuestos.<br />\r\nTodos los traslados se realizan en un veh&iacute;culo privado, con aire acondicionado, &uacute;ltimo modelo.</p>\r\n\r\n<p>El recorrido excluye art&iacute;culos personales, propinas y cualquier actividad opcional.</p>\r\n\r\n<p>Precios Cotizados Por Persona en U.S.D ($)<br />\r\nPersona soltera = 240 $<br />\r\nDe 2 a 3 Personas = 160 $<br />\r\nDe 4 a 6 Personas = 110 $<br />\r\nNi&ntilde;o de (6 a 10.99 a&ntilde;os) = 80 $</p>\r\n\r\n<p>VIAJE DE UN D&Iacute;A AL OASIS DE BAHARIYA DESDE EL CAIRO VISITE EL PROGRAMA DESIERTO BLANCO Y NEGRO<br />\r\nEl recorrido comienza a las 6 am. Nuestro gu&iacute;a tur&iacute;stico y el conductor lo recoger&aacute;n del hotel en El Cairo/Giza y lo trasladar&aacute;n al Oasis de Bahariya. El Oasis est&aacute; a unos 370 KM de El Cairo. Se tarda unas 4 horas en llegar all&iacute;. Una vez que llegue, cambiar&aacute; el veh&iacute;culo y continuar&aacute; su viaje en un autom&oacute;vil 4X4 hasta Black Desert (40 KM de distancia)<br />\r\nDespu&eacute;s de eso, el almuerzo se servir&aacute; en un restaurante tradicional donde podr&aacute; disfrutar de la vista del Valle de El Heize. Tambi&eacute;n puede nadar y relajarse en las aguas termales naturales del Valle de El Haize antes o despu&eacute;s del almuerzo.</p>\r\n\r\n<p>Luego visitar&aacute; el cristal de cuarzo en Crystal Mountain y las formaciones rocosas en el Valle de Agabat.<br />\r\nY luego el viaje contin&uacute;a hacia el Parque Nacional del Desierto Blanco, el destino des&eacute;rtico m&aacute;s conocido de Egipto, y por una buena raz&oacute;n. La cantidad de formaciones rocosas sobrenaturales y hermosas talladas por el viento en forma de hongos gigantes o guijarros no tiene igual en ning&uacute;n desierto del mundo.<br />\r\nAqu&iacute; el recorrido llega a su fin y ser&aacute; trasladado de regreso a El Cairo.</p>', 'sp');

-- --------------------------------------------------------

--
-- Table structure for table `transportations`
--

CREATE TABLE `transportations` (
  `id` int(10) UNSIGNED NOT NULL,
  `destination` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transportations`
--

INSERT INTO `transportations` (`id`, `destination`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, '97', '98', 120.00, '2019-06-28 05:07:43', '2019-06-28 05:07:43'),
(2, '99', '100', 175.00, '2019-06-28 06:24:18', '2019-06-28 06:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `transportation_prices`
--

CREATE TABLE `transportation_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `transportation_id` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transportation_prices`
--

INSERT INTO `transportation_prices` (`id`, `transportation_id`, `from`, `to`, `price`, `created_at`, `updated_at`) VALUES
(5, 1, 3, 6, 100.00, NULL, NULL),
(6, 1, 7, 10, 80.00, NULL, NULL),
(8, 2, 2, 3, 60.00, NULL, NULL),
(9, 2, 4, 7, 40.00, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` enum('admin','client') NOT NULL DEFAULT 'client',
  `phone` varchar(255) DEFAULT NULL,
  `security_code` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `phone`, `security_code`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@mummiestours.com', '$2y$10$2wfwYmyDokmfGsC7lmTtjODmysMqZrN5lC5NgF28II70b9X1trBsW', 'admin', NULL, NULL, 1, 'mtNSsew79AoAObT2QI8MjSYkqGWbZ6SV94rZN8bn7Gn4oziMCTnhLx8tbtv1', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Index 2` (`email`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_extras`
--
ALTER TABLE `order_extras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_extras`
--
ALTER TABLE `product_extras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_prices`
--
ALTER TABLE `product_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `translations_group_id_index` (`group_id`),
  ADD KEY `translations_locale_index` (`locale`);

--
-- Indexes for table `transportations`
--
ALTER TABLE `transportations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transportation_prices`
--
ALTER TABLE `transportation_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`,`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `order_extras`
--
ALTER TABLE `order_extras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_extras`
--
ALTER TABLE `product_extras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `product_prices`
--
ALTER TABLE `product_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- AUTO_INCREMENT for table `transportations`
--
ALTER TABLE `transportations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transportation_prices`
--
ALTER TABLE `transportation_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
