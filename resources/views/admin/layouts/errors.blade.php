
@if(count($errors)>0)
<div class="clearfix"><br/></div>
<div class="alert alert-danger  alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <ul style="margin-bottom:0;">
        @foreach($errors->all() as $error)
        <li>
            {{$error}}
        </li>
        @endforeach
    </ul>
</div>
@endif
@if (Request::session()->has('error')) 
<div class="clearfix"><br/></div>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    {{Request::session()->get('error')}}
</div>
@endif
@if (Request::session()->has('success')) 
<div class="clearfix"><br/></div>
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    {{Request::session()->get('success')}}
</div>
@endif
