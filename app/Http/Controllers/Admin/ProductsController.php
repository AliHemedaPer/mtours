<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Categories;
use App\Admin\Products;
use App\Admin\ProductPrices;
use App\Admin\ProductExtras;
use App\Admin\ProductImages;
use Validator;
use DB;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cat = $request->cat ?? '';
        $parent = $request->parent ?? '';

        if($cat){
            $products = Products::where('cat_id', $cat)->get();
        }else if($parent){
            $ids = Categories::where('parent_id', $parent)->pluck('id')->toArray();
            if(count($ids)){
                $products = Products::whereIn('cat_id', $ids)->get();
            }else{
                //i.e Hot Offers
                $products = Products::where('cat_id', $parent)->get();
            }
        }
        else{
            $products = Products::all();
        }
        return view('admin.products.index', compact('products', 'cat', 'parent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $cat = $request->cat ?? '';
        $parent = $request->parent ?? '';

        $product = NULL;
        $catIds = Categories::distinct('parent_id')->pluck('parent_id')->toArray();
        $categories = Categories::whereNotIn('id', $catIds)->get();
        
        $custome_prices = [ 0 => ['from' => '', 'to' => '', 'price' => '']];
        $opt_activities = (object)[ 0 => ['title' => ['en' => '', 'sp' => ''], 'price' => '']];

        //$parents = DB::table('categories as c')->select('c.id', 't.value')->leftjoin('translations as t', 'c.title' ,'=', 't.group_id')->get()->pluck('value', 'id')->toArray();
        return view('admin.products.form', compact('product', 'categories', 'custome_prices', 'opt_activities', 'cat', 'parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required',
                    'cat_id' => 'required',
                    'price' => 'required',
                    'day_type' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $data['hot_offer'] = $request->hot_offer == 'on' ? 1:0;
        $data['check_avail'] = $request->check_avail == 'on' ? 1:0;
        $data['days'] = $request->days ?? 1;

        $path = "uploads/products/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['image'] = $path . $file_name;
        }

        $product = Products::create($data);

        //Custome Prices
        $min_price = $data['price'];
        if(isset($request->cut_price) && count($request->cut_price) && $product->id){
            foreach ($request->cut_price as $key => $price_arr) {
                if($price_arr['from'] && $price_arr['to'] && $price_arr['price']){
                    ProductPrices::insert([
                        'product_id' => $product->id,
                        'from'       => $price_arr['from'],
                        'to'         => $price_arr['to'],
                        'price'      => $price_arr['price']
                    ]);

                    if($price_arr['price'] < $min_price)
                        $min_price = $price_arr['price'];
                }
            }
        }
        $product->update(['min_price' => $min_price]);
        ////////////////

        //Optional Activities
        if(isset($request->opt_act) && count($request->opt_act) && $product->id){
            foreach ($request->opt_act as $key => $opt_arr) {
                if($opt_arr['title']['en'] && $opt_arr['title']['sp']){
                    ProductExtras::create([
                        'product_id' => $product->id,
                        'title'       => array('en' => $opt_arr['title']['en'], 'sp' => $opt_arr['title']['sp']),
                        'price'      => $opt_arr['price'] ?? NULL
                    ]);
                }
            }
        }
        ////////////////
        $request->session()->flash('success', 'تم بنجاح!');

        $cat = $request->cat ? '?cat='.$request->cat : '';
        $parent = $request->parent ? '?parent='.$request->parent : '';

        return redirect('admin/products/'.$product->id.'/edit'.$cat.$parent);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $product, Request $request)
    {
        $cat    = $request->cat ?? '';
        $parent = $request->parent ?? '';

        $catIds = Categories::distinct('parent_id')->pluck('parent_id')->toArray();
        $categories = Categories::whereNotIn('id', $catIds)->get();

        $custome_prices = ProductPrices::where('product_id', $product->id)->get()->toArray();
        $opt_activities = ProductExtras::where('product_id', $product->id)->get();
        if(!count($custome_prices)){
            $custome_prices = [ 0 => ['from' => '', 'to' => '', 'price' => '']];
        }

        if(!count($opt_activities)){
            $opt_activities = (object)[ 0 => ['title' => ['en' => '', 'sp' => ''], 'price' => '']];
        }

        return view('admin.products.form', compact('product', 'custome_prices', 'opt_activities', 'categories', 'cat', 'parent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $product)
    {
        // print_r($request->all());
        // exit();
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required',
                    'cat_id' => 'required',
                    'price' => 'required',
                    'day_type' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $data['hot_offer'] = $request->hot_offer == 'on' ? 1:0;
        $data['check_avail'] = $request->check_avail == 'on' ? 1:0;
        $data['days'] = $request->days ?? 1;
        
        $path = "uploads/products/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($product->image)) {
                unlink($product->image);
            }
            $data['image'] = $path . $file_name;
        }
        $product->update($data);

        //Custome Prices
        $min_price = $data['price'];
        if(isset($request->cut_price) && count($request->cut_price) && $product->id){
            ProductPrices::where('product_id', $product->id)->delete();
            foreach ($request->cut_price as $key => $price_arr) {
                if($price_arr['from'] && $price_arr['to'] && $price_arr['price']){
                    ProductPrices::insert([
                        'product_id' => $product->id,
                        'from'       => $price_arr['from'],
                        'to'         => $price_arr['to'],
                        'price'      => $price_arr['price']
                    ]);

                    if($price_arr['price'] < $min_price)
                        $min_price = $price_arr['price'];
                }
            }
        }
        $product->update(['min_price' => $min_price]);
        ////////////////

        //Optional Activities
        if(isset($request->opt_act) && count($request->opt_act) && $product->id){
            //Do this way to delete translations records
            $currentExtras = DB::table('product_extras')->where('product_id', $product->id)->get();
            foreach ($currentExtras as $extra) {
                DB::table('translations')->where('group_id', $extra->title)->delete();
                ProductExtras::where('id', $extra->id)->delete();
            }

            foreach ($request->opt_act as $key => $opt_arr) {
                if($opt_arr['title']['en'] && $opt_arr['title']['sp']){
                    ProductExtras::create([
                        'product_id' => $product->id,
                        'title'       => array('en' => $opt_arr['title']['en'], 'sp' => $opt_arr['title']['sp']),
                        'price'      => $opt_arr['price'] ?? NULL
                    ]);
                }
            }
        }
        ////////////////

        $request->session()->flash('success', 'تم بنجاح!');

        $cat = $request->cat ? '?cat='.$request->cat : '';
        $parent = $request->parent ? '?parent='.$request->parent : '';

        return redirect('admin/products'.$cat.$parent);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $product)
    {
        if (file_exists($product->image)) {
            unlink($product->image);
        }
        $product->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/products');
    }

    public function fileUpload(Request $request)
    {
        $product_id = $request->pid;
        
        $path = "uploads/products/images/";
        if ($request->hasFile('file') && $product_id) {

            $new_pr_img = ProductImages::create(['product_id' => $product_id, 'image' => '']);

            $file = $request->file('file');
            $uniqueID = time().$new_pr_img->id;
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            
            if($file->move($path, $file_name)){
                $image_path = $path . $file_name;
                $new_pr_img->update(['image' => $image_path]);
            }else{
                $new_pr_img->delete();
                return false;
            }
            
        }else{
            return false;
        }
    }

    public function fileDelete(Request $request)
    {
        $product_id = $request->pid;
        $image_id   = $request->imgid;
        
        if ($image_id && $product_id) {
            $img = ProductImages::where('product_id', $product_id)->find($image_id);

            if($img){
                if (file_exists($img->image)) {
                    unlink($img->image);
                }
                if($img->delete())
                    $status = 'success';
                else
                    $status = 'failed1';
            }else{
                $status = 'failed2';
            }
        }else{
            $status = 'failed3';
        }

        return response()->json([ 'status' => $status ]); 
    }
}
