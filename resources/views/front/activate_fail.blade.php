@extends('front.front')

@section('content')
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
   

  <div class="sp-page">
    <div class="sp-page-a">
      <div class="">
          <div class="sp-page-lb">
            <div class="sp-page-p">
            <div class="booking-left">
              <h2 style="color: #ed5353">Account not Activated!</h2>
              
              <div class="comlete-alert">
                <div class="">
                  <b>Your Account is not Active.</b>
                  <br/>
                  <span>Please, try later or Contact support!</span>
                </div>
              </div>
              
              
              
            </div>
            </div>
          </div>
          <div class="clear"></div>
      </div>
    </div>

    <div class="clear"></div>
  </div>

    </div>  
  </div>  
</div>
@stop