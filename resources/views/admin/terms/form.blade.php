@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li class="active">
        Terms & Conditions
    </li>

</ol>
<div class="page-header">
    <h1>Terms & Conditions</h1>

</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">

                {!! Form::open(["url"=>"admin/terms","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
                <div class="tabbable">
                    <ul id="myTab" class="nav nav-tabs tab-bricky">
                        <li class="active">
                            <a href="#panel_tab1_example1" data-toggle="tab">
                                Title (EN)
                            </a>
                        </li>
                        <li class="">
                            <a href="#panel_tab2_example1" data-toggle="tab">
                                Title (SP)
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="panel_tab1_example1">
                            {!! Form::text('title[en]',($info)?$info->translate('en')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
                        </div>
                        <div class="tab-pane " id="panel_tab2_example1">
                            {!! Form::text('title[sp]',($info)?$info->translate('sp')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
                        </div>      
                    </div>
                </div>

                <div class="tabbable">
                    <ul id="myTab1" class="nav nav-tabs tab-bricky">
                        <li class="active">
                            <a href="#panel_tab1_example3" data-toggle="tab">
                                Details (EN)
                            </a>
                        </li>
                        <li class="">
                            <a href="#panel_tab2_example3" data-toggle="tab">
                                Details (SP)
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="panel_tab1_example3">
                            {!! Form::textarea('content[en]',($info)?$info->translate('en')->content:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'content']) !!}
                        </div>
                        <div class="tab-pane " id="panel_tab2_example3">
                            {!! Form::textarea('content[sp]',($info)?$info->translate('sp')->content:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'content']) !!}
                        </div>      
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-lg-12" style="text-align: right;">       
                        <a class="btn btn-danger" href="{{url("admin/dashboard")}}"><i class="icon-plus2 mr-2"></i>Cancel <i class="fa fa-minus-circle"></i></a>
                        <button type="submit" class="btn btn-success">Save <i class="fa fa-save"></i></button>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
@endsection