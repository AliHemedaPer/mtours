<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(
// [
// 	'prefix' => LaravelLocalization::setLocale(),
// 	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
// ],
// function()
// {
  Route::get('/', 'FrontController@index')->name('home');
  Route::get('/home', 'FrontController@index')->name('home');
  
  Route::get('/dashboard', function () {
      return redirect('/admin/dashboard');
  });
    
  Route::get('/about', 'FrontController@about')->name('about');
  Route::get('/terms', 'FrontController@terms')->name('terms');
  Route::get('/contact', 'FrontController@contact')->name('contact');
  Route::get('/services/{service}', 'FrontController@services')->name('services');
  
  Route::get('/tours/', 'FrontController@tours')->name('tours');
  Route::get('/tour/{tour}', 'FrontController@tour')->name('tour');
  Route::get('/tourbooking/{tour}', 'FrontController@tourbooking')->name('tourbooking');
  Route::post('/bookingaction/{id}', 'FrontController@bookingaction')->name('bookingaction');
  Route::get('/bookingconfirm/{order}', 'FrontController@bookingconfirm')->name('bookingconfirm');
  Route::get('/bookingsuccess', 'FrontController@booksuccess');
  Route::get('/orderconfirm/{order}', 'FrontController@orderconfirm')->name('orderconfirm');
  Route::post('/orderaction/{order}', 'FrontController@orderaction')->name('orderaction');

  Route::get('/blog', 'FrontController@blog')->name('blog');
  Route::get('/post/{post}', 'FrontController@post')->name('post');
  Route::get('/works', 'FrontController@works')->name('works');
  Route::get('/work/{work}', 'FrontController@work')->name('work');
  Route::get('/subcats/{catid}', 'FrontController@getSubcats');

  Route::get('/airport-transportations', 'FrontController@transportations')->name('transportations');
    Route::get('/transportation-booking/{transportation}', 'FrontController@transportationbooking')->name('transportationbooking');
  //Client Auth
  Route::post('create-user', 'ClientController@CreateUser');
  Route::get('activate-account/{code}', 'ClientController@activateAccount');
  Route::post('user-login', 'ClientController@UserLogin')->name('client.login.submit');
  Route::post('reset-password', 'ClientController@resetPassword');
  Route::post('logout', 'ClientController@logout')->name('clinet.logout');
  ////////////
// });



Route::get('locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
});
Route::post('/servicemail', 'FrontController@servicemail')->name('servicemail');
Route::post('/contactmail', 'FrontController@contactmail')->name('contactmail');
// Route::post('/joinmail', 'FrontController@joinmail')->name('joinmail');

// Route::get('/subcategories/{cat_id}', 'FrontController@getSubcats');
///////////Admin

Route::group(['prefix' => 'admin'], function () {
        

    
	  Auth::routes();
    Route::get('/logout', 'Auth\LoginController@logout');
});

Route::group(['prefix' => 'admin','middleware' => ['admin']], function () {
    Route::get('/dashboard', 'Admin\DashboardController@index');
    Route::get('/reports', 'Admin\DashboardController@reports');
    Route::resource('banners', 'Admin\BannerController');
    Route::get('/about', 'Admin\AboutController@index');
    Route::patch('/about', 'Admin\AboutController@update');

    Route::get('/terms', 'Admin\AboutController@termsIndex');
    Route::patch('/terms', 'Admin\AboutController@termsUpdate');

    Route::get('/settings', 'Admin\AboutController@settingsIndex');
    Route::patch('/settings', 'Admin\AboutController@settingsUpdate');

    Route::resource('/services', 'Admin\ServiceController');
    //Route::resource('integrations', 'Admin\IntegrationController');
    Route::resource('/categories', 'Admin\CategoriesController');
  
    Route::resource('/products', 'Admin\ProductsController');
    Route::post('/file-upload', 'Admin\ProductsController@fileUpload');
    Route::delete('/file-delete', 'Admin\ProductsController@fileDelete');
    //Route::resource('counters', 'Admin\CountersController');
    Route::resource('/posts', 'Admin\PostsController');
    Route::resource('/transportations', 'Admin\TransportationsController');

    Route::get('/reservations/{btype}', 'Admin\ReservationController@index');
    Route::get('/reservations/{btype}/{reservation}', 'Admin\ReservationController@show');
    Route::delete('/reservations/{btype}/{reservation}', 'Admin\ReservationController@destroy');
    Route::post('/updReservation', 'Admin\ReservationController@updReservation');
    Route::post('/updAvailability/{order}', 'Admin\ReservationController@updAvailability');

    Route::get('/clients', 'Admin\ClientsController@index')->name('clients');
    Route::get('/clients/mail', 'Admin\ClientsController@sendmail')->name('sendmail');
    Route::post('/clients/mail', 'Admin\ClientsController@sendaction')->name('sendaction');
    Route::delete('/clients/{client}', 'Admin\ClientsController@destroy')->name('clientdelete');
});
