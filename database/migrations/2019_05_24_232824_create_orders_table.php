<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('product_id');
            $table->integer('adults');
            $table->integer('childs')->nullable();
            $table->date('date_from');
            $table->date('date_to')->nullable();
            $table->string('name', 191);
            $table->string('email', 191);
            $table->string('phone', 191)->nullable();
            $table->string('passport', 191);
            $table->string('nationality', 50);
            $table->string('guide', 50);
            $table->string('hotel', 255)->nullable();
            $table->text('note', 65535);
            $table->double('total_price', 10, 2)->default(0.00);
            $table->string('payment_type', 5)->default('cash');
            $table->string('invoice_id', 191)->nullable();
            $table->tinyInteger('payment_status')->default(0);
            $table->enum('booking_type', ['tour', 'transportation']);
            $table->tinyInteger('order_done')->default(0);
            $table->tinyInteger('is_read')->default(0);
            $table->tinyInteger('is_available')->nullable()->default(NULL);
            $table->string('code', 191)->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
