@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script src="{{url('public/admin/')}}/assets/plugins/flot/jquery.flot.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/flot/jquery.flot.pie.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
<script src="{{url('public/admin/')}}/assets/js/index.js"></script>
@endsection