@extends('front.front')

@section('content')
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
    <div class="page-head">
      <div class="page-title">@lang('fnt.tours')</div>
      <!-- <div class="breadcrumbs">
        <a href="#">Home</a> / <a href="#">Tours</a> / <span>Grid page</span>
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="two-colls">
      <div class="srch-results-lbl fly-in" style="text-align: center;">
          <span>{{ $tours_count }} @lang('fnt.results') {!! isset(request()->query()['h_offer']) ? '<b style="color:#f53c32">['. __('fnt.hotoffers').' ]</b>' : '' !!}</span>
        </div>
      
      <div class="two-colls-left">
        <div class="two-colls-right-b">
          <div class="padding">
        
            
            <div class="catalog-row grid">
        <!-- // -->
          @foreach($tours as $tour)
          <div class="offer-slider-i catalog-i tour-grid fly-in" style="min-height: 430px;">
              <a href="{{ url('tour/'.$tour->id)}}" class="offer-slider-img">
                <img alt="" src="{{ $tour->image }}">
                <span class="offer-slider-overlay">
                  <span class="offer-slider-btn">@lang('fnt.details')</span><span></span>
                </span>
              </a>
              <div class="offer-slider-txt">
                <div class="offer-slider-link"><a href="{{ url('tour/'.$tour->id)}}">{{ $tour->translate($share_locale)->title }}</a></div>

                <div class="offer-slider-l">
                  @if($tour->categories)
                  <div class="offer-slider-location">{{ $tour->categories->translate($share_locale)->title }} </div>
                  @endif
                  <div class="offer-slider-location">@lang('fnt.views'): 
                    {{ $tour->views }} 
                  </div>
                  <!-- <nav class="stars">
                    <ul>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-a.png" /></a></li>
                    </ul>
                    <div class="clear"></div>
                  </nav> -->
                </div>

                <div class="offer-slider-r">
                  <b>{{ $tour->displayPrice() }} {{ $app_settings['app_currency'] }}</b>
                  <span>@lang('fnt.pricing')</span>
                </div>
                <div class="offer-slider-devider"></div>                
                <div class="clear"></div>
                      <div class="offer-slider-lead">{!! \Illuminate\Support\Str::words( strip_tags($tour->translate($share_locale)->description), 15, ' ...') !!}</div>
              <a href="{{ url('tourbooking/'.$tour->id) }}" class="cat-list-btn " style="    width: auto;
    padding: 9px;">{{ $tour->check_avail ? __('fnt.ch_avail') : __('fnt.book') }}</a>
                    </div>
            </div>
          @endforeach
          <!-- \\ -->       
            </div>
            <div class="clear"></div>

            @if( $tours_count > $pg_count)
              {{ $tours->appends(request()->query())->links('front.pagination') }}
            @endif

          </div>
        </div>
        
      </div>
      <div class="two-colls-right">
        <form method='GET'>
        <div class="side-block fly-in">
          <div class="side-block-search">
            <div class="page-search-p" style="border: 1px solid #c9d9e6;">
              <h4 style="margin-top: 0;color: #537896;">@lang('fnt.advanced')</h4>
            <!-- // -->
            <div class="srch-tab-line">
              <div class="transformed">
                <label>@lang('fnt.ky')</label>
                <div class="input-a">
                <input type="text" name="key" value="{{ request()->query()['key'] ?? '' }}" placeholder="@lang('fnt.typ')">
                </div>  
              </div>
              <div class="clear"></div>
            </div>

            <div class="srch-tab-line">
              <div class="transformed">
                <label>@lang('fnt.cat')</label>
                <div class="select-wrapper">
                <select class="custom-select" name='p' id="cat_id">
                  <option value="">--</option>
                  @foreach($share_categories as $category)
                    @if($category->parent_id == 0)
                      <option value="{{ $category->id }}" {{ (isset(request()->query()['p']) && request()->query()['p'] == $category->id) ? 'selected' : '' }}>{{ $category->translate($share_locale)->title }}</option>
                    @endif  
                  @endforeach
                </select>
                </div>  
              </div>
              <div class="clear"></div>
            </div>

            <div class="srch-tab-line">
              <div class="transformed" id="subcategory_trs">
                <label>@lang('fnt.scat')</label>
                <div class="select-wrapper">
                <select class="custom-select"  name='c' id="subcat_id">
                  <option value="">--</option>
                </select>
                </div>  
              </div>
              <div class="clear"></div>
            </div>
            <!-- \\ -->

            <div class="srch-tab-line">
              <div class="srch-tab-left transformed">
                <label>@lang('fnt.type')</label>
                <div class="select-wrapper">
                <select class="custom-select" name='typ' onchange="type_change(this)">
                  <option value="">--</option>
                  <option value="full" {{ (isset(request()->query()['typ']) && request()->query()['typ'] == 'full') ? 'selected' : '' }}>@lang('fnt.full')</option>
                  <option value="half" {{ (isset(request()->query()['typ']) && request()->query()['typ'] == 'half') ? 'selected' : '' }}>@lang('fnt.half')</option>
                  <option value="days" {{ (isset(request()->query()['typ']) && request()->query()['typ'] == 'days') ? 'selected' : '' }}>@lang('fnt.days')</option>
                </select>
                </div>  
              </div>
              <div class="srch-tab-right transformed">
                <label># @lang('fnt.days')</label>
                <div class="input-a" id="dy_div" style="background:#{{ (isset(request()->query()['typ']) && request()->query()['typ'] == 'days') ? 'fff' : 'efefef' }} ">
                <input type="text" name="days" id="dy_num" value="{{ request()->query()['days'] ?? '' }}" placeholder="" {{ (isset(request()->query()['typ']) && request()->query()['typ'] == 'days') ? '' : 'disabled=""' }}>
                </div>  
              </div>
              <div class="clear"></div>
            </div>
            <!-- // -->
            <!-- <div class="srch-tab-line">
              <div class="srch-tab-left">
                <label>From</label>
                <div class="input-a"><input type="text" name="from" value="{{ request()->query()['from'] ?? '' }}" class="date-inpt" placeholder="mm/dd/yy"> <span class="date-icon"></span></div>  
              </div>
              <div class="srch-tab-right">
                <label>To</label>
                <div class="input-a"><input type="text" name="to" value="{{ request()->query()['to'] ?? '' }}" class="date-inpt" placeholder="mm/dd/yy"> <span class="date-icon"></span></div>  
              </div>
              <div class="clear"></div>
            </div> -->
            <!-- \\ -->
            <div class="srch-tab-line">
              <div class="transformed">
                <label>@lang('fnt.pr_sort')</label>
                <div class="select-wrapper">
                <select class="custom-select" name='prs'>
                  <option value="">--</option>
                  <option value="ASC" {{ (isset(request()->query()['prs']) && request()->query()['prs'] == 'ASC') ? 'selected' : '' }}>@lang('fnt.asc')</option>
                  <option value="DESC" {{ (isset(request()->query()['prs']) && request()->query()['prs'] == 'DESC') ? 'selected' : '' }}>@lang('fnt.desc')</option>
                </select>
                </div>  
              </div>
              <div class="clear"></div>
            </div>
            <!-- \\ --> 

            <!-- // side // -->
        
                @if($is_mobile)
                  <div class="srch-tab-line" style="width: 48%; float: left;">
                    <div class="transformed">
                      <label>@lang('fnt.pr_from')</label>
                      <div class="input-a">
                      <input type="text" name="pr_from" value="{{ request()->query()['pr_from'] ?? 300 }}" placeholder="">
                      </div>  
                    </div>
                    <div class="clear"></div>
                  </div>

                  <div class="srch-tab-line" style="width: 48%; float: right;">
                    <div class="transformed">
                      <label>@lang('fnt.pr_to')</label>
                      <div class="input-a">
                      <input type="text" name="pr_to" value="{{ request()->query()['pr_to'] ?? 2000 }}">
                      </div>  
                    </div>
                    <div class="clear"></div>
                  </div>
                @else
                  <div class="side-block fly-in">
                  <div class="side-price">
                    <div class="side-padding">
                        <div class="side-lbl">@lang('fnt.price')</div>
                        <div class="price-ranger">
                          <div id="slider-range"></div>              
                        </div>
                        <div class="price-ammounts">
                          <input type="text" id="ammount-from"  readonly>
                          <input type="text" id="ammount-to"  readonly>

                          <input type="hidden" id="ammount-from-hid" name="pr_from">
                          <input type="hidden" id="ammount-to-hid" name="pr_to">
                          <div class="clear"></div>
                        </div>
                        </div>
                  </div>  
                </div>
                @endif
              
          <!-- \\ side \\ -->

          <button type="submit" class="srch-btn">@lang('fnt.srch')</button> 
        </div>  
                   
          </div>          
        </div>
        </form>
      </div>
    </div>
    <div class="clear"></div>
    
    </div>  
  </div>  
</div>
@endsection

@section('javascript')
  <script>
    $(document).ready(function(){
    'use strict';
      (function($) {
        $(function() {
          $('input:checkbox,input:radio,.search-engine-range-selection-container input:radio').styler();
        })
      })(jQuery);   
      
    var slider_range = $("#slider-range");
    var ammount_from = $("#ammount-from");
    var ammount_to = $("#ammount-to");
    var ammount_from_hid = $("#ammount-from-hid");
    var ammount_to_hid = $("#ammount-to-hid");
    
      $(function() {
        slider_range.slider({
          range: true,
          min: 0,
          max: 5000,
          values: [ {{ request()->query()['pr_from'] ?? 300 }}, {{ request()->query()['pr_to'] ?? 2000 }} ],
          slide: function( event, ui ) {
          ammount_from.val(ui.values[0]+" {{ $app_settings['app_currency'] }}");
          ammount_to.val(ui.values[1]+" {{ $app_settings['app_currency'] }}");

          ammount_from_hid.val(ui.values[0]);
          ammount_to_hid.val(ui.values[1]);
          }
        });
        ammount_from.val(slider_range.slider("values",0)+" {{ $app_settings['app_currency'] }}");
        ammount_to.val(slider_range.slider("values",1)+" {{ $app_settings['app_currency'] }}");

        ammount_from_hid.val(slider_range.slider("values",0));
        ammount_to_hid.val(slider_range.slider("values",1));
      });


    $(".side-time").each(function(){
          var $this = $(this);
          $this.find('.time-range').slider({
              range: true,
              min: 0,
              max: 24,
              values: [ 3, 20 ],
              slide: function( event, ui ) {
                $this.find(".time-from").text(ui.values[0]);
                $this.find(".time-to").text(ui.values[1]);
              }
          });
          $(this).find(".time-from").text($this.find(".time-range").slider("values",0));
          $(this).find(".time-to").text($this.find(".time-range").slider("values",1));
        });
    });

    function type_change(obj){
      if($(obj).val() == 'days'){
        $('#dy_div').css('background', '#fff');
        $('#dy_num').removeAttr('disabled');
      }else{
        $('#dy_div').css('background', '#efefef');
        $('#dy_num').attr('disabled', 'disabled');
        $('#dy_num').val('');
      }
    }
  </script>
  @endsection