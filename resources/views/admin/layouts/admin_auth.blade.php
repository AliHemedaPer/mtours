<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/fonts/style.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/main.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/main-responsive.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/theme_light.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/print.css" type="text/css" media="print"/>
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/rtl-version.css">
        <!--[if IE 7]>
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    </head>
    <!-- end: HEAD -->
   	<!-- start: BODY -->
	<body class="login example1 rtl">
		<div class="main-login col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="logo">
			</div>
                     @yield('content')
                        <!-- start: COPYRIGHT -->
			<div class="copyright">
				{{date('Y')}} &copy; {{ config('app.name', 'Laravel') }}.
			</div>
			<!-- end: COPYRIGHT -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="{{url('public/admin/')}}/assets/plugins/respond.min.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="{{url('public/admin/')}}/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="{{url('public/admin/')}}/assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="{{url('public/admin/')}}/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="{{url('public/admin/')}}/assets/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
			});
		</script>
	</body>
	<!-- end: BODY -->
</html>