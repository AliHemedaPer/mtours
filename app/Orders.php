<?php

namespace App;
use App\Admin\Products;
use App\Admin\Transportations;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = ['user_id', 'product_id', 'adults', 'childs','date_from', 'date_to', 'name', 'email', 'phone', 'passport', 'nationality', 'guide', 'note', 'hotel', 'total_price', 'payment_type' , 'payment_status', 'booking_type', 'order_done', 'is_read', 'is_available', 'code'];
    
    /**
    * The "booting" method of the model.
    *
    * @return void
    */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::deleting(function ($order) {
    //         $order->orderExtras->each->delete();
    //     });
    // }

	public function products()
    {
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }

    public function transportations()
    {
        return $this->belongsTo(Transportations::class, 'product_id', 'id');
    }

    public function orderExtras()
    {
        return $this->hasMany(OrderExtras::class, 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
