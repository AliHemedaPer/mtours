@extends('front.front')

@section('content')
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
    <div class="page-head">
      <div class="page-title">{{ $tour->translate($share_locale)->title }}</span></div>

      <div class="breadcrumbs">
        <div class="h-stars-lbl">{{ $tour->views }} @lang('fnt.view')</div>
      </div>
      <div class="clear"></div>
    </div>

  <div class="sp-page">
  

    <div class="sp-page-a">
      <div class="sp-page-l">
          <div class="sp-page-lb">
          <div class="sp-page-p">
            <div class="mm-tabs-wrapper">
            <!-- // tab item // -->
              @if( $tour->productImages->count() > 0 )
              <div class="tab-item">
                <div class="tab-gallery-big">
                  <img alt="" src="../{{ $tour->productImages->first()->image }}">
                </div>
                <div class="tab-gallery-preview">
                  <div id="gallery">
                    @foreach($tour->productImages as $img)
                    <!-- // -->
                      @if(file_exists($img->image))
                      <div class="gallery-i @if($loop->index == 0) active @endif">
                        <a href="../{{ $img->image }}"><img alt="" src="../{{ $img->image }}" style="max-width: 115px;" width="100%"><span></span></a>
                      </div>
                      @endif
                    <!-- \\ -->
                    @endforeach
                    
                  </div>
                </div>
              </div>
              @else
                @if(file_exists($tour->image))
                <div class="tab-item">
                  <div class="tab-gallery-big">
                    <img alt="" src="../{{ $tour->image }}">
                  </div>
                </div>
                @endif
              @endif
            <!-- \\ tab item \\ --> 
                    
          </div>
          
          <div class="content-tabs">
            <div class="content-tabs-head last-item">
              <nav>
                <ul>
                  <li><a class="active" href="#">@lang('fnt.desc')</a></li>
                  <!-- <li><a href="#">Prices</a></li> -->
                  @if($tour->productExtras->count())
                  <li><a href="#">@lang('fnt.opts')</a></li>
                  @endif
                  <li></li>
                </ul>
              </nav>
              
              <div class="clear"></div>
            </div>
            <div class="content-tabs-body">
              <!-- // content-tabs-i // -->
              <div class="content-tabs-i">
                {!! $tour->translate($share_locale)->description !!}
              </div>
              <!-- \\ content-tabs-i \\ -->
              <!-- // Prices // -->
              
              <!-- <div class="content-tabs-i">
                <h2>(Price per Person)</h2>
                <ul class="preferences-list">
                  <li class="checked">Single Person: <strong>({{ $tour->price }} {{ $app_settings['app_currency'] }})</strong></li>
                  @foreach($tour->productPrices as $prod_price)
                  <li class="checked">Form {{ $prod_price->from }} to {{ $prod_price->to }} Persons: <strong>({{ $prod_price->price }} {{ $app_settings['app_currency'] }}/Person)</strong></li>
                  @endforeach

                  @if($tour->child_from && $tour->child_to && $tour->child_price)
                  <li class="checked">Children (from {{ $tour->child_from }} to {{ $tour->child_to }} Years old):: <strong>({{ $tour->child_price }} {{ $app_settings['app_currency'] }}/Child)</strong></li>
                  @endif
                </ul>
                
                <div class="clear"></div>
              </div> -->
              <!-- \\ content-tabs-i \\ --> 
              <!-- // Opttional Actitivies // -->
              @if($tour->productExtras->count())
              <div class="content-tabs-i">
                <h2>(@lang('fnt.pr_prsn'))</h2>
                <ul class="preferences-list">
                  @foreach($tour->productExtras as $extra)
                  <li class="checked">{!! $extra->translate($share_locale)->title !!} <strong>({{ $extra->price ? $extra->price.' '.$app_settings['app_currency'].'/'.__('fnt.person') : __('fnt.prc_at_cost') }})</strong></li>
                  @endforeach
                </ul>
                
                <div class="clear"></div>
              </div>
              @endif
              <!-- \\ content-tabs-i \\ -->              
            </div>
          </div>

          </div>
          
          </div>
          <div class="clear"></div>
      </div>
    </div>

    <div class="sp-page-r">
      <div class="h-detail-r">
        <div class="h-detail-lbl">
          <div class="h-detail-lbl-a">{{ $from_price }} {{ $app_settings['app_currency'] }}</div>
          <div class="h-detail-lbl-b" style="    color: #246ba2;">@lang('fnt.from')</div>
          
          <div class="h-detail-lbl-b">
            <ul class="preferences-list">
              <!-- <li class="checked">Single Person: <strong>({{ $tour->price }} {{ $app_settings['app_currency'] }})</strong></li> -->
              @foreach($prices as $prod_price)
              <li class="checked">@lang('fnt.from') {{ $prod_price->from }} @lang('fnt.to') {{ $prod_price->to }} @lang('fnt.persons'): <strong>({{ $prod_price->price }} {{ $app_settings['app_currency'] }}/@lang('fnt.person'))</strong></li>
              @endforeach
              <li class="checked">@lang('fnt.s_person'): <strong>({{ $tour->price }} {{ $app_settings['app_currency'] }})</strong></li>

              @if($tour->child_from && $tour->child_to && $tour->child_price)
              <li class="checked">@lang('fnt.child') (@lang('fnt.from') {{ $tour->child_from }} @lang('fnt.to') {{ $tour->child_to }} @lang('fnt.old')): <strong>({{ $tour->child_price }} {{ $app_settings['app_currency'] }}/@lang('fnt.child'))</strong></li>
              @endif
            </ul>
          </div>
        </div>
        @if($tour->date_from || $tour->date_to )
        <div class="h-tour">
          <!-- <div class="tour-item-icons">
            <img alt="" src="img/tour-icon-01.png">
            <span class="tour-item-plus"><img alt="" src="{{ asset('public/front')}}/img/tour-icon.png"></span>
            <img alt="" src="img/tour-icon-02.png">
          </div> -->
          <div class="tour-icon-txt">{{ $tour->date_from ? $tour->date_from : '' }} {{ $tour->date_to ? '- '.$tour->date_to : '' }}</div>
          <div class="clear"></div>
        </div>
        @endif
        
        <div class="h-detail-stars" style="clear: both;padding: 17px 0px 5px 0px;">
          <!-- <ul class="h-stars-list">
            <li><a href="#"><img alt="" src="img/hd-star-b.png"></a></li>
            <li><a href="#"><img alt="" src="img/hd-star-b.png"></a></li>
            <li><a href="#"><img alt="" src="img/hd-star-b.png"></a></li>
            <li><a href="#"><img alt="" src="img/hd-star-b.png"></a></li>
            <li><a href="#"><img alt="" src="img/hd-star-a.png"></a></li>
          </ul> -->
          
          <div class="clear"></div>
        </div>
      
        <a href="{{ url('tourbooking/'.$tour->id) }}" class="book-btn">
          <span class="book-btn-l"><i></i></span>
          <span class="book-btn-r">{{ $tour->check_avail ? __('fnt.ch_avail') : __('fnt.book') }}</span>
          <div class="clear"></div>
        </a>
      </div>
      
      <div class="h-help">
        <div class="h-help-lbl">@lang('fnt.help')</div>
        <div class="h-help-lbl-a">@lang('fnt.h_help')</div>
        <div class="h-help-phone">{{ $app_settings['app_phone'] }}</div>
        <div class="h-help-email">{{ $app_settings['app_email'] }}</div>
      </div>
      
      <div class="h-liked">
        <div class="h-liked-lbl">@lang('fnt.also_like')</div>
        <div class="h-liked-row">
          <!-- // -->
          @foreach($related as $rel)
          <div class="h-liked-item">
          <div class="h-liked-item-i">
            <div class="h-liked-item-l">
                <a href="{{ url('tour/'.$rel->id)}}"><img alt="" src="../{{ $rel->image }}" style="max-width: 100px"></a>
            </div>
          <div class="h-liked-item-c">
              <div class="h-liked-item-cb">
                <div class="h-liked-item-p">
                <div class="h-liked-title"><a href="{{ url('tour/'.$rel->id)}}">{{ $rel->translate($share_locale)->title }}</a></div>
                <div class="h-liked-rating">
                 <nav class="stars">
                  <!-- <ul>
                    <li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
                    <li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
                    <li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
                    <li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
                    <li><a href="#"><img alt="" src="img/star-a.png" /></a></li>
                  </ul> -->
                  <div class="h-stars-lbl">{{ $rel->views }} @lang('fnt.views')</div>
                  <div class="clear"></div>
                 </nav>
                </div>
                <div class="h-liked-foot">
                  <span class="h-liked-price">{{ $rel->displayPrice() }} {{ $app_settings['app_currency'] }}</span>
                  <span class="h-liked-comment">@lang('fnt.pricing')</span>
                </div>
                </div>
              </div>
            <div class="clear"></div>
          </div>
          </div>
          <div class="clear"></div> 
          </div>
          @endforeach
          <!-- \\ -->

        </div>      
      </div>
           
      
    </div>
    <div class="clear"></div>
  </div>

    </div>  
  </div>  
</div>
@endsection
@section('javascript')
  <script>
    $(document).ready(function(){
    'use strict';
    $('.review-ranger').each(function(){    
      var $this = $(this);
      var $index = $(this).index();
      if ( $index=='0' ) {
      var $val = '3.0'
      } else if ( $index=='1' ) {
      var $val = '3.8'
      } else if ( $index=='2' ) {
      var $val = '2.8'
      } else if ( $index=='3' ) {
      var $val = '4.8'
      } else if ( $index=='4' ) {
      var $val = '4.3'
      } else if ( $index=='5' ) {
      var $val = '5.0'
      }
      $this.find('.slider-range-min').slider({
      range: "min",
      step: 0.1,
      value: $val,
      min: 0.1,
      max: 5.1,
      create: function( event, ui ) {
        $this.find('.ui-slider-handle').append('<span class="range-holder"><i></i></span>');
      },
      slide: function( event, ui ) {
        $this.find(".range-holder i").text(ui.value);
      }
      });
      $this.find(".range-holder i").text($val);
    });

      
        $('#gallery').bxSlider({
          infiniteLoop: true,
          speed: 500,
          slideWidth: 108,
          minSlides: 1,
          maxSlides: 6,
          moveSlides: 1,
          auto: false,
          slideMargin: 7,
          @if($is_mobile)
            touchEnabled: true
          @else
            touchEnabled: false
          @endif   
      });
    });
  </script>
  @endsection