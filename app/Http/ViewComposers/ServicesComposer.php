<?php 

namespace App\Http\ViewComposers;

use App;
use Illuminate\Contracts\View\View;
use App\Admin\Service;
use App\Admin\Products;
use App\Admin\Categories;
use App\Admin\Setting;

class ServicesComposer
{
    public function compose(View $view) {
    	$locale        = App::getLocale();
        $settings      = Setting::where('id', 1)->get();
    	//$services      = Service::get();
    	$categories    = Categories::get();
    	$recent_viewed = Products::orderBy('updated_at', 'desc')->limit(4)->get();

        //$view->with('share_services', $services);
        $view->with('share_categories', $categories);
        $view->with('share_locale', $locale);
        $view->with('recent_vi', $recent_viewed);
        $view->with('app_settings', $settings[0]);
    }
}