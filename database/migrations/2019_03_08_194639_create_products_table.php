<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('cat_id');
            $table->string('title');
            $table->text('description', 65535);
            $table->double('price', 10, 2);
            $table->double('min_price', 10, 2)->default(0);
            $table->integer('child_from')->nullable();
            $table->integer('child_to')->nullable();
            $table->double('child_price', 10, 2)->nullable();
            $table->enum('day_type', ['half', 'full', 'days']);
            $table->integer('days')->default(0);
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->integer('views')->default(0);
            $table->integer('booked')->default(0);
            $table->string('image')->nullable();
            $table->tinyInteger('hot_offer')->default(0);
            $table->tinyInteger('check_avail')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
