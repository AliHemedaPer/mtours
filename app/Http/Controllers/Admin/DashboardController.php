<?php

namespace App\Http\Controllers\Admin;

use App\Orders;
use App\Admin\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index() {
    	$tours  = Products::count();
    	$resrv  = Orders::count();
    	$profit = Orders::sum('total_price');

        $reservations = Orders::where('order_done', 0)->latest()->limit(5)->get();
        return view('admin.dashboard', compact('reservations', 'tours', 'resrv', 'profit'));
    }

    public function reports(Request $request) {

    	$days = ['0' => '--- All ---', '01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31'];
    	$monthes = ['0' => '--- All ---', '01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Des'];
      	$years   = ['0' => '--- All ---', '2019' => 2019, '2020' => 2020, '2021' => 2021, '2022' => 2022, '2023' => 2023, '2024' => 2024, '2025' => 2025, '2026' => 2026, '2026' => 2026];

      	$day   = $request->day ?? date('d');
      	$month = $request->month ?? date('m');
      	$year  = $request->year ?? date('Y');

        $reservQuery = Orders::where('id', '>', 0);
        if($day){
          $reservQuery->whereDay('date_from', '=', $day);
          $reservQuery->orWhere(function($query) use ($day){
               $query->where('date_to', '!=', null);
               $query->whereDay('date_from', '>=', $day);
               $query->whereDay('date_to', '<=', $day);
           });
        }
        if($month){
          $reservQuery->whereMonth('date_from', '=', $month);
          $reservQuery->orWhere(function($query) use ($month){
               $query->where('date_to', '!=', null);
               $query->whereMonth('date_from', '>=', $month);
               $query->whereMonth('date_to', '<=', $month);
           });
        }
        if($year){
          $reservQuery->whereYear('date_from', '=', $year);
          $reservQuery->orWhere(function($query) use ($year){
               $query->where('date_to', '!=', null);
               $query->whereYear('date_from', '>=', $year);
               $query->whereYear('date_to', '<=', $year);
           });
        }

      	$resrv  = $reservQuery->count();
      	$profit  = $reservQuery->sum('total_price');

        $reservations = $reservQuery->get();

        return view('admin.reports', compact('reservations', 'resrv', 'profit', 'days', 'day', 'monthes', 'month', 'years', 'year'));
    }
}
