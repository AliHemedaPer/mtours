<?php

namespace App\Http\Controllers;

use App;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Admin\Service;
use App\Admin\Banner;
use App\Admin\About;
use App\Admin\Works;
use App\Admin\Products;
use App\Admin\ProductPrices;
use App\Admin\ProductExtras;
use App\Admin\Counters;
use App\Admin\Posts;
use App\Admin\Integration;
use App\Admin\Team;
use App\Admin\Categories;
use App\Admin\Transportations;

use App\Orders;
use App\OrderExtras;
use App\Clients;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;

class FrontController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
        $ishome     = 1;
        $banners    = Banner::get();
        $about      = About::find(1);
        $services   = Service::get();
        $posts      = Posts::limit(4)->latest()->get();
        $populars   = Categories::where('in_home', 1)->get();
        $hotoffers  = Products::where('hot_offer', 1)->limit(20)->get();
        ////
        return view('front.home')->with(compact('ishome', 'banners', 'services', 'about', 'posts', 'populars', 'hotoffers'));
    }

    public function about()
    {
        $about = About::find(1);
        $inner_body = 1;

        return view('front.about')->with(compact('about', 'inner_body'));
    }

    public function terms()
    {
        $terms = About::find(2);
        $inner_body = 1;
        
        return view('front.terms')->with(compact('terms', 'inner_body'));
    }

    public function contact()
    {
        $inner_body = 1;
        return view('front.contact')->with(compact('inner_body'));
    }

    public function services(Service $service)
    {
        $lang = App::getLocale();
        $top_slider_img = 'slider-img';
        $title = explode(' ', $service->translate($lang)->title);
        
        //used in master
        $bannerBkg = $service->banner_image ? $service->banner_image : null;
        ///////////////

        return view('front.service')->with(compact('service', 'title', 'bannerBkg',  'top_slider_img'));
    }

    public function tours(Request $request)
    {
        if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])){
            $is_mobile = true;
        }else{
            $is_mobile = false;
        }

        $lang = App::getLocale();

        $key     = $request->key ?? '';
        $cat     = $request->c ?? '';
        $prt     = $request->p ?? '';
        $h_offer  = $request->h_offer ?? '';
        $typ      = $request->typ ?? ''; //Day Type 
        $days      = $request->days ?? ''; //Day Type 
        //$from    = $request->from ?? ''; 
        //$to      = $request->to ?? ''; 
        $pr_from = $request->pr_from ?? ''; 
        $pr_to   = $request->pr_to ?? ''; 
        $prs     = $request->prs ?? ''; 

        $toursQuery = new Products;

        if($key){
            $toursQuery = $toursQuery->leftjoin('translations as t','t.group_id', '=', 'products.title')
                                    ->where('t.locale', App::getLocale())
                                    ->where('t.value', 'LIKE', "%{$key}%");
        }

        //filter Category
        if($cat){
            $toursQuery = $toursQuery->where('cat_id', $cat);
        }else if($prt){
            $ids = Categories::where('parent_id', $prt)->pluck('id')->toArray();
            if(count($ids)){
                $toursQuery = $toursQuery->whereIn('cat_id', $ids);
            }else{
                //i.e Hot Offers
                $toursQuery = $toursQuery->where('cat_id', $prt);
            }
        }
        /////////////////
        if($h_offer)
            $toursQuery = $toursQuery->where('hot_offer', 1);

        if($days)
            $toursQuery = $toursQuery->where('days', $days);
        else if($typ)
            $toursQuery = $toursQuery->where('day_type', $typ);

        /*if($from)
            $toursQuery = $toursQuery->where('date_from', '>=', $from);

        if($to)
            $toursQuery = $toursQuery->where('date_to', '<=', $to);*/

        if($pr_from)
            $toursQuery = $toursQuery->where('min_price', '>=', $pr_from);

        if($pr_to)
            $toursQuery = $toursQuery->where('min_price', '<=', $pr_to);

        if($prs)
            $toursQuery = $toursQuery->orderBy('min_price', $prs);

        $tours_count = $toursQuery->count();

        $pg_count = 9;
        $tours = $toursQuery->paginate($pg_count);

        return view('front.tours')->with(compact('tours', 'tours_count', 'pg_count', 'is_mobile'));
    }

    public function tour(Products $tour)
    {
        if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])){
            $is_mobile = true;
        }else{
            $is_mobile = false;
        }

        $related = Products::where('id', '!=', $tour->id)->inRandomOrder()->limit(5)->get();
        $tour::where('id', $tour->id)->increment('views');
        $prices = ProductPrices::where('product_id', $tour->id)->orderBy('price', 'ASC')->get();

        $from_price = $tour->price;
        foreach ($prices as $price) {
            if($price->price < $from_price)
                $from_price = $price->price;
        }
        return view('front.tour')->with(compact('tour', 'related', 'prices', 'is_mobile', 'from_price'));
    }

    public function tourbooking(Products $tour, Request $request)
    {
        $tourCashedData = [];
        if(session()->get('tourCashedData')){
            $tourCashedData = session()->get('tourCashedData');
            \Session::forget('tourCashedData');
        }
        
        $pricesJson = $tour->productPrices->toArray();
        return view('front.tourbook')->with(compact('tour', 'pricesJson', 'tourCashedData'));
    }

    public function transportationbooking(Transportations $transportation)
    {
        $tourCashedData = [];
        if(session()->get('tourCashedData')){
            $tourCashedData = session()->get('tourCashedData');
            \Session::forget('tourCashedData');
        }

        $pricesJson = $transportation->transportationPrices->toArray();
        return view('front.transportationbook')->with(compact('transportation', 'pricesJson', 'tourCashedData'));
    }

    //// Confirm Tour booking using tour code
    public function orderconfirm(Orders $order, Request $request)
    {
        if($order->code != $request->code || $order->is_available != 1)
            return redirect('home');

        $tour = Products::where('id', $order->product_id)->first();
        $orderExtras = OrderExtras::where('order_id', $order->id)->get();

        return view('front.tourconfirm')->with(compact('order', 'tour', 'orderExtras'));
    }

    public function orderaction(Orders $order, Request $request){
        $payment = $request->payment_type;
        
        $product = Products::find($order->product_id)->first();
        if(!$product){
            $request->session()->flash('error', 'Not Found!');
            return redirect('orderconfirm/'.$order->id.'?code='.$order->code);
        }

        $product_name = $product->translate('en')->title;

        //#############  Create Order
        if(!$order){
            $request->session()->flash('error', 'Booking Error, Please contact Website Admin!');

        }
        ////////////////////////

        //############# Payment Proccess  
        if($payment == 'visa'){
            $order->update(['payment_type' => 'visa']);

            $appSettings = DB::table('settings')->select(['app_fees', 'fawaterak_key'])->where('id', 1)->first();

            $postData = [
                'vendorKey' => $appSettings->fawaterak_key,
                'cartItems' => [ array(
                                        'name'     => $product_name, 
                                        'price'    => $order->total_price, 
                                        'quantity' => 1
                                    ) 
                               ],
                'cartTotal' => $order->total_price, 
                'shipping' => 0,
                'customer' => [ 'first_name' => $order->name, 
                                'last_name'  => $order->name, 
                                'email'      => $order->email, 
                                'phone'      => $order->phone, 
                                'address'    => 'address'
                              ],
                'redirectUrl' => url('/bookingconfirm/'.$order->id),
                'currency' => 'USD'
            ];
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://app.fawaterk.com/api/invoice",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($postData)
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $resArr = json_decode($response, true);

            //$payment = 1;
            if(isset($resArr['url']) && isset($resArr['invoiceKey'])){
                return redirect($resArr['url']);
            }else{
               $order->delete();
               $request->session()->flash('error', 'Payment Error! '.$response);
            }
        }else{
            $order->update(['payment_type' => 'cash']);
            return redirect('/bookingconfirm/'.$order->id);
        }
        
        ///////////////////////////////
        return redirect($redirect.'/'.$product->id);
        /*return response()->json([
            'status' => 'success'
        ]);*/
    }

    public function bookingaction($id, Request $request)
    {   
        $fulldata = $request->all();
        
        $data = $request->except(['first_name', 'last_name', 'tour_accept', 'tour_opts', 'date_from', 'date_to']);

        $payment = $request->payment_type;

        if($request->booking_type == 'tour'){
            $product = Products::find($id);
            $data['booking_type'] = 'tour';
            $product_name = $product->translate('en')->title;
            $redirect = 'tourbooking';
            $data['code'] = str_random(10).time().str_random(10);
            //$data['is_available'] = $product->check_avail ? 0 : NULL;
        }
        else if($request->booking_type == 'transportation'){
            $product = Transportations::find($id);
            $data['booking_type'] = 'transportation';
            $product_name = $product->translate('en')->destination;
            $redirect = 'transportationbooking';
        }
        else{
            return redirect('home');
        }

        if(!$product){
            $request->session()->flash('error', 'Not Found!');
            return redirect('tourbooking/'.$id);
        }
        //############# Prepare data
        $data_opts = $request->tour_opts ?? [];
        
        $data['date_from'] = date('Y-m-d', strtotime($request->date_from));
        if($request->date_to)
            $data['date_to'] = date('Y-m-d', strtotime($request->date_to));
        $data['product_id'] = $product->id;
        ///////////////

        //############ Price calc
        $adults_price = $product->price;
        $childs_price = $product->child_price ?? 0;

        $tour_adults = $request->adults;
        $tour_childs = $request->childs ?? 0;

        // Get price from tour prices interval
        if($tour_adults > 1){

            if($request->booking_type == 'tour')
                $prices = $product->productPrices->toArray();
            else if($request->booking_type == 'transportation')
                $prices = $product->transportationPrices->toArray();

            foreach ($prices as $price) {
                if( ($tour_adults >= $price['from'])  && ( $tour_adults <= $price['to']) ){
                    $adults_price = $price['price'];
                    break;
                  }
            }
        }

        //total people price
        $total_presons_price = ($adults_price * $tour_adults) + ($childs_price * $tour_childs);
        
        //Tour options price calc
        $opts = false;
        $total_opts = 0;
        if(count($data_opts) && $request->booking_type == 'tour'){
            $opts = true;
            $total_opts = ProductExtras::whereIn('id', $data_opts)->sum('price');
        }

        $appSettings = DB::table('settings')->select(['app_fees', 'fawaterak_key'])->where('id', 1)->first();
        $app_fees = $appSettings->app_fees ?? 3;
        
        $total = $total_presons_price + ( $total_opts * $tour_adults ) + ( $total_opts * $tour_childs );
        $total_fees = ( ($app_fees/100) * $total + $total );

        $data['total_price'] = number_format($total_fees, 2, '.', '');
        ////////////

        //////// Create Client data
        $dataClient = [
                        'name'  => $request->first_name.' '.$request->last_name,
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'passport' => $request->passport,
                        'nationality' => $request->nationality
                      ];
        try{
            Clients::firstOrCreate(['email' => $request->email], $dataClient);
        }catch(Exception $e){}
        //////////////////////

        //#############  Create Order
        $data['name'] = $request->first_name.' '.$request->last_name;

        $order = Orders::create($data);

        if(!$order->id ){
            $request->session()->flash('error', 'Booking Error, Please contact Website Admin!');
            /*return response()->json([
                'status' => 'failed' , 'message' => 'Booking Error, Please contact Website Admin!'
            ]); */
        }

        if($opts){
            foreach ($data_opts as $opt) {
                OrderExtras::insert(['order_id' => $order->id , 'product_extras_id' => $opt]);
            }
        }
        ////////////////////////

        //############# Payment Proccess  
        if($payment == 'visa'){

           //$fkapi = "https://app.fawaterak.xyz/api/v2/createInvoiceLink";
             $fkapi = "https://app.fawaterk.com/api/v2/createInvoiceLink";

            $postData = [
                //'vendorKey' => $appSettings->fawaterak_key,
                'cartItems' => [ array(
                                        'name'     => $product_name, 
                                        'price'    => $data['total_price'], 
                                        'quantity' => 1
                                    ) 
                               ],
                'cartTotal' => $data['total_price'],
                'shipping' => 0,
                'customer' => [ 'first_name' => $request->first_name, 
                                'last_name'  => $request->last_name, 
                                'email'      => $request->email, 
                                'phone'      => $request->phone, 
                                'address'    => $request->address ?? 'address'
                              ],
                'redirectUrl' => url('/bookingconfirm/'.$order->id),
                'currency' => 'USD'
            ];
            
            $curl = curl_init();

            /*curl_setopt_array($curl, array(
                CURLOPT_URL => $fkapi,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($postData)
            ));*/
            curl_setopt_array($curl, array(
                CURLOPT_URL => $fkapi,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                   'Content-Type: application/json',
                   'Authorization: Bearer '. $appSettings->fawaterak_key ?? ''
               ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $resArr = json_decode($response, true);
            //dd($err);
            //$payment = 1;
            if(isset($resArr['data']['url']) && isset($resArr['data']['invoiceId'])){
                /*return response()->json([
                    'status' => 'success' , 'url' => $resArr['url']
                ]);*/
                $order->update(['invoice_id',  $resArr['data']['invoiceId']]);
                return redirect($resArr['data']['url']);
            }else{
               $order->delete();
               $request->session()->flash('error', 'Payment Error! '.$response);

               /*return response()->json([
                    'status' => 'failed' , 'message' => 'Payment Error!'
                ]);*/
            }
        }else{
            return redirect('/bookingconfirm/'.$order->id);
        }
        
        ///////////////////////////////
        return Redirect::route($redirect, $id)->with( ['tourCashedData' => $fulldata] );
        //return redirect($redirect.'/'.$product->id);
        /*return response()->json([
            'status' => 'success'
        ]);*/
    }

    /*public function bookingaction(Products $tour, Request $request)
    {   
        //############# Prepare data
        $data_opts = $request->tour_opts;
        $data = $request->except(['first_name', 'last_name', 'tour_accept', 'tour_opts', 'date_from', 'date_to']);
        $data['date_from'] = date('Y-m-d', strtotime($request->date_from));
        if($request->date_to)
            $data['date_to'] = date('Y-m-d', strtotime($request->date_to));
        $data['product_id'] = $tour->id;
        ///////////////

        //############ Price calc
        $adults_price = $tour->price;
        $childs_price = $tour->child_price ?? 0;

        $tour_adults = $request->adults;
        $tour_childs = $request->childs;

        // Get price from tour prices interval
        if($tour_adults > 1){
            $prices = $tour->productPrices->toArray();
            foreach ($prices as $price) {
                if( ($tour_adults >= $price['from'])  && ( $tour_adults <= $price['to']) ){
                    $adults_price = $price['price'];
                    break;
                  }
            }
        }

        //total people price
        $total_presons_price = ($adults_price * $tour_adults) + ($childs_price * $tour_childs);
        
        //Tour options price calc
        $opts = false;
        $total_opts = 0;
        if(count($data_opts)){
            $opts = true;
            $total_opts = ProductExtras::whereIn('id', $data_opts)->sum('price');
        }

        $appSettings = DB::table('settings')->select(['app_fees', 'fawaterak_key'])->where('id', 1)->first();
        $app_fees = $appSettings->app_fees ?? 3;
        
        $total = $total_presons_price + ( $total_opts * $tour_adults );
        $total_fees = ( ($app_fees/100) * $total + $total );

        $data['total_price'] = number_format($total_fees, 2, '.', '');
        ////////////


        //#############  Create Order
        $data['name'] = $request->first_name.' '.$request->last_name;
        $order = Orders::create($data);

        if(!$order->id ){
            $request->session()->flash('error', 'Booking Error, Please contact Website Admin!');
        }

        if($opts){
            foreach ($data_opts as $opt) {
                OrderExtras::insert(['order_id' => $order->id , 'product_extras_id' => $opt]);
            }
        }
        ////////////////////////

        //############# Payment Proccess  
        $postData = [
            'vendorKey' => $appSettings->fawaterak_key,
            'cartItems' => [ array(
                                    'name'     => $tour->translate('en')->title, 
                                    'price'    => $data['total_price'], 
                                    'quantity' => 1
                                ) 
                           ],
            'cartTotal' => $data['total_price'],
            'shipping' => 0,
            'customer' => [ 'first_name' => $request->first_name, 
                            'last_name'  => $request->last_name, 
                            'email'      => $request->email, 
                            'phone'      => $request->phone, 
                            'address'    => $request->address
                          ],
            'redirectUrl' => url('/bookingconfirm/'.$order->id),
            'currency' => 'USD'
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://app.fawaterk.com/api/invoice",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($postData)
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $resArr = json_decode($response, true);

        //$payment = 1;
        if($resArr['url'] && $resArr['invoiceKey']){
            
            return redirect($resArr['url']);
        }else{
           $order->delete();
           $request->session()->flash('error', 'Payment Error!');

           
        }
        ///////////////////////////////
        return redirect('tourbooking/'.$tour->id);
        
    }*/

    public function bookingconfirm(Orders $order, Request $request){

        //Fawaterk check payment
        if($order->payment_status == 'visa'){

            $appSettings = DB::table('settings')->select(['app_fees', 'fawaterak_key'])->where('id', 1)->first();

            //$fkapi = "https://app.fawaterak.xyz/api/getInvoice";
            $fkapi = "https://app.fawaterk.com/api/getInvoice";

            $invoice_id = $request->invoice_id;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                 CURLOPT_URL =>$fkapi,
                 CURLOPT_RETURNTRANSFER => true,
                 CURLOPT_ENCODING => "",
                 CURLOPT_MAXREDIRS => 10,
                 CURLOPT_TIMEOUT => 30000,
                 CURLOPT_CUSTOMREQUEST => "GET",
                 CURLOPT_POSTFIELDS =>'{"invoice_id": '.$invoice_id.'}',
                 CURLOPT_HTTPHEADER => array(
                  'token: '.$appSettings->fawaterak_key.'',
                  'Content-Type: application/json'
                  ),
             ));
            $response = curl_exec($curl);
            curl_close($curl); 
            $response = json_decode($response);
            
            if($response->status=='success' && $response->data->paid){ // payment done
              $order->update(['payment_status' => 1]);
            }else if($response->status=='success' && !$response->data->paid){//pending payment
              $order->update(['payment_status' => 2]);
            }
        }else{
            if((!$order->products->check_avail) || ($order->products->check_avail && $order->is_available))
            $order->update(['payment_status' => 1]);
        }


        $extras = '';
        if($order->booking_type == 'tour'){
            $product_name = $order->products->translate('en')->title;
            $booking_type = 'Tour';

            $orderExtras = OrderExtras::where('order_id', $order->id)->get();
            foreach($orderExtras as $extra){
                $extras .= '<li>'. $extra->productExtras->translate('en')->title .'</li>';
            }
        }
        else if($order->booking_type == 'transportation'){
            $product_name = $order->transportations->translate('en')->destination;
            $booking_type = 'Airport Transportation';
        }

        $appSettings = DB::table('settings')->select(['app_email'])->where('id', 1)->first();
        $app_email = $appSettings->app_email;

        $toMail = $app_email;
        $toMail2 = $order->email;

        if((!$order->products->check_avail) || ($order->products->check_avail && $order->is_available)){
            $subject = 'M-Tours New '.$booking_type.' Booking Request';
            $availability = 0;
        }
        else{
            $availability = 1;
            $subject = 'M-Tours New '.$booking_type.' Check Availability Request';
        }

        $data = [
                    'order' => $order,
                    //'id' => $order->id, 
                    //'name' => $order->name, 
                   // 'email' => $order->email, 
                    //'phone' => $order->phone, 
                    'tour' => $product_name, 
                    //'hotel' => $order->hotel ?? '', 
                    'date_from' => $order->date_from ?? '', 
                    'date_to' => $order->date_to ?? '',
                    'date_type' => ($order->booking_type == 'transportation') ? 'date' : 'date_from', 
                    //'total' => $order->total_price, 
                    //'note' => $order->note,
                    'booking_type' => $booking_type,
                    'extras' => $extras,
                    'availability' => $availability
                ];

        $mail = Mail::send('emails.booking', ['data' => $data], function ($m) use ($toMail, $subject) {
            $m->to($toMail)->subject($subject);
        });

        $mail = Mail::send('emails.booking', ['data' => $data], function ($m) use ($toMail2, $subject) {
            $m->to($toMail2)->subject($subject);
        });
        
        return redirect('bookingsuccess');

        /*$request->session()->flash('error', 'Payment Error!');
        return redirect('tourbooking/'.$tour->id);*/
    }


    public function booksuccess()
    {
        return view('front.tourbooksucc');
    }

    public function webhook_fk(){
     
      $responseDataJson = file_get_contents('php://input');
      if (!empty($responseDataJson)){
          $responseDataArray = json_decode($responseDataJson);
          $invoice_id = $responseDataArray->invoice_id;
          
          $order =  Orders::where('invoice_id', $invoice_id)->get();

          if($order){
              $appSettings = DB::table('settings')->select(['app_fees', 'fawaterak_key'])->where('id', 1)->first();

              // check if invoice paid
              //$fkapi = "https://app.fawaterak.xyz/api/getInvoice";
              $fkapi = "https://app.fawaterk.com/api/getInvoice";

              $curl = curl_init();
              curl_setopt_array($curl, array(
                  CURLOPT_URL =>$fkapi,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30000,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_POSTFIELDS =>'{"invoice_id": '.$invoice_id.'}',
                  CURLOPT_HTTPHEADER => array(
                      'token: '.$appSettings->fawaterak_key.'',
                      'Content-Type: application/json'
                  ),
              ));
              $response = curl_exec($curl);
              curl_close($curl);
              $response = json_decode($response);

              if($responseDataArray->invoice_status == 'paid' && $response->status == 'success' && $response->data->paid){
                  $order->update(['payment_status' => 1]);
              }

            }
        }
    }

    public function transportations()
    {
        $pg_count = 10;
        $transportations_count = Transportations::count();
        $transportations = Transportations::paginate($pg_count);

        return view('front.transportations')->with(compact('transportations', 'transportations_count', 'pg_count'));
    }

    public function blog()
    {
        $posts = Posts::paginate(9);
        $inner_body = 1;

        return view('front.blog')->with(compact('posts', 'inner_body'));
    }

    public function post(Posts $post)
    {
        $posts = Posts::where('id', '!=', $post->id)->latest()->limit(5)->get();
        $inner_body = 1;

        return view('front.blogpost')->with(compact('post', 'posts', 'inner_body'));
    }


    public function contactmail(Request $request){
        $appSettings = DB::table('settings')->select(['app_email'])->where('id', 1)->first();
        $app_email = $appSettings->app_email;

        $toMail = $app_email;

        $subject = $request->subject;
        $data = ['email' => $request->e_mail, 'name' => $request->name, 'subject' => $subject, 'message' => $request->message];

        $mail = Mail::send('emails.contact', ['data' => $data], function ($m) use ($toMail, $subject) {
            $m->to($toMail)->subject($subject);
        });
        $request->session()->flash('success', 'Message Sent Successfully!');
        return redirect('/contact');
    }

    public function getSubcats($cat_id){
        $lang = App::getLocale();
        $subs = DB::table('categories as c')->select('c.id', 't.value')->leftjoin('translations as t', 'c.title' ,'=', 't.group_id')->where('c.parent_id','=', $cat_id)->where('t.locale','=', $lang )->get();
        return response()->json($subs);
    }
}
