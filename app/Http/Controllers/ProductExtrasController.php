<?php

namespace App\Http\Controllers;

use App\ProductExtras;
use Illuminate\Http\Request;

class ProductExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductExtras  $productExtras
     * @return \Illuminate\Http\Response
     */
    public function show(ProductExtras $productExtras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductExtras  $productExtras
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductExtras $productExtras)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductExtras  $productExtras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductExtras $productExtras)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductExtras  $productExtras
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductExtras $productExtras)
    {
        //
    }
}
