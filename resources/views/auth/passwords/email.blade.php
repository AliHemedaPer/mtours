@extends('admin.layouts.admin_auth')

@section('content')
<div class="box-login">
                <h3>إسترجاع الرقم السري</h3>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" style="text-align: right;" class="col-md-12 control-label">البريد الإليكتروني</label>

                           
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                           
                        </div>

                        <div class="form-group">
                           
                                <button type="submit" class="btn btn-primary">
                                    إسترجاع
                                </button>
                                <a href="{{url('admin/login')}}" class="btn btn-primary">
                                    دخول
                                </a>
                            
                        </div>
                    </form>
                </div>
        
</div>
@endsection
