<?php

namespace App\Http\Controllers\Admin;

use App\Clients;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $clients   = Clients::all();
        return view('admin.clients.index', compact('clients'));
    }


    public function sendmail(Request $request)
    {
        $client = Clients::where('id', $request->id)->first();

        return view('admin.clients.form', compact('clients', 'client'));
    }

    public function sendaction(Request $request)
    {
        $subject = $request->subject;
        $htmlMessage = $request->message;

        $mails = [];
        $client_id = $request->client_id ?? '';

        if($client_id){
            $client  = Clients::where('id', $client_id)->first();
            $mails   = [$client->email];
        }else{
            $clients = Clients::all();
            foreach ($clients as $client) {
                $mails[] = $client->email;
            }
        }

        $count = count($mails);
        if($count > 0 && $subject && $htmlMessage){
            for ($i=0; $i < $count; $i++) { 

                $toMail = $mails[$i];

                Mail::send('emails.newsletter', ['data' => $htmlMessage], function ($m) use ($toMail, $subject) {
                    $m->to($toMail)->subject($subject);
                });
            }
        }else{
            \Session::flash('error', 'Failed: Email, Subject or Message missed !');
            return redirect('admin/clients/mail');
        }
        \Session::flash('success', 'Success!');
        return redirect('admin/clients/mail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function show(Clients $clients)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function edit(Clients $clients)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clients $clients)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clients $client)
    {
        $client->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/clients');
    }
}
