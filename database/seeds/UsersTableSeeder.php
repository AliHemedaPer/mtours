<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'senjider@gmail.com',
                'password' => '$2y$10$2wfwYmyDokmfGsC7lmTtjODmysMqZrN5lC5NgF28II70b9X1trBsW',
                'type' => 'admin',
                'remember_token' => 'prxcPEpF1boEhVLsmC8F69ViofE9OrXsJqImyBAFYuawYoNh2kVFkYnIqmL4',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}