<?php
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

class ClientController extends Controller
{
    /**
     *
     * @param int $is_Web
     *
     * @return mixed
     */
    public function CreateUser(Request $request, $is_Web = 0)
    {
        //      if (!$this->isCsrfAccepted ()) {
        //          return Response()->make(['status' => 'FAILURE' , 'msg' => 'Invalid Request made'] , 401);
        //
        //      }
        try {
            $email = $request->email ?? '';
            $name = $request->user_name ?? '';
            $phone = $request->phone ?? '';
            $password = $request->password ?? '';
            $confirm_password = $request->confirm_password ?? '';
            $user_type = 'client';

            $ex = User::where('email', $email)->where('type', 'client')->count();
            
            if ($ex != 0) {
                return Response()->make(['status' => 'FAILURE' , 'msg' => 'This E-mail Exists!'], 422);
            }


            //check if password == confirm password(not needed for mobile), then proceed the following
            $digits = 4;
            $randomValue = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

            $user = new User;
            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->type = $user_type;
            $user->phone = $phone;
            $user->security_code = $randomValue;
            $user->status = 0;
            $user->save();

             Mail::send ('emails.register' , array('name' => $name , 'user_name' => $email , 'pwd' => $password , 'code' => $randomValue) , function ($message) use ($email) {
                 $message->to ($email)->subject ('M-Tours Activate Account');
             });

            //else
            return Response()->json(['status' => 'SUCCESS' , 'msg' => 'Acccount successfully created, Please check mail for the code'], 201);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * Update User Details
     *
     * @param int $isWeb
     *
     * @return mixed
     */
    public function profileUpdate(Request $request, $isWeb = 0)
    {
        if ($isWeb) {
            $oldemail = auth('client')->user()->email;
            $email = $request->email ?? '';
            $name = $request->name ?? '';
            $phone = $request->phone ?? 00;
            $gender = $request->gender;
            $bdate = $request->bdate;
        }

        $clientDetails = ['name' => $name ,
                          'phone' => $phone ,
                          'email' => $email,
                          'gender' => $gender,
                          'bdate' => $bdate
                         ];
        $affectedRows = Client::where('email', '=', $oldemail)->update($clientDetails);

        if (count($affectedRows) == 1) {
            $result = ['status' => 'SUCCESS' , 'msg' => 'Profile updated !'];
        } else {
            $result = ['status' => 'FAILED' , 'msg' => 'Profile update fail!'];
        }

        return Response()->json($result);
    }

    /**
     * User Login
     *
     * @param int $isWeb
     *
     * @return mixed
     */
    public function UserLogin(Request $request, $isWeb = 0)
    {
        try {
            $email = $request->email ?? '';
            $password = $request->password ?? '';
            $status = DB::table('users')->select('status')->where('email', '=', $email)->where('type', '=', 'client')->first();
            if (!empty($status)) {
                if ($status->status == 0) {
                    $result = ['status' => 'notactive'];

                    $request->session()->put('user_password', $password);
                } elseif ($status->status == 1) {
                    if (Auth::attempt(['email' => $email , 'password' => $password, 'type' => 'client'])) {
                        $request->session()->put('user_id', $email);
                        $result = ['status' => 'success' , 'page' => 'yes'];
                    } else {
                        $result = ['status' => 'failure'];
                    }
                } else {
                    $result = ['status' => 'delete'];
                }
            } else {
                $result = ['status' => 'failure'];
            }
            
            return Response()->json($result);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }


    public function logout() {
        Auth::logout();
        return redirect('/');
    }

    /**
     * function to activate the user registration from web
     *
     * @param $code
     *
     * @return mixed
     */
    public function activateAccount($code)
    {
        $client = User::where('security_code', $code)->where('type', 'client')->first();
        if (count($client)) {
            $updatedValues = ['security_code' => '', 'status' => 1];
            $client->update($updatedValues);

            return view('front.activate_succ');
        } else {
            return view('front.activate_fail');
        }
    }


    /**
     * Reset Password
     *
     * @return mixed
     */
    public function resetPassword(Request $request)
    {
            $email = $request->email ?? '';
            if ($request->email && $request->security_code && $request->new_password) {
                $security_code = $request->security_code;
                $password = $request->new_password;
                $confirm_password = $request->confirm_password;
                $client = Client::where('email', '=', $email)->where('security_code', '=', $security_code)->first();
                if (!is_null($client)) {
                    $client->password = Hash::make($password);
                    $client->status = 1;
                    $client->save();
                    //                  $result = array(array('result' => array('status' => 'success')));
                    $result = ['status' => 'SUCCESS' , 'msg' => 'Password Changed'];
                    return Response()->json($result);
                }
            }else if ($request->email && auth('client')->check() && $request->new_password) {
                if(auth('client')->user()->email == $email){
                    $password = $request->new_password;
                    $confirm_password = $request->confirm_password;
                    $client = Client::where('email', '=', $email)->first();
                    if (!is_null($client)) {
                        $client->password = Hash::make($password);
                        //$client->status = 1;
                        $client->save();
                        //                  $result = array(array('result' => array('status' => 'success')));
                        $result = ['status' => 'SUCCESS' , 'msg' => 'Password Changed'];
                        return Response()->json($result);
                    }
                }
            } else {
                if (Client::where('email', '=', $email)->count() == 1) {
                    $digits = 8;
                    $randomValue = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                    $updatedValues = ['security_code' => $randomValue];
                    $client = Client::where('email', '=', $email)->update($updatedValues);
                    $data = ['email' => $email, 'app_name' => 'Saidalista', 'code' => $randomValue, 'guard' => ''];
                    Mail::send('emails.reset_password', ['data' => $data], function ($message) use ($email) {
                        $message->to($email)->subject('Reset Password');
                    });
                    //                  $result = array(array('result' => array('status' => 'reset_success')));
                    $result = ['status' => 'SUCCESS' , 'msg' => 'Password Reset'];
                    return Response()->json($result);
                }
                
            }
            return Response()->make(['status' => 'FAILURE' , 'msg' => 'No User Found'], 404);
    }

    /**
     * Check out username
     */
    public function CheckUserName(Request $request)
    {
        try {
            $current_mail = $request->u_name;
            $regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^";
            if (preg_match($regex, $current_mail)) {
                $name = DB::table('clients')->where('email', $current_mail)->pluck('email');
                if (count($name) > 0) {
                    throw new Exception('Email Already Exists !', 409);
                }
            } else {
                throw new Exception('Email is not valid !', 400);
            }

            return Response()->json(['status' => 'SUCCESS' , 'msg' => 'Email is valid'], 200);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * Account Page
     *
     * @return mixed
     */
    public function getAccountPage()
    {
        $clinet_id = auth('client')->id();
        $user_data = Client::find($clinet_id);

        if($user_data->bdate)
            $bdate = explode('-', $user_data->bdate);
        else
            $bdate = [0,0,0];

        return view('client.profile')->with(compact('user_data', 'bdate'));
    }


    /**
     * Check the login status
     *
     * @return mixed
     */
    public function getCheckSession()
    {
        if (auth('client')->check()) {
            $login = 1;
        } else {
            $login = 0;
        }

        return Response()->json($login);
    }

}
