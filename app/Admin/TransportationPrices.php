<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class TransportationPrices extends Model
{
    protected $fillable = ['transportation_id', 'from', 'to', 'price'];

    public function transportations()
    {
        return $this->belongsTo(Transportations::class, 'transportation_id', 'id');
    }
}
