@extends('front.front')

@section('content')
<div class="main-cont about-page">   
  
  <div class="inner-page">
    <div class="about-content">
      <div class="content-wrapper" style="padding-top: 40px">
        <header class="page-lbl fly-in">
          <div class="offer-slider-lbl">{{ $about->translate($share_locale)->title }}</div>
          
            {!! $about->translate($share_locale)->content !!}
          
        </header>     
        
        <div class="clear"></div>
    </div>
    
    <div class="about-us-devider fly-in"></div>
      
  <div class="why-we">
    <div class="content-wrapper">
      <header class="page-lbl fly-in">
        <div class="offer-slider-lbl">@lang('fnt.whyus')</div>
        
      </header>   
      
      <div class="tree-colls fly-in">
        <div class="tree-colls-i">
          <div class="about-percent why-we-item">
            <div class="why-we-img"><img alt="" src="{{ asset('public/front') }}/img/why-we-01.png"></div>
            <div class="why-we-lbl">@lang('fnt.whyus_t1')</div>
            <div class="why-we-txt">@lang('fnt.whyus_x1')</div>
          </div>
        </div>
        <div class="tree-colls-i">
          <div class="about-percent why-we-item">
            <div class="why-we-img"><img alt="" src="{{ asset('public/front') }}/img/why-we-02.png"></div>
            <div class="why-we-lbl">@lang('fnt.whyus_t2')</div>
            <div class="why-we-txt">@lang('fnt.whyus_x2')</div>
          </div>
        </div>
        <div class="tree-colls-i">
          <div class="about-percent why-we-item">
            <div class="why-we-img"><img alt="" src="{{ asset('public/front') }}/img/why-we-03.png"></div>
            <div class="why-we-lbl">@lang('fnt.whyus_t3')</div>
            <div class="why-we-txt">@lang('fnt.whyus_x3')</div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  
</div>
</div>
</div>
@stop