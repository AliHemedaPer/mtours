@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li>
        <a href="{{url('admin/categories')}}">
            Categories
        </a>
    </li>
    <li class="active">
        @if($category)
        Edit
        @else 
        Add
        @endif
    </li>
  
</ol>
<div class="page-header">
    <h1 class="col-md-6">Category</h1>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="{{url("admin/categories")}}"><i class="icon-plus2 mr-2"></i> Back <i class="fa fa-reply"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            
        <div class="panel-body">
        @if($category)
        {!! Form::model($category,["url"=>"admin/categories/$category->id","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
        @include('admin.categories.input',['buttonAction'=>'Save'])
        @else 
        {!! Form::open(["url"=>"admin/categories","class"=>"form-horizontal","method"=>"POST","enctype"=>"multipart/form-data"]) !!}
        @include('admin.categories.input',['buttonAction'=>'Save'])
        @endif
        {!! Form::close() !!}
    </div>
</div>
</div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({
            allowClear: true
        });
        FormElements.init();
    });
</script>
@endsection