<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title>{{ config('app.name', 'MTours') }}</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/fonts/style.css?v=1">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/main.css?v=1">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/main-responsive.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/theme_light.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/print.css" type="text/css" media="print"/>
        <!-- <link rel="stylesheet" href="{{url('public/admin/')}}/assets/css/rtl-version.css"> -->
        <!--[if IE 7]>
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/select2/select2.css" />
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/DataTables/media/css/DT_bootstrap.css" />      
        <link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="shortcut icon" href="favicon.ico" />
        @yield('style')
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body >
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
                    <a class="navbar-brand" href="{{url('/')}}" target="_blank">
                        {{ config('app.name', 'MTours') }}
                    </a>
                    <!-- end: LOGO -->
                </div>
                <div class="navbar-tools">
                    <!-- start: TOP NAVIGATION MENU -->
                    <ul class="nav navbar-right">

                        <!-- start: USER DROPDOWN -->
                        <li class="dropdown current-user">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                <!-- <img src="{{url('public/admin/')}}/assets/images/avatar-1-small.jpg" class="circle-img" alt=""> -->
                                <span class="username"> {{ Auth::user()->name }}</span>
                                <i class="clip-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- <li>
                                    <a href="{{url('admin/profile')}}">
                                        <i class="clip-user-2"></i>
                                        &nbsp; الصحفحة الشخصية
                                    </a>
                                </li>                              
                                <li class="divider"></li>   -->                           
                                <li>
                                    <a href="{{url('admin/logout')}}">
                                        <i class="clip-exit"></i>
                                        &nbsp; Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end: USER DROPDOWN -->
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                <div class="main-navigation navbar-collapse collapse">
                    <!-- start: MAIN MENU TOGGLER BUTTON -->
                    <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                    </div>
                    <!-- end: MAIN MENU TOGGLER BUTTON -->
                    <!-- start: MAIN NAVIGATION MENU -->
                    <ul class="main-navigation-menu">
                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'dashboard') !== false)) active @endif">
                            <a href="{{url('admin/dashboard')}}"><i class="clip-home-3"></i>
                                <span class="title"> Home </span><span class="selected"></span>
                            </a>
                        </li>
                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'about') !== false)) active @endif">
                            <a href="{{url('admin/about')}}"><i class="clip-info"></i>
                                <span class="title"> About {{ config('app.name', 'MTours') }} </span><span class="selected"></span>
                            </a>
                        </li>
                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'categories') !== false)) active @endif">
                            <a href="{{url('admin/categories')}}"><i class="clip-grid"></i>
                                <span class="title"> Categories </span><span class="selected"></span>
                            </a>
                        </li>
                        <!-- <li class="@if(strpos($_SERVER['REQUEST_URI'], 'services') !== false)) active @endif">
                            <a href="{{url('admin/services')}}"><i class="clip-star-4"></i>
                                <span class="title"> الخدمات </span><span class="selected"></span>
                            </a>
                        </li> -->
                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'products') !== false)) active @endif">
                            <a href="{{url('admin/products')}}"><i class="clip-stack-empty"></i>
                                <span class="title"> Tours </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'admin/transportations') !== false)) active @endif">
                            <a href="{{url('admin/transportations')}}"><i class="clip-truck"></i>
                                <span class="title"> Airport Transportation </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'reservations/tours') !== false)) active @endif">
                            <a href="{{url('admin/reservations/tours')}}"><i class="clip-list"></i>
                                <span class="title"> Tours Reservations </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'reservations/transportations') !== false)) active @endif">
                            <a href="{{url('admin/reservations/transportations')}}"><i class="clip-list"></i>
                                <span class="title"> Airport Trs. Reservations </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'reports') !== false)) active @endif">
                            <a href="{{url('admin/reports')}}"><i class="clip-file"></i>
                                <span class="title"> Reports </span><span class="selected"></span>
                            </a>
                        </li>
                        
                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'posts') !== false)) active @endif">
                            <a href="{{url('admin/posts')}}"><i class="clip-book"></i>
                                <span class="title"> Blog </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'banners') !== false)) active @endif">
                            <a href="{{url('admin/banners')}}"><i class="clip-image"></i>
                                <span class="title"> Banners </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'clients') !== false)) active @endif">
                            <a href="{{url('admin/clients')}}"><i class="clip-users"></i>
                                <span class="title"> Clients </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'terms') !== false)) active @endif">
                            <a href="{{url('admin/terms')}}"><i class="clip-info"></i>
                                <span class="title"> Terms & Conditions </span><span class="selected"></span>
                            </a>
                        </li>

                        <li class="@if(strpos($_SERVER['REQUEST_URI'], 'settings') !== false)) active @endif">
                            <a href="{{url('admin/settings')}}"><i class="clip-cog"></i>
                                <span class="title"> Settings </span><span class="selected"></span>
                            </a>
                        </li>

                    </ul>
                    <!-- end: MAIN NAVIGATION MENU -->
                </div>
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">
                <!-- start: PANEL CONFIGURATION MODAL FORM -->
                <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    &times;
                                </button>
                                <h4 class="modal-title">Panel Configuration</h4>
                            </div>
                            <div class="modal-body">
                                Here will be a configuration form
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary">
                                    Save changes
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- end: SPANEL CONFIGURATION MODAL FORM -->
                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            @include('admin.layouts.errors')
                            @yield('content')
                        </div>
                    </div>
                    <!-- end: PAGE -->
                </div>
                <!-- end: MAIN CONTAINER -->
                <!-- start: FOOTER -->
                <div class="footer clearfix">
                    <div class="footer-inner">
                        {{date('Y')}} &copy; {{ config('app.name', 'Laravel') }}.
                    </div>
                    <div class="footer-items">
                        <span class="go-top"><i class="clip-chevron-up"></i></span>
                    </div>
                </div>
                <!-- end: FOOTER -->

                <!-- start: MAIN JAVASCRIPTS -->
                <!--[if lt IE 9]>
                <script src="{{url('public/admin/')}}/assets/plugins/respond.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/excanvas.min.js"></script>
                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                <![endif]-->
                <!--[if gte IE 9]><!-->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
                <!--<![endif]-->
                <script src="{{url('public/admin/')}}/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/blockUI/jquery.blockUI.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/iCheck/jquery.icheck.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/less/less-1.5.0.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
                <script src="{{url('public/admin/')}}/assets/js/main.js"></script>
                <!-- end: MAIN JAVASCRIPTS -->
                <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
                <!-- <script src="{{url('public/admin/')}}/assets/plugins/flot/jquery.flot.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/flot/jquery.flot.pie.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/flot/jquery.flot.resize.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script> -->
                <script src="{{url('public/admin/')}}/assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
                <script src="{{url('public/admin/')}}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
                <!-- <script src="{{url('public/admin/')}}/assets/js/index.js"></script> -->
                <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
                @yield('page_script')
                <script>
                jQuery(document).ready(function () {
                    Main.init();
                    Index.init();

                });
                </script>
                </body>
                <!-- end: BODY -->
                </html>