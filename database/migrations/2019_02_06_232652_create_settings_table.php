<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('app_name');
            $table->string('app_email')->nullabel();
            $table->string('app_phone')->nullabel();
            $table->string('app_currency');
            $table->string('app_feed');
            $table->string('app_fb')->nullabel();
            $table->string('app_tw')->nullabel();
            $table->string('app_in')->nullabel();
            $table->string('app_wats')->nullabel();
            $table->string('fawaterak_key')->nullabel();
            $table->tinyInteger('show_cash')->default(0);
            $table->string('image', 255)->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
