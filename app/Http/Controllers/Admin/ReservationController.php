<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Orders;
use App\OrderExtras;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;
class ReservationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($btype, Request $request) {

        if($btype == 'transportations')
            $whereType = 'transportation';
        else
            $whereType = 'tour';

        $typ = $request->typ ?? 'p';

        if($typ == 'd')
            $reservations   = Orders::where('booking_type', $whereType)->where('order_done', 1)->get();
        else
            $reservations   = Orders::where('booking_type', $whereType)->where('order_done', 0)->get();
        return view('admin.reservations.index', compact('typ', 'btype' ,'reservations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($btype, Orders $reservation) {
        $reservation->update(['is_read' => 1]);
        return view('admin.reservations.show', compact('btype', 'reservation'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($btype, $order) {
        if(Orders::where('id', $order)->delete())
            OrderExtras::where('order_id', $order)->delete();

        \Session::flash('success', 'Success!');
        return redirect('admin/reservations/'.$btype);
    }

    public function updReservation(Request $request){
        $typ = ($request->typ == 'd') ? 1 : 0;
        $id  = $request->id;

        if(!$id){
            $status = 'failed1';
        }else{
            $updt = Orders::where('id', $id)->update(['order_done' => $typ]);
            if($updt)
                $status = 'success';
            else
                $status = 'failed2';
        }

        return response()->json([ 'status' => $status ]); 
    }

    public function updAvailability(Orders $order, Request $request){
        if($request->avail == '0')
            $avail = 0;
        else if($request->avail == '1')
            $avail = 1;
        else
            $avail = null;

        if(!$order){
            $status = 'failed1';
        }else{
            $updt = $order->update(['is_available' => $avail]);

            if($updt){
                $product_name = $order->products->translate('en')->title;
                $booking_type = 'Tour';

                $extras = '';
                $orderExtras = OrderExtras::where('order_id', $order->id)->get();
                foreach($orderExtras as $extra){
                    $extras .= '<li>'. $extra->productExtras->translate('en')->title .'</li>';
                }

                $toMail = $order->email;

                $subject = 'M-Tours Tour Availability Reply';

                $data = [
                    'order' => $order,
                    //'id' => $order->id, 
                    //'name' => $order->name, 
                   // 'email' => $order->email, 
                    //'phone' => $order->phone, 
                    'tour' => $product_name, 
                    //'hotel' => $order->hotel ?? '', 
                    'date_from' => $order->date_from ?? '', 
                    'date_to' => $order->date_to ?? '',
                    'date_type' => 'date_from', 
                    //'total' => $order->total_price, 
                    //'note' => $order->note,
                    'booking_type' => $booking_type,
                    'extras' => $extras
                ];

                //Send Available Mail
                if($avail == 1){
                    $data['availMessage'] = 'It\'s M-Tours again! <br/>This is to confirm your booking availability. <br/> <br/> Plese use <a href="'.url('orderconfirm/'.$order->id.'?code='.$order->code).'">This Link</a> to confirm the Booking. <br/> <br/>Thanks for booking with M-Tours.<br/> <br/>';

                    $mail = Mail::send('emails.availability', ['data' => $data], function ($m) use ($toMail, $subject) {
                        $m->to($toMail)->subject($subject);
                    });
                }else if($avail == 0){
                    $data['availMessage'] = 'Thanks for reaching M-Tours.<br/>Unfortunately your booking has not been placed due to unavailability.<br/> <br/>';

                    $mail = Mail::send('emails.availability', ['data' => $data], function ($m) use ($toMail, $subject) {
                        $m->to($toMail)->subject($subject);
                    });
                }                

                $status = 'success';
            }
            else{
                $status = 'failed2';
            }
        }

        return response()->json([ 'status' => $status ]); 
    }

}
