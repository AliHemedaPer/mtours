@extends('front.front')
@section('style')
<style type="text/css">
  .req{
    position: absolute;
    color: #ff0b0b;
    top: -2px;
    right: 0px;
    font-size: 13px;
    display: none;
  }
</style>
@endsection
@section('content')
<div class="main-cont" style="padding-top: 100px">   
  <!-- <div class="contacts-map">
    <div id="map"></div>
  </div> -->
  
  <div class="contacts-page-holder">
  <div class="contacts-page">
    <header class="page-lbl">
      <div class="offer-slider-lbl">@lang('fnt.get_in_tch')</div>
    </header>   
    
    <div class="contacts-colls">
      <div class="contacts-colls-l">
        <div class="contact-colls-lbl">@lang('fnt.office')</div>
        <div class="contacts-colls-txt">
          <p><strong> @lang('fnt.address'): </strong> {{ $app_settings['app_address'] ?? ''}}</p>
          <p><strong> @lang('fnt.phone'): </strong> {{ $app_settings['app_phone'] }}</p>
          <p><strong> @lang('fnt.email'): </strong> {{ $app_settings['app_email'] }}</p>
          <div class="side-social">
                @if( $app_settings['app_tw'] )
                <a href="{{ $app_settings['app_tw'] }}" class="side-social-twitter" target="_blank"></a>
                @endif

                @if( $app_settings['app_fb'] )
                <a href="{{ $app_settings['app_fb'] }}" class="side-social-facebook" target="_blank"></a>
                @endif

                @if( $app_settings['app_in'] )
                <a href="{{ $app_settings['app_in'] }}" class="side-social-instagram" target="_blank"></a>
                @endif
              </div>
        </div>
      </div>
      <div class="contacts-colls-r">
          @if (Request::session()->has('error')) 
           <div class="message-box-c" style="border: 1px solid #dc2323;background: #e83434;padding: 8px 0;">{{Request::session()->get('error')}}</div>
          @elseif (Request::session()->has('success')) 
           <div class="message-box-c" style="border: 1px solid #1e9675;background: #24a984;  padding: 8px 0;">{{Request::session()->get('success')}}</div>
          @endif

          <div class="contacts-colls-rb">
          <div class="contact-colls-lbl">@lang('fnt.contact')</div>
          <div class="booking-form">
            <form id="message_form" method="POST" action="{{ url('/contactmail') }}">
              {{ csrf_field() }}
              <div class="booking-form-i" style="position: relative;">
                <label>@lang('fnt.name'):</label>
                <div class="input"><input type="text" id="name" name="name" value="" /></div>
                <div class="req" id="name_req">Required!</div>
              </div>
              <div class="booking-form-i" style="position: relative;">
                <label>@lang('fnt.email'):</label>
                <div class="input"><input type="text" id="e-mail" name="e_mail" value="" /></div>
                <div class="req" id="e-mail_req">Required!</div>
              </div>
              <div class="booking-form-i" style="position: relative;">
                <label>@lang('fnt.subject'):</label>
                <div class="input"><input type="text" id="subject" name="subject" value="" /></div>
                <div class="req" id="subject_req">Required!</div>
              </div>
              <div class="booking-form-i">
                <label>@lang('fnt.phone'):</label>
                <div class="input"><input type="text" name="phone" value="" /></div>
              </div>
              <div class="booking-form-i textarea" style="position: relative;">
                <label>@lang('fnt.message'):</label>
                <div class="textarea-wrapper">
                  <textarea name="message" id="message"></textarea>
                </div>
                <div class="req" id="message_req">@lang('fnt.req')</div>
              </div>
              <div class="clear"></div>
              <button class="contacts-send" type="button" onclick="mail_action()" style="float: left;">@lang('fnt.send')</button>

              <img id="loading" src="{{ asset('public/front/img/') }}/89.gif" style="float: left; margin: 5px 0 0 10px;display: none;" />

              <div id="error_message" style="float: left; margin: 15px 0 0 10px;color: rgb(253, 48, 48);display: none;font-weight: bold;"></div>
              <div class="clear"></div>
            </form>
            
          </div>
          </div>
        <div class="clear"></div>
      </div>
      </div>
    <div class="clear"></div> 
  </div>
  </div>
  
</div>
@endsection

@section('javascript')
  <script>
    function mail_action(){
      var vali_arr = ['name', 'e-mail', 'subject', 'message'];

      var validate = true;
      for (var i = 0; i < vali_arr.length; i++) {
        console.log(vali_arr[i], $('#'+vali_arr[i]).val());
        if( !$('#'+vali_arr[i]).val() || 
            $('#'+vali_arr[i]).val() == '' || 
            $('#'+vali_arr[i]).val() == undefined || 
            $('#'+vali_arr[i]).val() == null )
        {
          $('#'+vali_arr[i]+'_req').fadeIn();
          validate = false;
        }else{
          $('#'+vali_arr[i]+'_req').fadeOut();
        }
      }

      if(validate){
        $('#loading').fadeIn();
        $('#error_message').fadeOut();

        $("#message_form").submit();

      }else{
        $('#error_message').text('Please check required fields!');
        $('#error_message').fadeIn();
      }
    }
  </script>
  @endsection