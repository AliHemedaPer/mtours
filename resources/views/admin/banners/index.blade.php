@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="#">
            Home
        </a>
    </li>
    <li class="active">
        Banners
    </li>

</ol>
<div class="page-header">
    <h1 class="col-md-6">Banners</h1>
    <div class="col-md-6">
        <a class="btn btn-success pull-right" href="{{url("admin/banners/create")}}"><i class="icon-plus2 mr-2"></i> Add Banner <i class="fa fa-plus"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">

                    <thead>
                        <tr>             
                            <th>Title</th>
                            <th>Text</th>
                            <th>Link</th>                
                            <th class="text-center" style="width: 150px">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($banners as $k => $value)
                        <tr>                   
                            <td>{{$value->translate('ar')->title}}</td>
                            <td>{{$value->translate('ar')->description}}</td>
                            <td><a href="{{$value->url}}">{{$value->url}}</a></td>
                           
                            <td class="center">
                                <div class="visible-md visible-lg hidden-sm hidden-xs">
                                    <a href="{{url('admin/banners/'.$value->id.'/edit')}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                    {{ Form::open(array('url' => 'admin/banners/' . $value->id, 'style' => 'display: inline-block;')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <button type="submit" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Delete" onclick="return confirm('are you sure?')" ><i class="fa fa-times fa fa-white"></i></button>
                            {{ Form::close() }}
                                   
                                </div>
                              
                            </td>
                   

                    </td>                           
                    </tr>
                    @endforeach         
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="{{url('public/admin/')}}/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
                                    jQuery(document).ready(function () {

                                        TableData.init();
                                    });
</script>
@endsection