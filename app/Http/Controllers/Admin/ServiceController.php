<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Service;
use Validator;

class ServiceController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $services = Service::all();
        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $service = NULL;
        return view('admin.services.form', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/services/";
        if ($request->hasFile('home_image')) {
            $file = $request->file('home_image');
            $uniqueID = time();
            $file_name = 'home_image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['home_image'] = $path . $file_name;
        }
        if ($request->hasFile('banner_image')) {
            $file = $request->file('banner_image');
            $uniqueID = time();
            $file_name = 'banner_image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['banner_image'] = $path . $file_name;
        }
        if ($request->hasFile('page_image')) {
            $file = $request->file('page_image');
            $uniqueID = time();
            $file_name = 'page_image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['page_image'] = $path . $file_name;
        }
        Service::create($data);

        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service) {
        return view('admin.services.form', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service) {
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $Newservice = $request->all();
        $path = "uploads/services/";
        if ($request->hasFile('home_image')) {
            $file = $request->file('home_image');
            $uniqueID = time();
            $file_name = 'home_image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($service->home_image)) {
                unlink($service->home_image);
            }
            $Newservice['home_image'] = $path . $file_name;
        }
        if ($request->hasFile('banner_image')) {
            $file = $request->file('banner_image');
            $uniqueID = time();
            $file_name = 'banner_image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($service->banner_image)) {
                unlink($service->banner_image);
            }
            $Newservice['banner_image'] = $path . $file_name;
        }
        if ($request->hasFile('page_image')) {
            $file = $request->file('page_image');
            $uniqueID = time();
            $file_name = 'page_image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($service->page_image)) {
                unlink($service->page_image);
            }
            $Newservice['page_image'] = $path . $file_name;
        }
        $service->update($Newservice);
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service) {
        if (file_exists($service->page_image)) {
            unlink($service->page_image);
        }
        if (file_exists($service->banner_image)) {
            unlink($service->banner_image);
        }
        if (file_exists($service->home_image)) {
            unlink($service->home_image);
        }
        $service->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/services');
    }

}
