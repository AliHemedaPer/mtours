
<div class="col-lg-12"> 
        <div class="form-group">
        <label for="form-field-select-3">
            Parent Category
        </label>
        <select id="form-field-select-3" class="form-control search-select" name="parent_id">
            <option value="0">-- None --</option>
            @foreach($parents as $parent)
                <option value="{{ $parent->id }}" @if(isset($category) && $category->parent_id == $parent->id) selected="" @endif>{{ $parent->translate('en')->title }}</option>
            @endforeach
        </select>
    </div>
</div>

<div style="clear: both;"></div>
<hr/>
<div class="form-group row text-center">
    <label class="col-form-label col-lg-4" style="text-align:center">Show in Home page Popular Destinations ?</label>
    <div class="col-lg-1">       
        {!! Form::checkbox('in_home', null, ($category && $category->in_home == 1)?true:false) !!}
    </div>
</div>
<hr/>
<div style="clear: both;"></div>

<div class="tabbable">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example1" data-toggle="tab">
                Title (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example1" data-toggle="tab">
                Title (SP)
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example1">
            {!! Form::text('title[en]',($category)?$category->translate('en')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example1">
            {!! Form::text('title[sp]',($category)?$category->translate('sp')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4">
        <label>
            Image <span style="direction: ltr;">(450X360)</span>
        </label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                @if($category && file_exists($category->image))
                <img src="{{url($category->image)}}" alt=""/>
                @else
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
                @endif
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                    <input type="file" name="image">
                </span>
                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                    <i class="fa fa-times"></i> Remove
                </a>
            </div>
        </div>                        
    </div>
</div>

<div class="row"> 
    <div class="col-lg-12" style="text-align: right;">       
        <a class="btn btn-danger" href="{{url("admin/categories")}}"><i class="icon-plus2 mr-2"></i>Cancel <i class="fa fa-minus-circle"></i></a>
        <button type="submit" class="btn btn-success">{{$buttonAction}} <i class="fa fa-save"></i></button>
    </div>
</div>
