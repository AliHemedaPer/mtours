@extends('admin.layouts.admin_master')
@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li>
        <a href="{{url('admin/posts')}}">
            Blog Posts
        </a>
    </li>
    <li class="active">
        @if($post)
        Edit
        @else 
        Add
        @endif
    </li>
  
</ol>
<div class="page-header">
    <h1 class="col-md-6">Blog Post</h1>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="{{url("admin/posts")}}"><i class="icon-plus2 mr-2"></i> Back <i class="fa fa-reply"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            
        <div class="panel-body">
        @if($post)
        {!! Form::model($post,["url"=>"admin/posts/$post->id","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
        @include('admin.posts.input',['buttonAction'=>'Save'])
        @else 
        {!! Form::open(["url"=>"admin/posts","class"=>"form-horizontal","method"=>"POST","enctype"=>"multipart/form-data"]) !!}
        @include('admin.posts.input',['buttonAction'=>'Save'])
        @endif
        {!! Form::close() !!}
    </div>
</div>
</div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
@endsection