<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Posts;
use Validator;
use DB;

class PostsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $posts = Posts::all();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $post = NULL;
        return view('admin.posts.form', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/posts/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            $data['image'] = $path . $file_name;
        }
        Posts::create($data);

        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Posts $post) {
        return view('admin.posts.form', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $post) {
        $validator = Validator::make($request->all(), [
                    'title.*' => 'required',
                    'description.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $data = $request->all();
        $path = "uploads/posts/";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $uniqueID = time();
            $file_name = 'image_' . $uniqueID . "." . $file->getClientOriginalExtension();
            $file->move($path, $file_name);
            if (file_exists($post->image)) {
                unlink($post->image);
            }
            $data['image'] = $path . $file_name;
        }
        $post->update($data);
        $request->session()->flash('success', 'تم بنجاح!');
        return redirect('admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posts $post) {
        if (file_exists($post->image)) {
            unlink($post->image);
        }
        $post->delete();
        \Session::flash('success', 'تم بنجاح!');
        return redirect('admin/posts');
    }

}
