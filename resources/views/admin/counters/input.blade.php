<div class="tabbable">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example1" data-toggle="tab">
                العنوان (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example1" data-toggle="tab">
                العنوان (AR)
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example1">
            {!! Form::text('title[en]',($counter)?$counter->translate('en')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example1">
            {!! Form::text('title[ar]',($counter)?$counter->translate('ar')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>
<div class="col-lg-12"> 
        <div class="form-group">
        <label for="form-field-select-3">
            القيمه
        </label>
        {!! Form::text('count',($counter)?$counter->count:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4">
        <label>
            الصوره
        </label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                @if($counter && file_exists($counter->image))
                <img src="{{url($counter->image)}}" alt=""/>
                @else
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
                @endif
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> إختيار</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> تغيير</span>
                    <input type="file" name="image">
                </span>
                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                    <i class="fa fa-times"></i> حذف
                </a>
            </div>
        </div>                        
    </div>
</div>

<div class="row"> 
    <div class="col-lg-3 col-lg-pull-9" style="text-align: left;">       
        <button type="submit" class="btn btn-success">{{$buttonAction}} <i class="fa fa-check"></i></button>
        <a class="btn btn-danger" href="{{url("admin/counters")}}"><i class="icon-plus2 mr-2"></i>إلغاء <i class="fa fa-minus-circle"></i></a>
    </div>
</div>
