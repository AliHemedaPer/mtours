-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.2.3-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mtours.about
CREATE TABLE IF NOT EXISTS `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.about: ~2 rows (approximately)
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` (`id`, `title`, `short_description`, `content`, `image`, `created_at`, `updated_at`) VALUES
	(1, '5', '6', '7', 'uploads/AboutPage/1559445188.png', NULL, '2019-06-02 03:13:08'),
	(2, '101', '', '102', NULL, '2019-07-04 13:30:17', '2019-07-04 13:30:17');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;

-- Dumping structure for table mtours.banners
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.banners: ~2 rows (approximately)
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` (`id`, `title`, `description`, `url`, `image`, `created_at`, `updated_at`) VALUES
	(1, '33', '34', NULL, 'uploads/banner/image_1559190884.jpg', '2019-05-25 05:20:26', '2019-05-30 04:34:44'),
	(2, '42', '43', NULL, 'uploads/banner/image_1559190969.jpg', '2019-05-30 04:36:10', '2019-05-30 04:36:10');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;

-- Dumping structure for table mtours.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `in_home` tinyint(4) NOT NULL DEFAULT 0,
  `full_half` tinyint(4) NOT NULL DEFAULT 0,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.categories: ~13 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `parent_id`, `title`, `in_home`, `full_half`, `image`, `created_at`, `updated_at`) VALUES
	(1, 0, '13', 0, 0, '0', '2019-05-25 01:21:14', '2019-05-25 01:21:14'),
	(2, 1, '14', 1, 1, 'uploads/categories/image_1559450496.jpg', '2019-05-25 01:23:43', '2019-06-02 04:41:36'),
	(3, 1, '15', 1, 0, 'uploads/categories/image_1559009806.png', '2019-05-25 01:27:08', '2019-05-25 01:27:08'),
	(4, 1, '16', 1, 0, 'uploads/categories/image_1559009807.jpg', '2019-05-25 01:30:40', '2019-05-25 01:30:40'),
	(5, 1, '17', 1, 0, 'uploads/categories/image_1559009808.jpg', '2019-05-25 01:31:20', '2019-05-25 01:31:20'),
	(6, 1, '18', 1, 0, 'uploads/categories/image_1559009809.jpg', '2019-05-25 01:31:48', '2019-05-25 01:31:48'),
	(7, 1, '19', 1, 0, 'uploads/categories/image_15590098010.jpg', '2019-05-25 01:32:19', '2019-05-25 01:32:19'),
	(8, 0, '20', 0, 0, '0', '2019-05-25 01:32:56', '2019-05-25 01:32:56'),
	(9, 0, '21', 0, 0, '0', '2019-05-25 01:33:24', '2019-05-25 01:33:24'),
	(10, 9, '22', 0, 0, '0', '2019-05-25 01:33:54', '2019-05-25 01:33:54'),
	(11, 9, '23', 0, 0, '0', '2019-05-25 01:34:20', '2019-05-25 01:34:20'),
	(12, 9, '24', 0, 0, '0', '2019-05-25 01:34:39', '2019-05-25 01:34:39'),
	(13, 9, '25', 0, 0, '0', '2019-05-25 01:35:01', '2019-05-25 01:35:01');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table mtours.clients
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table mtours.clients: ~2 rows (approximately)
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` (`id`, `name`, `email`, `phone`, `passport`, `nationality`, `created_at`, `updated_at`) VALUES
	(27, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', '2019-06-30 15:49:36', '2019-06-30 15:49:36'),
	(29, 'Ali Hemeda', 'senjider2@gmail.com', '1066336172', '123', 'albanian', '2019-06-30 15:54:51', '2019-06-30 15:54:51'),
	(30, 'Ali Hemeda', 'admin@medicare.com', '01111111111111', '111', 'egyptian', '2022-07-14 16:28:34', '2022-07-14 16:28:34');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

-- Dumping structure for table mtours.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.migrations: ~15 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_02_06_232652_create_about_table', 1),
	(4, '2019_02_06_232652_create_banners_table', 1),
	(5, '2019_02_06_232652_create_categories_table', 1),
	(6, '2019_02_06_232652_create_posts_table', 1),
	(7, '2019_02_06_232652_create_services_table', 1),
	(8, '2019_02_06_232652_create_settings_table', 1),
	(9, '2019_02_08_171058_translations', 1),
	(10, '2019_03_08_194639_create_products_table', 1),
	(11, '2019_05_24_232503_create_product_images_table', 1),
	(12, '2019_05_24_232611_create_product_prices_table', 1),
	(13, '2019_05_24_232628_create_product_extras_table', 1),
	(14, '2019_05_24_232824_create_orders_table', 1),
	(15, '2019_05_24_232846_create_order_extras_table', 1),
	(16, '2019_06_28_002636_create_transportaions_table', 2),
	(17, '2019_06_28_002851_create_transportaion_prices_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table mtours.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childs` int(11) DEFAULT NULL,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `guide` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_price` double(10,2) NOT NULL DEFAULT 0.00,
  `payment_type` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cash',
  `invoice_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` tinyint(1) NOT NULL DEFAULT 0,
  `booking_type` enum('tour','transportation') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'tour',
  `order_done` tinyint(1) NOT NULL DEFAULT 0,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `is_available` tinyint(1) DEFAULT NULL,
  `code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.orders: ~38 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user_id`, `product_id`, `adults`, `childs`, `date_from`, `date_to`, `name`, `email`, `phone`, `passport`, `nationality`, `guide`, `note`, `hotel`, `total_price`, `payment_type`, `invoice_id`, `payment_status`, `booking_type`, `order_done`, `is_read`, `is_available`, `code`, `created_at`, `updated_at`) VALUES
	(1, NULL, 3, 2, 1, '2019-06-19', '2019-06-28', 'Ali Hemeda', 'xzy@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 0.00, 'cash', NULL, 0, 'tour', 1, 0, 1, NULL, '2019-06-15 13:00:09', '2019-07-13 20:28:07'),
	(2, NULL, 3, 2, 1, '2019-06-19', '2019-06-28', 'Ali Hemeda', 'abc@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 0.00, 'cash', NULL, 0, 'tour', 1, 0, NULL, NULL, '2019-06-15 13:01:21', '2019-05-15 13:01:21'),
	(3, NULL, 3, 2, 1, '2019-06-19', '2019-06-28', 'Ali Hemeda', 'folow@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 0.00, 'cash', NULL, 0, 'tour', 1, 0, NULL, NULL, '2019-06-15 13:01:33', '2019-06-25 23:28:48'),
	(4, NULL, 3, 2, 1, '2019-06-19', '2019-06-28', 'Ali Hemeda', 'man@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 178.19, 'cash', NULL, 1, 'tour', 1, 0, NULL, NULL, '2019-06-15 13:28:35', '2019-05-15 13:28:35'),
	(5, NULL, 3, 2, 1, '2019-06-19', '2019-06-28', 'Ali Hemeda', 'xyzr@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 178.19, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-15 13:38:05', '2019-06-25 23:28:30'),
	(6, NULL, 3, 2, 1, '2019-06-19', '2019-06-28', 'Ali Hemeda', 'zyas@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 178.19, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-15 13:38:45', '2019-06-25 23:28:57'),
	(7, NULL, 3, 2, 1, '2019-06-19', '2019-06-28', 'Ali Hemeda', 'dfrr@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 178.19, 'cash', NULL, 0, 'tour', 1, 0, NULL, NULL, '2019-06-15 13:41:03', '2019-06-25 23:21:46'),
	(8, NULL, 3, 2, 1, '2019-06-23', '2019-06-26', 'Ali Hemeda', 'sfht@gmail.com', '01066666666', '123456', 'egyptian', 'English', 'test note', NULL, 178.19, 'cash', NULL, 1, 'tour', 1, 0, NULL, NULL, '2019-06-15 13:41:32', '2019-06-25 23:28:40'),
	(9, NULL, 3, 1, 0, '2019-06-08', NULL, 'Ali Hemeda', 'drer@gmail.com', '01066666666', '123456', 'barbudans', 'English', NULL, NULL, 56.65, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-15 13:55:57', '2019-06-14 13:55:57'),
	(10, NULL, 3, 1, 0, '2019-06-08', NULL, 'Ali Hemeda', 'dfjider@gmail.com', '01066666666', '123456', 'barbudans', 'English', NULL, NULL, 66.95, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-15 13:56:28', '2019-06-25 23:28:29'),
	(14, NULL, 3, 1, 0, '2019-06-26', NULL, 'Ali Hemeda', 'sdwer@gmail.com', '01066666666', '123', 'bangladeshi', 'German', '33', NULL, 56.65, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-15 14:05:41', '2019-06-25 23:28:52'),
	(17, NULL, 1, 1, NULL, '2019-06-26', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123456', 'batswana', 'English', 'yyy', NULL, 61.80, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-27 23:07:07', '2019-06-27 23:07:07'),
	(18, NULL, 1, 1, NULL, '2019-06-26', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123456', 'batswana', 'English', 'yyy', NULL, 61.80, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-27 23:07:55', '2019-06-27 23:07:55'),
	(19, NULL, 1, 1, NULL, '2019-06-26', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123456', 'batswana', 'English', 'yyy', NULL, 61.80, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-27 23:08:22', '2019-06-27 23:08:22'),
	(20, NULL, 1, 1, NULL, '2019-06-25', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', 'sd', 'afghan', 'Spanish', 'dd', NULL, 123.60, 'cash', NULL, 0, 'transportation', 0, 0, NULL, NULL, '2019-06-28 04:25:40', '2019-06-28 04:51:04'),
	(22, NULL, 1, 1, NULL, '2019-06-25', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'algerian', 'English', '55', NULL, 61.80, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-29 01:49:23', '2019-06-29 01:49:23'),
	(23, NULL, 1, 1, NULL, '2019-06-25', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'algerian', 'English', '55', NULL, 61.80, 'cash', NULL, 0, 'tour', 0, 1, NULL, NULL, '2019-06-29 01:49:42', '2019-07-08 20:45:12'),
	(24, NULL, 1, 1, NULL, '2019-06-25', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'algerian', 'English', '55', NULL, 61.80, 'cash', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-06-29 01:49:58', '2019-06-29 01:49:58'),
	(26, NULL, 1, 1, NULL, '2019-06-25', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'albanian', 'English', 'hhj', NULL, 123.60, 'cash', NULL, 0, 'transportation', 0, 0, NULL, NULL, '2019-06-29 01:54:16', '2019-06-29 01:54:16'),
	(27, NULL, 1, 1, NULL, '2019-07-02', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'algerian', 'English', '66', NULL, 123.60, 'cash', NULL, 0, 'transportation', 0, 0, NULL, NULL, '2019-07-04 12:39:01', '2019-07-04 12:39:01'),
	(28, NULL, 1, 1, NULL, '2019-07-02', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'algerian', 'English', '66', NULL, 123.60, 'cash', NULL, 1, 'transportation', 0, 0, NULL, NULL, '2019-07-04 12:39:17', '2019-07-04 12:40:27'),
	(29, NULL, 1, 1, NULL, '2019-07-31', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'English', 'tt', NULL, 123.60, 'cash', NULL, 1, 'transportation', 0, 1, NULL, NULL, '2019-07-04 12:43:02', '2019-07-08 20:43:05'),
	(30, NULL, 2, 1, 0, '2019-07-29', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'algerian', 'English', 'gg', NULL, 82.40, 'cash', NULL, 1, 'tour', 0, 0, NULL, NULL, '2019-07-04 12:43:56', '2019-07-04 12:43:57'),
	(31, NULL, 1, 1, NULL, '2019-06-05', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'belgian', 'Spanish', 'gg', NULL, 123.60, 'cash', NULL, 1, 'transportation', 0, 0, NULL, NULL, '2019-07-04 12:45:24', '2019-07-08 20:43:01'),
	(32, NULL, 1, 1, NULL, '2019-06-18', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'beninese', 'Spanish', '66', NULL, 123.60, 'visa', NULL, 1, 'transportation', 1, 1, NULL, NULL, '2019-07-04 12:47:08', '2019-08-31 17:49:33'),
	(33, NULL, 3, 1, 0, '2019-07-04', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'English', 'test', NULL, 56.65, 'visa', NULL, 0, 'tour', 0, 0, NULL, NULL, '2019-07-07 20:21:19', '2019-07-07 20:21:19'),
	(34, NULL, 1, 1, NULL, '2019-07-01', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'Spanish', 'ttttttttt', 'Meredian', 92.70, 'cash', NULL, 1, 'tour', 0, 0, NULL, NULL, '2019-07-07 21:19:33', '2019-07-07 21:19:34'),
	(35, NULL, 1, 3, NULL, '2019-07-17', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'English', 'aabbcc', 'Hilton', 154.50, 'cash', NULL, 1, 'tour', 0, 0, NULL, NULL, '2019-07-07 22:31:49', '2019-07-07 22:31:49'),
	(36, NULL, 1, 2, NULL, '2019-07-30', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'English', 'aabbccdd', 'Hilton', 164.80, 'cash', NULL, 1, 'tour', 0, 1, NULL, NULL, '2019-07-07 22:32:48', '2019-07-08 20:43:59'),
	(37, NULL, 1, 2, NULL, '2019-07-16', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'English', 'aabbccdd', 'Hilton', 206.00, 'cash', NULL, 1, 'tour', 0, 1, NULL, NULL, '2019-07-07 22:33:43', '2019-07-08 20:51:57'),
	(38, NULL, 5, 1, 0, '2019-07-01', '2019-07-30', 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'English', 'gg', 'Hilton', 180.25, 'visa', NULL, 0, 'tour', 0, 1, 1, '0ycd8bTbjy1563048599Q4lIJXrz9M', '2019-07-13 16:54:23', '2019-07-13 22:00:59'),
	(39, NULL, 4, 2, 3, '2019-07-01', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'English', 'test', 'Hilton', 1333.85, 'cash', NULL, 1, 'tour', 0, 1, NULL, 'LeUBLEjsOm1563055771sLqjnkhC8o', '2019-07-13 22:09:31', '2019-07-13 22:09:41'),
	(40, NULL, 2, 4, 0, '2019-07-31', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'afghan', 'Russian', '123456', 'Meridian', 412.00, 'cash', NULL, 1, 'tour', 0, 0, NULL, 'Q3ggjW0qgM1563225788xEsjcmb9IC', '2019-07-15 21:23:08', '2019-07-15 21:23:09'),
	(41, NULL, 4, 1, 0, '2019-07-30', '2019-07-30', 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'albanian', 'German', 'ddd', 'Meridian', 432.60, 'cash', NULL, 1, 'tour', 0, 0, 1, 'iaBJUz1Wbm1563226445prki7UNoel', '2019-07-15 21:34:05', '2019-07-15 21:34:06'),
	(42, NULL, 5, 1, 0, '2019-07-29', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'albanian', 'Spanish', 'aaa', 'Meridian', 180.25, 'cash', NULL, 1, 'tour', 0, 1, 1, 'fUJwpAf7a81563226507aFP9l5oIVj', '2019-07-15 21:35:07', '2019-07-15 21:37:29'),
	(43, NULL, 4, 2, 0, '2019-07-09', NULL, 'Ali Hemeda', 'senjider@gmail.com', '1066336172', '123', 'albanian', 'Spanish', 'exclude Childrens from Optional Activities', 'Meridian', 782.80, 'cash', NULL, 1, 'tour', 0, 1, 1, 'QSwSjemSCC1563551313YqbJUOF6ZI', '2019-07-19 15:48:34', '2022-07-15 15:21:52'),
	(45, NULL, 2, 3, 1, '2022-07-15', NULL, 'Ali Hemeda', 'admin@medicare.com', '01111111111111', '111', 'egyptian', 'English', NULL, 'test', 427.45, 'cash', NULL, 2, 'tour', 0, 1, NULL, 'ATymbk1bWB1657816177sRRw6EIcTo', '2022-07-14 16:29:37', '2022-07-14 16:35:30'),
	(48, NULL, 2, 1, 2, '2022-07-15', NULL, 'Ali Hemeda', 'admin@medicare.com', '01111111111111', '111', 'afghan', 'English', NULL, 'test', 113.30, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'IfhG88dPZ31657820275FFrBnDZcUc', '2022-07-14 17:37:55', '2022-07-14 17:37:55'),
	(49, NULL, 2, 1, 2, '2022-07-15', NULL, 'Ali Hemeda', 'admin@medicare.com', '01111111111111', '111', 'afghan', 'English', NULL, 'test', 113.30, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'gn20XrMvOa16578202983ckj6mIT3P', '2022-07-14 17:38:18', '2022-07-14 17:38:18'),
	(50, NULL, 2, 1, 2, '2022-07-15', NULL, 'Ali Hemeda', 'admin@medicare.com', '01111111111111', '111', 'afghan', 'English', NULL, 'test', 113.30, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'iN7Mexh00Q1657820316wUWxiepJFU', '2022-07-14 17:38:36', '2022-07-14 17:38:36'),
	(51, NULL, 2, 1, 2, '2022-07-15', NULL, 'Ali Hemeda', 'admin@medicare.com', '01111111111111', '111', 'afghan', 'English', NULL, 'test', 113.30, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'idFj5BpnBg16578203615wooxVOHYN', '2022-07-14 17:39:21', '2022-07-14 17:39:21'),
	(52, NULL, 2, 1, 2, '2022-07-15', NULL, 'Ali Hemeda', 'admin@medicare.com', '01111111111111', '111', 'afghan', 'English', NULL, 'test', 113.30, 'visa', NULL, 0, 'tour', 0, 0, NULL, 'bkK0RDV6w81657820399zGjj047x4i', '2022-07-14 17:39:59', '2022-07-14 17:39:59');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table mtours.order_extras
CREATE TABLE IF NOT EXISTS `order_extras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_extras_id` int(11) NOT NULL,
  `adults` int(11) DEFAULT NULL,
  `childs` int(11) DEFAULT NULL,
  `total_price` double(10,2) DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.order_extras: ~43 rows (approximately)
/*!40000 ALTER TABLE `order_extras` DISABLE KEYS */;
INSERT INTO `order_extras` (`id`, `order_id`, `product_extras_id`, `adults`, `childs`, `total_price`, `created_at`, `updated_at`) VALUES
	(1, 3, 44, NULL, NULL, 0.00, NULL, NULL),
	(2, 3, 45, NULL, NULL, 0.00, NULL, NULL),
	(3, 4, 44, NULL, NULL, 0.00, NULL, NULL),
	(4, 4, 45, NULL, NULL, 0.00, NULL, NULL),
	(5, 4, 46, NULL, NULL, 0.00, NULL, NULL),
	(6, 5, 44, NULL, NULL, 0.00, NULL, NULL),
	(7, 5, 45, NULL, NULL, 0.00, NULL, NULL),
	(8, 5, 46, NULL, NULL, 0.00, NULL, NULL),
	(9, 6, 44, NULL, NULL, 0.00, NULL, NULL),
	(10, 6, 45, NULL, NULL, 0.00, NULL, NULL),
	(11, 6, 46, NULL, NULL, 0.00, NULL, NULL),
	(12, 8, 44, NULL, NULL, 0.00, NULL, NULL),
	(13, 8, 45, NULL, NULL, 0.00, NULL, NULL),
	(14, 8, 46, NULL, NULL, 0.00, NULL, NULL),
	(15, 10, 44, NULL, NULL, 0.00, NULL, NULL),
	(16, 30, 59, NULL, NULL, 0.00, NULL, NULL),
	(17, 30, 60, NULL, NULL, 0.00, NULL, NULL),
	(18, 34, 64, NULL, NULL, 0.00, NULL, NULL),
	(19, 34, 65, NULL, NULL, 0.00, NULL, NULL),
	(20, 34, 66, NULL, NULL, 0.00, NULL, NULL),
	(21, 34, 67, NULL, NULL, 0.00, NULL, NULL),
	(22, 36, 64, NULL, NULL, 0.00, NULL, NULL),
	(23, 36, 65, NULL, NULL, 0.00, NULL, NULL),
	(24, 36, 66, NULL, NULL, 0.00, NULL, NULL),
	(25, 36, 67, NULL, NULL, 0.00, NULL, NULL),
	(26, 37, 64, NULL, NULL, 0.00, NULL, NULL),
	(27, 37, 65, NULL, NULL, 0.00, NULL, NULL),
	(28, 37, 66, NULL, NULL, 0.00, NULL, NULL),
	(29, 37, 67, NULL, NULL, 0.00, NULL, NULL),
	(30, 37, 68, NULL, NULL, 0.00, NULL, NULL),
	(31, 40, 58, NULL, NULL, 0.00, NULL, NULL),
	(32, 40, 59, NULL, NULL, 0.00, NULL, NULL),
	(33, 40, 60, NULL, NULL, 0.00, NULL, NULL),
	(34, 40, 61, NULL, NULL, 0.00, NULL, NULL),
	(35, 40, 62, NULL, NULL, 0.00, NULL, NULL),
	(36, 40, 63, NULL, NULL, 0.00, NULL, NULL),
	(37, 43, 54, NULL, NULL, 0.00, NULL, NULL),
	(38, 45, 58, NULL, NULL, 0.00, NULL, NULL),
	(39, 45, 59, NULL, NULL, 0.00, NULL, NULL),
	(40, 45, 60, NULL, NULL, 0.00, NULL, NULL),
	(41, 45, 61, NULL, NULL, 0.00, NULL, NULL),
	(42, 45, 62, NULL, NULL, 0.00, NULL, NULL),
	(43, 45, 63, NULL, NULL, 0.00, NULL, NULL);
/*!40000 ALTER TABLE `order_extras` ENABLE KEYS */;

-- Dumping structure for table mtours.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table mtours.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.posts: ~4 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
	(1, '40', '41', 'uploads/posts/image_1559449831.jpg', '2019-05-28 02:37:43', '2019-06-02 04:30:31'),
	(2, '46', '47', 'uploads/posts/image_1559364113.jpg', '2019-06-01 04:41:53', '2019-06-01 04:41:53'),
	(3, '71', '72', 'uploads/posts/image_1559450074.jpg', '2019-06-02 04:34:34', '2019-06-02 04:34:34'),
	(4, '73', '74', 'uploads/posts/image_1559450102.jpg', '2019-06-02 04:35:02', '2019-06-02 04:35:02');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table mtours.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `min_price` double(10,2) NOT NULL DEFAULT 0.00,
  `child_from` int(11) DEFAULT NULL,
  `child_to` int(11) DEFAULT NULL,
  `child_price` double(10,2) DEFAULT NULL,
  `day_type` enum('half','full','days') COLLATE utf8_unicode_ci NOT NULL,
  `days` int(11) NOT NULL DEFAULT 0,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `booked` int(11) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot_offer` tinyint(1) DEFAULT 0,
  `check_avail` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.products: ~5 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `cat_id`, `title`, `description`, `price`, `min_price`, `child_from`, `child_to`, `child_price`, `day_type`, `days`, `date_from`, `date_to`, `views`, `booked`, `image`, `hot_offer`, `check_avail`, `created_at`, `updated_at`) VALUES
	(1, 2, '29', '30', 60.00, 40.00, NULL, NULL, NULL, 'full', 1, NULL, NULL, 89, 0, 'uploads/products/image_1559445863.jpg', 1, 0, '2019-05-25 02:31:16', '2022-07-15 14:38:14'),
	(2, 2, '31', '32', 60.00, 40.00, 6, 10, 25.00, 'half', 1, NULL, NULL, 62, 0, 'uploads/products/image_1559446621.jpg', 1, 0, '2019-05-25 03:54:23', '2022-07-14 16:27:07'),
	(3, 2, '35', '36', 55.00, 35.00, 6, 10, 23.00, 'full', 1, NULL, NULL, 25, 0, 'uploads/products/image_1559447178.jpg', 1, 0, '2019-05-26 00:40:59', '2022-07-15 14:15:34'),
	(4, 3, '65', '66', 420.00, 345.00, 6, 11, 185.00, 'full', 1, NULL, NULL, 28, 0, 'uploads/products/image_1559451428.jpg', 1, 1, '2019-06-02 03:51:35', '2022-07-14 16:37:26'),
	(5, 8, '68', '69', 175.00, 105.00, 6, 11, 62.50, 'full', 1, NULL, NULL, 4, 0, 'uploads/products/image_1559448155.jpg', 1, 1, '2019-06-02 04:02:36', '2019-07-13 16:35:24');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table mtours.product_extras
CREATE TABLE IF NOT EXISTS `product_extras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.product_extras: ~19 rows (approximately)
/*!40000 ALTER TABLE `product_extras` DISABLE KEYS */;
INSERT INTO `product_extras` (`id`, `product_id`, `title`, `price`, `created_at`, `updated_at`) VALUES
	(54, 4, '79', 10.00, '2019-06-21 02:12:09', '2019-06-21 02:12:09'),
	(55, 3, '80', 10.00, '2019-06-21 02:12:34', '2019-06-21 02:12:34'),
	(56, 3, '81', 10.00, '2019-06-21 02:12:34', '2019-06-21 02:12:34'),
	(57, 3, '82', 10.00, '2019-06-21 02:12:34', '2019-06-21 02:12:34'),
	(58, 2, '83', NULL, '2019-06-21 02:12:52', '2019-06-21 02:12:52'),
	(59, 2, '84', NULL, '2019-06-21 02:12:52', '2019-06-21 02:12:52'),
	(60, 2, '85', 20.00, '2019-06-21 02:12:53', '2019-06-21 02:12:53'),
	(61, 2, '86', 10.00, '2019-06-21 02:12:53', '2019-06-21 02:12:53'),
	(62, 2, '87', 20.00, '2019-06-21 02:12:53', '2019-06-21 02:12:53'),
	(63, 2, '88', 10.00, '2019-06-21 02:12:54', '2019-06-21 02:12:54'),
	(64, 1, '89', NULL, '2019-06-21 02:13:08', '2019-06-21 02:13:08'),
	(65, 1, '90', NULL, '2019-06-21 02:13:08', '2019-06-21 02:13:08'),
	(66, 1, '91', 20.00, '2019-06-21 02:13:08', '2019-06-21 02:13:08'),
	(67, 1, '92', 10.00, '2019-06-21 02:13:08', '2019-06-21 02:13:08'),
	(68, 1, '93', 20.00, '2019-06-21 02:13:09', '2019-06-21 02:13:09'),
	(69, 1, '94', 10.00, '2019-06-21 02:13:09', '2019-06-21 02:13:09'),
	(70, 1, '95', NULL, '2019-06-21 02:13:09', '2019-06-21 02:13:09'),
	(71, 1, '96', NULL, '2019-06-21 02:13:10', '2019-06-21 02:13:10'),
	(73, 5, '103', 30.00, '2019-07-13 16:32:06', '2019-07-13 16:32:06');
/*!40000 ALTER TABLE `product_extras` ENABLE KEYS */;

-- Dumping structure for table mtours.product_images
CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.product_images: ~8 rows (approximately)
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
	(11, 1, 'uploads/products/images/image_1560903826.jpg', NULL, NULL),
	(14, 1, 'uploads/products/images/image_1560903907.jpg', NULL, NULL),
	(15, 1, 'uploads/products/images/image_1560903919.jpg', NULL, NULL),
	(24, 2, 'uploads/products/images/image_156090476724.jpg', '2019-06-19 00:39:27', '2019-06-19 00:39:27'),
	(25, 2, 'uploads/products/images/image_156090476725.jpg', '2019-06-19 00:39:27', '2019-06-19 00:39:27'),
	(26, 2, 'uploads/products/images/image_156090476826.jpg', '2019-06-19 00:39:28', '2019-06-19 00:39:28'),
	(27, 1, 'uploads/products/images/image_156107314127.jpg', '2019-06-20 23:25:41', '2019-06-20 23:25:41'),
	(28, 5, 'uploads/products/images/image_157868847128.jpg', '2020-01-10 20:34:31', '2020-01-10 20:34:31');
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;

-- Dumping structure for table mtours.product_prices
CREATE TABLE IF NOT EXISTS `product_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.product_prices: ~8 rows (approximately)
/*!40000 ALTER TABLE `product_prices` DISABLE KEYS */;
INSERT INTO `product_prices` (`id`, `product_id`, `from`, `to`, `price`, `created_at`, `updated_at`) VALUES
	(78, 4, 2, 3, 370.00, NULL, NULL),
	(79, 4, 4, 9, 345.00, NULL, NULL),
	(80, 3, 2, 3, 45.00, NULL, NULL),
	(81, 3, 4, 6, 35.00, NULL, NULL),
	(82, 2, 2, 3, 50.00, NULL, NULL),
	(83, 2, 4, 6, 40.00, NULL, NULL),
	(84, 1, 2, 3, 50.00, NULL, NULL),
	(85, 1, 4, 6, 40.00, NULL, NULL),
	(88, 5, 2, 3, 125.00, NULL, NULL),
	(89, 5, 4, 9, 105.00, NULL, NULL);
/*!40000 ALTER TABLE `product_prices` ENABLE KEYS */;

-- Dumping structure for table mtours.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `home_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.services: ~0 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `title`, `description`, `home_image`, `created_at`, `updated_at`) VALUES
	(1, '10', '11', 'uploads/services/home_image_1549665773.png', NULL, NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table mtours.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_fees` int(2) NOT NULL,
  `app_fb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_tw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_in` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_wats` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fawaterak_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_cash` tinyint(4) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.settings: ~1 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `app_name`, `app_email`, `app_phone`, `app_address`, `app_currency`, `app_fees`, `app_fb`, `app_tw`, `app_in`, `app_wats`, `fawaterak_key`, `show_cash`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'M-Tours', 'support@mtours.com', '+20 106666666', '5th St. Cairo, Egypt.', 'USD', 3, '#', '#', '#', '0106666666', '85d4811d7956bc526c5130bf53c5b963f274e4381a9ba552b0', 1, 'uploads/logo/logo.png', NULL, '2019-07-04 13:58:42');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table mtours.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `translations_group_id_index` (`group_id`),
  KEY `translations_locale_index` (`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.translations: ~152 rows (approximately)
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` (`id`, `group_id`, `value`, `locale`) VALUES
	(4, 3, 'Permanent design service', 'en'),
	(5, 3, 'Permanent design service', 'sp'),
	(6, 4, 'Sign up now with our Instant Design service and save your monthly expenses...', 'en'),
	(7, 4, 'Sign up now with our Instant Design service and save your monthly expenses...', 'sp'),
	(8, 5, 'About MTours', 'en'),
	(9, 5, 'MTours', 'sp'),
	(10, 6, 'We are very excited to introduce M TOURS as a provider for all your travel needs and requirements in Egypt. We label ourselves as innovative and highly efficient travel agency and we look forward to demonstrating these qualities to let you have an unforgettable and a unique experience in Egypt. Our primary aim is to provide quality travel services to our travelers and guests. \r\nIf you would like a tour that is tailored exactly like the way you want, you found the right company. Send us a request and we would be happy to assist! Also, if this is your first time to Egypt and you want a designed-by-experts tour to make the most of your time, without all the thinking and planning, have a look at some our all-time popular and available itineraries! \r\n***Please have a look at our FAQs page for more information on our service. Check out our Gallery page for some of the awesome sights you may get to see on our tours as well!', 'en'),
	(11, 6, 'We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .', 'sp'),
	(12, 7, '<p>Terms &amp; Conditions</p>', 'en'),
	(13, 7, '<p>We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .</p>', 'sp'),
	(14, 8, 'Marketing', 'en'),
	(15, 8, 'Marketing', 'sp'),
	(16, 9, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'en'),
	(17, 9, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'sp'),
	(18, 10, 'Marketing', 'en'),
	(19, 10, 'Marketing', 'sp'),
	(20, 11, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'en'),
	(21, 11, 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.', 'sp'),
	(22, 12, 'Day Tour', 'en'),
	(23, 12, 'Dia de paseo', 'sp'),
	(24, 13, 'Day Tour', 'en'),
	(25, 13, 'Dia de paseo', 'sp'),
	(26, 14, 'Cairo', 'en'),
	(27, 14, 'El Cairo', 'sp'),
	(28, 15, 'Luxur', 'en'),
	(29, 15, 'Luxur', 'sp'),
	(30, 16, 'Alexandira', 'en'),
	(31, 16, 'Alejandría', 'sp'),
	(32, 17, 'Aswan', 'en'),
	(33, 17, 'Aswan', 'sp'),
	(34, 18, 'Hurghada', 'en'),
	(35, 18, 'Hurghada', 'sp'),
	(36, 19, 'Sharm El-SHiekh', 'en'),
	(37, 19, 'Sharm El-SHiekh', 'sp'),
	(38, 20, 'Multi Days Packages', 'en'),
	(39, 20, 'Paquetes Multi Días', 'sp'),
	(40, 21, 'Night Cruise Trip', 'en'),
	(41, 21, 'Crucero nocturno', 'sp'),
	(42, 22, '5 Stars', 'en'),
	(43, 22, '5 estrellas', 'sp'),
	(44, 23, 'Dinner', 'en'),
	(45, 23, 'Cena', 'sp'),
	(46, 24, 'Luxury', 'en'),
	(47, 24, 'Lujo', 'sp'),
	(48, 25, 'Falouka Ride', 'en'),
	(49, 25, 'Paseo de Falouka', 'sp'),
	(52, 27, 'Cairo Tours to Giza Pyramids, Egyptian Museum and Khan EL-Khalili Bazaar', 'en'),
	(53, 27, 'Cairo Tours to Giza Pyramids, Egyptian Museum and Khan EL-Khalili Bazaar', 'sp'),
	(54, 28, '<p>Eight-hour tour starts every day from 8 am to 4 pm. Thetour includes pick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local restaurant, service charges, taxes and snacks bag (A bottle of water, a can of soft drink, chips and a cake)</p>\r\n\r\n<p>All Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The tour excludes personal items, tipping and any Optional Activities.</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>GIZA PYRAMIDS, EGYPTIAN MUSEUM AND KHAN EL-KHALILI BAZAAR PROGRAM:</h3>\r\n\r\n<p>The tour starts at 08:00 am.&nbsp;<strong>M Tours</strong>&nbsp;guide will pick you up from your hotel whether in Cairo or Giza to start the full day tour. It begins with&nbsp;<strong>Giza Pyramid Complex</strong>&nbsp;where you can visit the&nbsp;<strong>Three Great Pyramids</strong>; Cheops, Chephren and Mykerinos,&nbsp;<strong>The Queens&#39; Pyramids</strong>,&nbsp;<strong>The Valley Temple&nbsp;</strong>and&nbsp;<strong>The Sphinx</strong>, the mythical creature with a body of a lion and a human face, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that, your day tour continues to&nbsp;<strong>The Egyptian Museum</strong>, one of the largest museums in the region. The Egyptian Museum of Antiquities contains many important pieces of ancient Egyptian history. It hosts the world&#39;s largest collection of Pharaonic antiquities. It has 120,000 items, with a representative amount on display. It also includes&nbsp;<strong>King Tutankhamon</strong>&#39;s fascinating gold collection.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Then, the tour proceeds with&nbsp;<strong>Khan El-Khalili</strong>, which is the major Souk in the historic center of Islamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike. You can shop from there some memorable souvenirs as copper handicrafts, perfumes, leather, silver and gold products, antiques and many other special products.</p>\r\n\r\n<p><strong>Lunch is included</strong>&nbsp;during any of the tours at a Local Restaurant. (Full Meal for each person; beverages excluded)</p>\r\n\r\n<p>The Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The Tour Includes:</strong></p>\r\n\r\n<ul>\r\n	<li>A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)</li>\r\n	<li>A short camel ride (15 minutes)</li>\r\n	<li>Lunch at a Local Restaurant (beverages excluded)</li>\r\n	<li>Pick-up and drop off</li>\r\n	<li>Expert Tour guide</li>\r\n</ul>', 'en'),
	(55, 28, '<p>Eight-hour tour starts every day from 8 am to 4 pm. Thetour includes pick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local restaurant, service charges, taxes and snacks bag (A bottle of water, a can of soft drink, chips and a cake)</p>\r\n\r\n<p>All Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The tour excludes personal items, tipping and any Optional Activities.</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>GIZA PYRAMIDS, EGYPTIAN MUSEUM AND KHAN EL-KHALILI BAZAAR PROGRAM:</h3>\r\n\r\n<p>The tour starts at 08:00 am. <strong>M Tours</strong> guide will pick you up from your hotel whether in Cairo or Giza to start the full day tour. It begins with <strong>Giza Pyramid Complex</strong> where you can visit the <strong>Three Great Pyramids</strong>; Cheops, Chephren and Mykerinos, <strong>The Queens&#39; Pyramids</strong>, <strong>The Valley Temple </strong>and <strong>The Sphinx</strong>, the mythical creature with a body of a lion and a human face, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that, your day tour continues to <strong>The Egyptian Museum</strong>, one of the largest museums in the region. The Egyptian Museum of Antiquities contains many important pieces of ancient Egyptian history. It hosts the world&#39;s largest collection of Pharaonic antiquities. It has 120,000 items, with a representative amount on display. It also includes <strong>King Tutankhamon</strong>&#39;s fascinating gold collection.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Then, the tour proceeds with <strong>Khan El-Khalili</strong>, which is the major Souk in the historic center of Islamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike. You can shop from there some memorable souvenirs as copper handicrafts, perfumes, leather, silver and gold products, antiques and many other special products.</p>\r\n\r\n<p><strong>Lunch is included</strong> during any of the tours at a Local Restaurant. (Full Meal for each person; beverages excluded)</p>\r\n\r\n<p>The Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The Tour Includes:</strong></p>\r\n\r\n<ul>\r\n	<li>A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)</li>\r\n	<li>A short camel ride (15 minutes)</li>\r\n	<li>Lunch at a Local Restaurant (beverages excluded)</li>\r\n	<li>Pick-up and drop off</li>\r\n	<li>Expert Tour guide</li>\r\n</ul>', 'sp'),
	(56, 29, 'Cairo Tours to Giza Pyramids, Egyptian Museum and Khan EL-Khalili Bazaar', 'en'),
	(57, 29, 'Cairo Tours to Giza Pyramids, Egyptian Museum and Khan EL-Khalili Bazaar', 'sp'),
	(58, 30, '<p>Eight-hour tour starts every day from 8 am to 4 pm. Thetour includes pick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local restaurant, service charges, taxes and snacks bag (A bottle of water, a can of soft drink, chips and a cake)</p>\r\n\r\n<p>All Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The tour excludes personal items, tipping and any Optional Activities.</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>GIZA PYRAMIDS, EGYPTIAN MUSEUM AND KHAN EL-KHALILI BAZAAR PROGRAM:</h3>\r\n\r\n<p>The tour starts at 08:00 am.&nbsp;<strong>M Tours</strong>&nbsp;guide will pick you up from your hotel whether in Cairo or Giza to start the full day tour. It begins with&nbsp;<strong>Giza Pyramid Complex</strong>&nbsp;where you can visit the&nbsp;<strong>Three Great Pyramids</strong>; Cheops, Chephren and Mykerinos,&nbsp;<strong>The Queens&#39; Pyramids</strong>,&nbsp;<strong>The Valley Temple&nbsp;</strong>and&nbsp;<strong>The Sphinx</strong>, the mythical creature with a body of a lion and a human face, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that, your day tour continues to&nbsp;<strong>The Egyptian Museum</strong>, one of the largest museums in the region. The Egyptian Museum of Antiquities contains many important pieces of ancient Egyptian history. It hosts the world&#39;s largest collection of Pharaonic antiquities. It has 120,000 items, with a representative amount on display. It also includes&nbsp;<strong>King Tutankhamon</strong>&#39;s fascinating gold collection.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Then, the tour proceeds with&nbsp;<strong>Khan El-Khalili</strong>, which is the major Souk in the historic center of Islamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike. You can shop from there some memorable souvenirs as copper handicrafts, perfumes, leather, silver and gold products, antiques and many other special products.</p>\r\n\r\n<p><strong>Lunch is included</strong>&nbsp;during any of the tours at a Local Restaurant. (Full Meal for each person; beverages excluded)</p>\r\n\r\n<p>The Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The Tour Includes:</strong></p>\r\n\r\n<ul>\r\n	<li>A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)</li>\r\n	<li>A short camel ride (15 minutes)</li>\r\n	<li>Lunch at a Local Restaurant (beverages excluded)</li>\r\n	<li>Pick-up and drop off</li>\r\n	<li>Expert Tour guide</li>\r\n</ul>', 'en'),
	(59, 30, '<p>Eight-hour tour starts every day from 8 am to 4 pm. Thetour includes pick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local restaurant, service charges, taxes and snacks bag (A bottle of water, a can of soft drink, chips and a cake)</p>\r\n\r\n<p>All Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The tour excludes personal items, tipping and any Optional Activities.</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>GIZA PYRAMIDS, EGYPTIAN MUSEUM AND KHAN EL-KHALILI BAZAAR PROGRAM:</h3>\r\n\r\n<p>The tour starts at 08:00 am. <strong>M Tours</strong> guide will pick you up from your hotel whether in Cairo or Giza to start the full day tour. It begins with <strong>Giza Pyramid Complex</strong> where you can visit the <strong>Three Great Pyramids</strong>; Cheops, Chephren and Mykerinos, <strong>The Queens&#39; Pyramids</strong>, <strong>The Valley Temple </strong>and <strong>The Sphinx</strong>, the mythical creature with a body of a lion and a human face, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that, your day tour continues to <strong>The Egyptian Museum</strong>, one of the largest museums in the region. The Egyptian Museum of Antiquities contains many important pieces of ancient Egyptian history. It hosts the world&#39;s largest collection of Pharaonic antiquities. It has 120,000 items, with a representative amount on display. It also includes <strong>King Tutankhamon</strong>&#39;s fascinating gold collection.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Then, the tour proceeds with <strong>Khan El-Khalili</strong>, which is the major Souk in the historic center of Islamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike. You can shop from there some memorable souvenirs as copper handicrafts, perfumes, leather, silver and gold products, antiques and many other special products.</p>\r\n\r\n<p><strong>Lunch is included</strong> during any of the tours at a Local Restaurant. (Full Meal for each person; beverages excluded)</p>\r\n\r\n<p>The Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The Tour Includes:</strong></p>\r\n\r\n<ul>\r\n	<li>A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)</li>\r\n	<li>A short camel ride (15 minutes)</li>\r\n	<li>Lunch at a Local Restaurant (beverages excluded)</li>\r\n	<li>Pick-up and drop off</li>\r\n	<li>Expert Tour guide</li>\r\n</ul>', 'sp'),
	(60, 31, 'DAY TOUR TO GIZA PYRAMIDS, MEMPHIS CITY AND SAQQARA PYRAMID', 'en'),
	(61, 31, 'DAY TOUR TO GIZA PYRAMIDS, MEMPHIS CITY AND SAQQARA PYRAMID', 'sp'),
	(62, 32, '<p>Eight-hour tour starts every day from 07:30 am, 08:00 am or 09:00 am. The tour includes<br />\r\npick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour<br />\r\nguide, lunch at a local restaurant, a camel ride, all Taxes Services and snacks bag (A bottle of<br />\r\nwater, a can of soft drink, chips and a cake)<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping and any Optional Activities.</strong></p>\r\n\r\n<p>GIZA PYRAMIDS, MEMPHIS CITY, SAQQARA &amp; DAHSHUR PYRAMIDS TOUR PROGRAM:&nbsp;<br />\r\nThe tour starts at 08:00 am. M Tours guide will pick you up from your hotel whether in Cairo<br />\r\nor Giza to start the full day tour. It begins with Giza Pyramid Complex where you can visit<br />\r\nthe Three Great Pyramids; Cheops, Chephren and Mykerinos, The Queens&#39; Pyramids, The<br />\r\nValley Temple and The Sphinx, the mythical creature with a body of a lion and a human<br />\r\nface, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that you will visit Saqqara which features numerous pyramids, including the world-<br />\r\nfamous Step pyramid of Djoser, sometimes referred to as the Step Tomb due to its<br />\r\nrectangular base, as well as a number of&nbsp; mastabas&nbsp; (meaning &#39;bench&#39;). Saqqara is located<br />\r\nabout 30 KMsouth of Cairo.<br />\r\nLast but not least, we will visit Memphis City which was the ancient Egyptian. It is located 20<br />\r\nKM south of Giza.<br />\r\nLunch is included during any of the tours at a Local Restaurant. (Full Meal for each person;<br />\r\nbeverages excluded)<br />\r\nThe Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>The Tour Includes:<br />\r\n- A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)<br />\r\n- A short camel ride (15 minutes)<br />\r\n- Lunch at a Local Restaurant (beverages excluded)<br />\r\n- Pick-up and drop off<br />\r\n- Expert Tour guide</p>', 'en'),
	(63, 32, '<p>Eight-hour tour starts every day from 07:30 am, 08:00 am or 09:00 am. The tour includes<br />\r\npick-up and drop off transfers from customers&prime; location in Cairo, entry fees, expert tour<br />\r\nguide, lunch at a local restaurant, a camel ride, all Taxes Services and snacks bag (A bottle of<br />\r\nwater, a can of soft drink, chips and a cake)<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping and any Optional Activities.</strong></p>\r\n\r\n<p>GIZA PYRAMIDS, MEMPHIS CITY, SAQQARA &amp; DAHSHUR PYRAMIDS TOUR PROGRAM:&nbsp;<br />\r\nThe tour starts at 08:00 am. M Tours guide will pick you up from your hotel whether in Cairo<br />\r\nor Giza to start the full day tour. It begins with Giza Pyramid Complex where you can visit<br />\r\nthe Three Great Pyramids; Cheops, Chephren and Mykerinos, The Queens&#39; Pyramids, The<br />\r\nValley Temple and The Sphinx, the mythical creature with a body of a lion and a human<br />\r\nface, believed to present Chephren&#39;s face.</p>\r\n\r\n<p>After that you will visit Saqqara which features numerous pyramids, including the world-<br />\r\nfamous Step pyramid of Djoser, sometimes referred to as the Step Tomb due to its<br />\r\nrectangular base, as well as a number of&nbsp; mastabas&nbsp; (meaning &#39;bench&#39;). Saqqara is located<br />\r\nabout 30 KMsouth of Cairo.<br />\r\nLast but not least, we will visit Memphis City which was the ancient Egyptian. It is located 20<br />\r\nKM south of Giza.<br />\r\nLunch is included during any of the tours at a Local Restaurant. (Full Meal for each person;<br />\r\nbeverages excluded)<br />\r\nThe Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>The Tour Includes:<br />\r\n- A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)<br />\r\n- A short camel ride (15 minutes)<br />\r\n- Lunch at a Local Restaurant (beverages excluded)<br />\r\n- Pick-up and drop off<br />\r\n- Expert Tour guide</p>', 'sp'),
	(64, 33, 'The Best Realx for Us', 'en'),
	(65, 33, 'The Best Realx for Us', 'sp'),
	(66, 34, 'Make your life Better', 'en'),
	(67, 34, 'Make your life Better', 'sp'),
	(68, 35, 'Islamic and Coptic Cairo', 'en'),
	(69, 35, 'Islamic and Coptic Cairo', 'sp'),
	(70, 36, '<p>Eight-hour tour starts every day from 8 am to 4 pm. The tour includes pick-up and drop off<br />\r\ntransfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local<br />\r\nrestaurant, all Taxes Services and a bag of snacks (A bottle of water, a can of soft drink, chips<br />\r\nand a cake)<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping and any Optional Activities.</strong></p>\r\n\r\n<p>CAIRO DAY TOUR TO CHRISTIAN &amp; ISLAMIC SIGHTS PROGRAM:</p>\r\n\r\n<p>The tour starts at 08:00 am. M Tours guide will pick you up from your hotel whether in Cairo<br />\r\nor Giza to start the full day tour. It begins with visiting Coptic Cairo which is a part of Old<br />\r\nCairo that encompasses the Babylon Fortress, the Hanging Church, the Greek Church of St.<br />\r\nGeorge and many other Coptic churches and historical sites. It is believed in Christian<br />\r\ntradition that the Holy Family visited this area and stayed at the site of Saints Sergius and<br />\r\nBacchus Church (Abu Serga).&nbsp;<br />\r\nAlso, you will visit The Ben Ezra Synagogue which is according to local folklore, is located on<br />\r\nthe site where baby Moses was found.<br />\r\nThen the tour will be continued to Islamic Cairo that is a part of central Cairo around the<br />\r\nold walled city and around the Citadel of Cairo which is characterized by hundreds<br />\r\nof mosques, tombs,&nbsp; madrasas , mansions, caravanserais, and fortifications dating from<br />\r\nthe Islamic era.<br />\r\nYou will get to visit some of the historic, well-known mosques like:<br />\r\n- Mosque of Amr Ibn Al-As (Built by the Muslim Leader Amr Ibn Al-As at Islamic Cairo<br />\r\nCapital Al-Fustat)<br />\r\n- The Mosque-Madrassa of Sultan Hassan (a massive mosque and&nbsp; madrassa built<br />\r\nduring the&nbsp; Mamluk&nbsp; Islamic era)</p>\r\n\r\n<p>- Al-Rifa&#39;i Mosque (located opposite to the Mosque-Madrassa of Sultan Hassan)</p>\r\n\r\n<p>After that, you will visit The Saladin Citadel, a medieval Islamic fortification in Cairo which<br />\r\nwas once famous for its fresh breeze and grand views of the city. It contains the Mosque of<br />\r\nMuhammad Ali, built by Muhammad Ali Pashaon the summit of the citadel.<br />\r\nThen, the tour proceeds with Khan El-Kalili, which is the major Souk in the historic center of<br />\r\nIslamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike.<br />\r\nYou can shop from theresome memorable souvenirs as copper handicrafts, perfumes,<br />\r\nleather, silver and gold products, antiques and many other special products.<br />\r\nLunch is included during any of the tours at a Local Restaurant. (Full Meal for each person;<br />\r\nbeverages excluded)<br />\r\nThe Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>The Tour Includes:<br />\r\n- A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)<br />\r\n- Lunch at a Local Restaurant (beverages excluded)<br />\r\n- Pick-up and drop off<br />\r\n- Expert Tour guide</p>', 'en'),
	(71, 36, '<p>Eight-hour tour starts every day from 8 am to 4 pm. The tour includes pick-up and drop off<br />\r\ntransfers from customers&prime; location in Cairo, entry fees, expert tour guide, lunch at a local<br />\r\nrestaurant, all Taxes Services and a bag of snacks (A bottle of water, a can of soft drink, chips<br />\r\nand a cake)<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping and any Optional Activities.</strong></p>\r\n\r\n<p>CAIRO DAY TOUR TO CHRISTIAN &amp; ISLAMIC SIGHTS PROGRAM:</p>\r\n\r\n<p>The tour starts at 08:00 am. M Tours guide will pick you up from your hotel whether in Cairo<br />\r\nor Giza to start the full day tour. It begins with visiting Coptic Cairo which is a part of Old<br />\r\nCairo that encompasses the Babylon Fortress, the Hanging Church, the Greek Church of St.<br />\r\nGeorge and many other Coptic churches and historical sites. It is believed in Christian<br />\r\ntradition that the Holy Family visited this area and stayed at the site of Saints Sergius and<br />\r\nBacchus Church (Abu Serga).&nbsp;<br />\r\nAlso, you will visit The Ben Ezra Synagogue which is according to local folklore, is located on<br />\r\nthe site where baby Moses was found.<br />\r\nThen the tour will be continued to Islamic Cairo that is a part of central Cairo around the<br />\r\nold walled city and around the Citadel of Cairo which is characterized by hundreds<br />\r\nof mosques, tombs,&nbsp; madrasas , mansions, caravanserais, and fortifications dating from<br />\r\nthe Islamic era.<br />\r\nYou will get to visit some of the historic, well-known mosques like:<br />\r\n- Mosque of Amr Ibn Al-As (Built by the Muslim Leader Amr Ibn Al-As at Islamic Cairo<br />\r\nCapital Al-Fustat)<br />\r\n- The Mosque-Madrassa of Sultan Hassan (a massive mosque and&nbsp; madrassa built<br />\r\nduring the&nbsp; Mamluk&nbsp; Islamic era)</p>\r\n\r\n<p>- Al-Rifa&#39;i Mosque (located opposite to the Mosque-Madrassa of Sultan Hassan)</p>\r\n\r\n<p>After that, you will visit The Saladin Citadel, a medieval Islamic fortification in Cairo which<br />\r\nwas once famous for its fresh breeze and grand views of the city. It contains the Mosque of<br />\r\nMuhammad Ali, built by Muhammad Ali Pashaon the summit of the citadel.<br />\r\nThen, the tour proceeds with Khan El-Kalili, which is the major Souk in the historic center of<br />\r\nIslamic Cairo. This Bazaar is one of Cairo&#39;s main attractions for tourists and Egyptians alike.<br />\r\nYou can shop from theresome memorable souvenirs as copper handicrafts, perfumes,<br />\r\nleather, silver and gold products, antiques and many other special products.<br />\r\nLunch is included during any of the tours at a Local Restaurant. (Full Meal for each person;<br />\r\nbeverages excluded)<br />\r\nThe Day Tour ends with a drop off at the customer&#39;s hotel in Cairo/Giza</p>\r\n\r\n<p>The Tour Includes:<br />\r\n- A bag of snacks (A bottle of water, a can of soft drink, chips and a cake)<br />\r\n- Lunch at a Local Restaurant (beverages excluded)<br />\r\n- Pick-up and drop off<br />\r\n- Expert Tour guide</p>', 'sp'),
	(116, 40, 'Best Places To Visit in Summer', 'en'),
	(117, 40, 'Best Places To Visit in Summer', 'sp'),
	(118, 41, '<p>Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer Best Places To Visit in Summer</p>', 'en'),
	(119, 41, '<p>Best Places To Visit in Summer</p>', 'sp'),
	(120, 42, 'the best relax with us', 'en'),
	(121, 42, 'the best relax with us', 'sp'),
	(122, 43, 'the best relax with us', 'en'),
	(123, 43, 'the best relax with us', 'sp'),
	(134, 5, 'About MTours', 'ar'),
	(135, 6, 'We are very excited to introduce M TOURS as a provider for all your travel needs and requirements in Egypt. We label ourselves as innovative and highly efficient travel agency and we look forward to demonstrating these qualities to let you have an unforgettable and a unique experience in Egypt. Our primary aim is to provide quality travel services to our travelers and guests. If you would like a tour that is tailored exactly like the way you want, you found the right company. Send us a request and we would be happy to assist! Also, if this is your first time to Egypt and you want a designed-by-experts tour to make the most of your time, without all the thinking and planning, have a look at some our all-time popular and available itineraries! ***Please have a look at our FAQs page for more information on our service. Check out our Gallery page for some of the awesome sights you may get to see on our tours as well!', 'ar'),
	(136, 7, '<p>Terms &amp; Conditions</p>', 'ar'),
	(137, 46, 'Best Places To Visit in Summer', 'en'),
	(138, 46, 'Best Places To Visit in Summer', 'sp'),
	(139, 47, '<p>Best Places To Visit in Summer</p>', 'en'),
	(140, 47, '<p>Best Places To Visit in Summer</p>', 'sp'),
	(183, 65, 'Luxur From Cairo By Plan', 'en'),
	(184, 65, 'Luxur From Cairo By Plan', 'sp'),
	(185, 66, '<p>12-hour starts daily but it depends on flight availability. The tour includes pick-up and drop<br />\r\noff transfers from customers&prime; location in Cairo, Flight Tickets (Economy), expert tour guide,<br />\r\nentry fees, lunch at a local restaurant and all Taxes Services<br />\r\nIf there are no Economic seats available on your desired date, the Flight can be upgraded<br />\r\nto (Business Class). This is subject to extra fees as USD 90.00/Ticket.<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping, entry Visa to Egypt and any Optional Activities.</strong></p>\r\n\r\n<p><ins>ONE DAY TOUR TO LUXOR FROM CAIRO BY PLANE</ins><br />\r\nThe tour starts by 3:30 am. Our representative will pick you up from the hotel in Cairo/Giza<br />\r\nand you will be transfer to the domestic airport to take your flight to Luxor.<br />\r\nOnce arrived to Luxor, our expert tour guide will be waiting for you to begin your Full Day<br />\r\nTour. It will start with the West Bank of The Nile River. There you will visit The Valley of The<br />\r\nKings, Dayr al-Bahri, Colossi of Memnon (twin two massive stone statues of<br />\r\nthe Pharaoh&nbsp; Amenhotep III ) and The Mortuary Temple of Hatshepesut. Then, the tour<br />\r\nproceeds with a visit to Necropolis of Thebes.<br />\r\nLunch is included during any of the tours at a Local Restaurant. (Full Meal for each person;<br />\r\nbeverages excluded)<br />\r\nAfter Lunch, you will join your guide to continue your tour to The East Bank of Nile River.<br />\r\nThere you will visit The Karnak Temple and Luxor Temple.</p>\r\n\r\n<p>At the end, you will be transferred to a hotel in Luxor to enjoy a day-use and have some rest<br />\r\nbefore your flight from Luxor airport. Then by 10:00 pm, you will be transferred to the</p>\r\n\r\n<p>domestic airport to get your flight back to Cairo. After arrival, our representative will be<br />\r\nwaiting for you to drop you off to your hotel in Cairo/Giza.</p>', 'en'),
	(186, 66, '<p>12-hour starts daily but it depends on flight availability. The tour includes pick-up and drop<br />\r\noff transfers from customers&prime; location in Cairo, Flight Tickets (Economy), expert tour guide,<br />\r\nentry fees, lunch at a local restaurant and all Taxes Services<br />\r\nIf there are no Economic seats available on your desired date, the Flight can be upgraded<br />\r\nto (Business Class). This is subject to extra fees as USD 90.00/Ticket.<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping, entry Visa to Egypt and any Optional Activities.</strong></p>\r\n\r\n<p><ins>ONE DAY TOUR TO LUXOR FROM CAIRO BY PLANE</ins><br />\r\nThe tour starts by 3:30 am. Our representative will pick you up from the hotel in Cairo/Giza<br />\r\nand you will be transfer to the domestic airport to take your flight to Luxor.<br />\r\nOnce arrived to Luxor, our expert tour guide will be waiting for you to begin your Full Day<br />\r\nTour. It will start with the West Bank of The Nile River. There you will visit The Valley of The<br />\r\nKings, Dayr al-Bahri, Colossi of Memnon (twin two massive stone statues of<br />\r\nthe Pharaoh&nbsp; Amenhotep III ) and The Mortuary Temple of Hatshepesut. Then, the tour<br />\r\nproceeds with a visit to Necropolis of Thebes.<br />\r\nLunch is included during any of the tours at a Local Restaurant. (Full Meal for each person;<br />\r\nbeverages excluded)<br />\r\nAfter Lunch, you will join your guide to continue your tour to The East Bank of Nile River.<br />\r\nThere you will visit The Karnak Temple and Luxor Temple.</p>\r\n\r\n<p>At the end, you will be transferred to a hotel in Luxor to enjoy a day-use and have some rest<br />\r\nbefore your flight from Luxor airport. Then by 10:00 pm, you will be transferred to the</p>\r\n\r\n<p>domestic airport to get your flight back to Cairo. After arrival, our representative will be<br />\r\nwaiting for you to drop you off to your hotel in Cairo/Giza.</p>', 'sp'),
	(189, 68, 'RED SEA (AIN SOHKNA) FROM CAIRO', 'en'),
	(190, 68, 'RED SEA (AIN SOHKNA) FROM CAIRO', 'sp'),
	(191, 69, '<p>Ain Sokhna ten-hour tour starts every day from 7 am to 5 pm. The tour includes pick-up and<br />\r\ndrop off transfers from customers&prime; location in Cairo/Giza, expert tour guide, lunch, a day-use<br />\r\nat a hotel, changing room ( at ), all Taxes Services and a bag of snacks (A bottle of water, a<br />\r\ncan of soft drink, chips and a cake)<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping and any Optional Activities.</strong></p>\r\n\r\n<p><ins>DAY TOUR TO RED SEA FROM CAIRO PROGRAM</ins><br />\r\nThe tour starts at 7 am. Our representative will pick you up from the hotel and transfer you<br />\r\nto Ain Sokhna which is 134 KM away from Cairo. It&#39;s expected to arrive by 9 am to check in<br />\r\nthe hotel for a day-use and enjoy your day by the marvelous Red Sea. Lunch is included and<br />\r\nit can be served on beach, upon request.<br />\r\nCheck-out is done by 5 pm and then you will be transferred back to Cairo with the same<br />\r\ndriver and representative.</p>', 'en'),
	(192, 69, '<p>Ain Sokhna ten-hour tour starts every day from 7 am to 5 pm. The tour includes pick-up and<br />\r\ndrop off transfers from customers&prime; location in Cairo/Giza, expert tour guide, lunch, a day-use<br />\r\nat a hotel, changing room ( at ), all Taxes Services and a bag of snacks (A bottle of water, a<br />\r\ncan of soft drink, chips and a cake)<br />\r\nAll Transfers are by a private, air-conditioned, latest model vehicle.</p>\r\n\r\n<p><strong>The tour excludes personal items, tipping and any Optional Activities.</strong></p>\r\n\r\n<p><ins>DAY TOUR TO RED SEA FROM CAIRO PROGRAM</ins><br />\r\nThe tour starts at 7 am. Our representative will pick you up from the hotel and transfer you<br />\r\nto Ain Sokhna which is 134 KM away from Cairo. It&#39;s expected to arrive by 9 am to check in<br />\r\nthe hotel for a day-use and enjoy your day by the marvelous Red Sea. Lunch is included and<br />\r\nit can be served on beach, upon request.<br />\r\nCheck-out is done by 5 pm and then you will be transferred back to Cairo with the same<br />\r\ndriver and representative.</p>', 'sp'),
	(197, 71, 'Best Places To Visit in Summer', 'en'),
	(198, 71, 'Best Places To Visit in Summer', 'sp'),
	(199, 72, '<p>Best Places To Visit in Summer</p>', 'en'),
	(200, 72, '<p>Best Places To Visit in Summer</p>', 'sp'),
	(201, 73, 'Best Places To Visit in Summer', 'en'),
	(202, 73, 'Best Places To Visit in Summer', 'sp'),
	(203, 74, '<p>Best Places To Visit in Summer</p>', 'en'),
	(204, 74, '<p>Best Places To Visit in Summer</p>', 'sp'),
	(209, 76, 'ww', 'en'),
	(210, 76, 'ww', 'sp'),
	(211, 77, '<p>ww</p>', 'en'),
	(212, 77, '<p>ww</p>', 'sp'),
	(217, 79, 'Visiting King Tutankhamun Tomb at The Valley of The Kings', 'en'),
	(218, 79, 'Visiting King Tutankhamun Tomb at The Valley of The Kings', 'sp'),
	(219, 80, 'Coptic Museum', 'en'),
	(220, 80, 'Coptic Museum', 'sp'),
	(221, 81, 'Islamic Museum', 'en'),
	(222, 81, 'Islamic Museum', 'sp'),
	(223, 82, 'A Felucca boat ride on the Nile', 'en'),
	(224, 82, 'A Felucca boat ride on the Nile', 'sp'),
	(225, 83, 'Getting inside any of the Pyramids', 'en'),
	(226, 83, 'Getting inside any of the Pyramids', 'sp'),
	(227, 84, 'Visiting the Solar Boat', 'en'),
	(228, 84, 'Visiting the Solar Boat', 'sp'),
	(229, 85, 'One-hour camel ride', 'en'),
	(230, 85, 'One-hour camel ride', 'sp'),
	(231, 86, 'Half-hour camel ride', 'en'),
	(232, 86, 'Half-hour camel ride', 'sp'),
	(233, 87, 'Quad Bike at the Wide Desert', 'en'),
	(234, 87, 'Quad Bike at the Wide Desert', 'sp'),
	(235, 88, 'A Felucca boat ride on the Nile', 'en'),
	(236, 88, 'A Felucca boat ride on the Nile', 'sp'),
	(237, 89, 'Getting inside any of the Pyramids', 'en'),
	(238, 89, 'Getting inside any of the Pyramids', 'sp'),
	(239, 90, 'Visiting the Solar Boat', 'en'),
	(240, 90, 'Visiting the Solar Boat', 'sp'),
	(241, 91, 'One-hour camel ride', 'en'),
	(242, 91, 'One-hour camel ride', 'sp'),
	(243, 92, 'Half-hour camel ride', 'en'),
	(244, 92, 'Half-hour camel ride', 'sp'),
	(245, 93, 'Quad Bike at the Wide Desert', 'en'),
	(246, 93, 'Quad Bike at the Wide Desert', 'sp'),
	(247, 94, 'A Felucca boat ride on the Nile', 'en'),
	(248, 94, 'A Felucca boat ride on the Nile', 'sp'),
	(249, 95, 'Mummies Room at The Egyptian Museum', 'en'),
	(250, 95, 'Mummies Room at The Egyptian Museum', 'sp'),
	(251, 96, 'Photography Fees at The Egyptian Museum – (Photography is prohibited inside Tutankhamon\'s room)', 'en'),
	(252, 96, 'Photography Fees at The Egyptian Museum – (Photography is prohibited inside Tutankhamon\'s room)', 'sp'),
	(253, 97, 'From Cairo Airport to Alexanderia', 'en'),
	(254, 97, 'From Cairo Airport to Alexanderia', 'sp'),
	(255, 98, '<p>Cairo Airport to Alexanderia</p>', 'en'),
	(256, 98, '<p>Cairo Airport to Alexanderia</p>', 'sp'),
	(257, 99, 'From Cairo Airport to Cairo', 'en'),
	(258, 99, 'From Cairo Airport to Cairo', 'sp'),
	(259, 100, '<p>From Cairo Airport to Cairo</p>', 'en'),
	(260, 100, '<p>From Cairo Airport to Cairo</p>', 'sp'),
	(266, 101, 'Terms & Conditions', 'en'),
	(267, 101, 'Terms & Conditions', 'sp'),
	(268, 102, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'en'),
	(269, 102, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'sp'),
	(272, 103, 'Jet-skiing for 1 Hour', 'en'),
	(273, 103, 'Jet-skiing for 1 Hour', 'sp');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Dumping structure for table mtours.transportations
CREATE TABLE IF NOT EXISTS `transportations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.transportations: ~0 rows (approximately)
/*!40000 ALTER TABLE `transportations` DISABLE KEYS */;
INSERT INTO `transportations` (`id`, `destination`, `description`, `price`, `created_at`, `updated_at`) VALUES
	(1, '97', '98', 120.00, '2019-06-28 01:07:43', '2019-06-28 01:07:43'),
	(2, '99', '100', 175.00, '2019-06-28 02:24:18', '2019-06-28 02:24:18');
/*!40000 ALTER TABLE `transportations` ENABLE KEYS */;

-- Dumping structure for table mtours.transportation_prices
CREATE TABLE IF NOT EXISTS `transportation_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transportation_id` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.transportation_prices: ~2 rows (approximately)
/*!40000 ALTER TABLE `transportation_prices` DISABLE KEYS */;
INSERT INTO `transportation_prices` (`id`, `transportation_id`, `from`, `to`, `price`, `created_at`, `updated_at`) VALUES
	(5, 1, 3, 6, 100.00, NULL, NULL),
	(6, 1, 7, 10, 80.00, NULL, NULL);
/*!40000 ALTER TABLE `transportation_prices` ENABLE KEYS */;

-- Dumping structure for table mtours.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('admin','client') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'client',
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `security_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mtours.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `phone`, `security_code`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'senjider@gmail.com', '$2y$10$2wfwYmyDokmfGsC7lmTtjODmysMqZrN5lC5NgF28II70b9X1trBsW', 'admin', NULL, NULL, 1, 'mtNSsew79AoAObT2QI8MjSYkqGWbZ6SV94rZN8bn7Gn4oziMCTnhLx8tbtv1', NULL, NULL),
	(3, 'ali', 'senjider1@gmail.com', '$2y$10$1PSHPhaGmACyS70OLVcSouhr2qxFUr8VX9C7xTsHYMm/jpyO9pNVq', 'client', '1066336172', '2394', 1, 'YdpoQZ6ZRp9uKtd6GshuypkGKReP4sZNGqiwsvs8FZojUpM4kq47kjAM6LdB', '2019-05-31 22:10:49', '2019-05-31 22:10:49'),
	(4, 'ali', 'senjider3@gmail.com', '$2y$10$cSiNRwKqDaqTquZGVDpZKel4QRqKE0JFPsWgvPyUkwE.VvIwYrDua', 'client', '1066336172', '4971', 0, NULL, '2019-05-31 22:17:55', '2019-05-31 22:17:55'),
	(7, 'ali h', 'senjider@gmail.com', '$2y$10$SQUKf2epVLaszeuhV7mhGOont4zWgmfXCOrNlETEU3mTnPCKguTVm', 'client', '1066336172', '9201', 1, '670drApdPWq72fq2wuS01RrBBWTJzy9DfYLCzMWNABoFP6eu8o2MtX5JJwlz', '2019-05-31 23:24:58', '2019-05-31 23:41:40');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
