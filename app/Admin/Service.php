<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title','description'];
    
     protected $fillable = ['title', 'description','home_image','banner_image','page_image'];
}
