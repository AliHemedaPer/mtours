@extends('front.front')

@section('content')
<div class="main-cont" style="background-color: #fff">   
  
  <div class="inner-page">
    <div class="inner-breadcrumbs" style="margin-bottom: 30px;">
      <div class="content-wrapper">
        <div class="page-title">@lang('fnt.transportations')</div>
        <div class="breadcrumbs">
              <!-- <a href="#">Home</a> / <span>Blog Masonry</span> -->
            </div>
            <div class="clear"></div>
          </div>    
    </div>
    
     <div class="typography">
    <div class="content-wrapper">
      <div class="shortcodes">
          <!-- // accordeon // -->
            <div class="accordeon">
              @foreach($transportations as $transportation)
              <!-- // -->
                <div class="accordeon-item {{ $loop->index == 0 ? 'open' : '' }}">
                  <div class="accordeon-a">
                    <i class="accordeon-icon"></i>
                    <span>{{ $transportation->translate('en')->destination }}</span>
                    <div class="clear"></div>
                  </div>
                  <div class="accordeon-b">
                    <div class="blockqoute-tp-a float-right">
                      <b><strong>@lang('fnt.prices')</strong></b>
                      <nav class="marked-c">
                        <ul>
                          @if(count($transportation->transportationPrices))
                            @foreach($transportation->transportationPrices as $price)
                            <li>@lang('fnt.from') {{ $price->from }} @lang('fnt.to') {{ $price->to }} @lang('fnt.persons'): <strong>({{ $price->price }} {{ $app_settings['app_currency'] }}/@lang('fnt.person'))</strong></li>
                            @endforeach
                          @endif
                          <li>@lang('fnt.s_person'): <strong>({{ $transportation->price }} {{ $app_settings['app_currency'] }})</strong></li>
                        </ul>
                      </nav>
                      <div style="width: 200px">
                      <a href="{{ url('transportation-booking/'.$transportation->id) }}" class="book-btn">
                        <span class="book-btn-l"><i></i></span>
                        <span class="book-btn-r" style="float: none;padding-top: 9px;width: 190px;    font-size: 12px;color: #fff;">@lang('fnt.request')</span>
                        <div class="clear"></div>
                      </a>
                    </div>
                    </div>
                    <p>
                      {!! $transportation->translate('en')->description !!}
                    </p>
                    <div style="clear: both;"></div>
                    
                  </div>
                </div>
              <!-- \\ -->
              @endforeach
            </div>
          <!-- \\ accordeon \\ -->
        </div>
              <div class="clear"></div>

        @if( $transportations_count > $pg_count)
          {{ $transportations->appends(request()->query())->links('front.pagination') }}
        @endif
      </div>

     </div>
  </div>
</div>
@stop