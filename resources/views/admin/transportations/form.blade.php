@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li>
        <a href="{{url('admin/transportations')}}">
            Airport Transportations
        </a>
    </li>
    <li class="active">
        @if($transportation)
        Edit
        @else 
        Add
        @endif
    </li>
  
</ol>
<div class="page-header">
    <h1 class="col-md-6">Airport Transportation</h1>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="{{url("admin/transportations")}}"><i class="icon-plus2 mr-2"></i> Back <i class="fa fa-reply"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            
        <div class="panel-body">
        @if($transportation)
        {!! Form::model($transportation,["url"=>"admin/transportations/$transportation->id","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
        @include('admin.transportations.input',['buttonAction'=>'Save'])
        @else 
        {!! Form::open(["url"=>"admin/transportations","class"=>"form-horizontal","method"=>"POST","enctype"=>"multipart/form-data"]) !!}
        @include('admin.transportations.input',['buttonAction'=>'Save'])
        @endif

        {!! Form::close() !!}

    </div>
</div>
</div>
</div>


@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>

<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({
            allowClear: true
        });
        FormElements.init();
    });

    function addPrice(){
        let prc_count = $("#prices_count").val();
        let newHtml = '<div class="col-lg-1" style="padding-top: 25px;"># '+ (parseInt(prc_count) + 1) +'</div><div class="col-lg-4"><div class="form-group"><label for="form-field-select-3">Persons Num From</label><input class="form-control" placeholder="" name="cut_price['+prc_count+'][from]" type="number" value=""></div></div>';

        newHtml += '<div class="col-lg-4"><div class="form-group"><label for="form-field-select-3">Persons Num To</label><input class="form-control" placeholder="" name="cut_price['+prc_count+'][to]" type="number" value=""></div></div>';

        newHtml += '<div class="col-lg-3"><div class="form-group"><label for="form-field-select-3">Price</label><input class="form-control" placeholder="" name="cut_price['+prc_count+'][price]" type="text" value=""></div></div><div style="clear: both;"></div>';

        $("#custome_prices").append(newHtml);
        $("#prices_count").val(parseInt(prc_count) + 1);       
    }

</script>
@endsection