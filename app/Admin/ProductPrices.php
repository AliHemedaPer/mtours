<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductPrices extends Model
{
    protected $fillable = ['product_id', 'from', 'to', 'price'];

    public function products()
    {
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }
}
