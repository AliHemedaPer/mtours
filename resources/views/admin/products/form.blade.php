@extends('admin.layouts.admin_master')

        
@section('style')
<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
<link rel="stylesheet" href="{{url('public/admin/')}}/assets/plugins/dropzone/downloads/css/dropzone.css">

<style type="text/css">
    .imgBox{
        position: relative;
        padding: 0;
        border: 1px solid #ccc;
        border-radius: 4px;
        margin-bottom: 20px;
        margin-right: 15px
    }

    .imgBox img{
        border-radius: 4px;
    }

    .imgRemove{
        display: block;
        position: absolute;
        width: 40px;
        height: 40px;
        top: 0;
        right: 0;
        background-image: url({{url('/')}}/public/admin/assets/plugins/dropzone/downloads/images/spritemap.png);
        background-position: -268px -123px;
    }

    .imgRemove span{
        display: none;
    }
</style>
@endsection

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li>
        <a href="{{url('admin/products')}}">
            Tours
        </a>
    </li>
    <li class="active">
        @if($product)
        Edit
        @else 
        Add
        @endif
    </li>
  
</ol>
<div class="page-header">
    <h1 class="col-md-6">Tour</h1>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="{{url("admin/products")}}{{ $cat ? '?cat='.$cat : '' }}{{ $parent ? '?parent='.$parent : '' }}"><i class="icon-plus2 mr-2"></i> Back <i class="fa fa-reply"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">
            
        <div class="panel-body">
        @if($product)
        {!! Form::model($product,["url"=>"admin/products/$product->id","class"=>"form-horizontal","method"=>"patch","enctype"=>"multipart/form-data"]) !!}
        @include('admin.products.input',['buttonAction'=>'Save'])
        @else 
        {!! Form::open(["url"=>"admin/products","class"=>"form-horizontal","method"=>"POST","enctype"=>"multipart/form-data"]) !!}
        @include('admin.products.input',['buttonAction'=>'Save'])
        @endif

        {!! Form::close() !!}

    </div>
</div>
</div>
</div>

@if($product)
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: DROPZONE PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-picture-o"></i>
                Tour Images
            </div>
            <div class="panel-body">
                <div>
                    @if( $product->productImages->count() > 0 )
                        @foreach($product->productImages as $img)
                            <div class="col-sm-3 imgBox" id="img-{{ $img->id }}">
                                <img width="100%" src="{{url($img->image)}}" alt="">
                                <a title="Delete" class="imgRemove" href="javascript://" onclick="removeImage('{{ $img->id }}','{{ $product->id }}')"><span>✘</span></a>
                            </div>
                            @if(  $loop->index > 0 &&  ($loop->index +1 ) % 3 == 0)
                                <div style="clear: both;"></div>
                            @endif
                        @endforeach
                        <div style="clear: both;"></div>
                    @else
                        <div class="alert alert-warning">
                            No Images!
                        </div>
                    @endif
                </div>
                <form action="{{ url('admin/file-upload') }}" class="dropzone" id="my-awesome-dropzone">
                    <input type="hidden" name="pid" value="{{ $product->id }}"/>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
        <!-- end: DROPZONE PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->
@endif

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{url('public/admin/')}}/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-elements.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{url('public/admin/')}}/assets/plugins/dropzone/downloads/dropzone.min.js"></script>
<script src="{{url('public/admin/')}}/assets/js/form-dropzone.js"></script>
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({
            allowClear: true
        });
        FormElements.init();
        Dropzone.init();
    });

    function addPrice(){
        let prc_count = $("#prices_count").val();
        let newHtml = '<div class="col-lg-1" style="padding-top: 25px;"># '+ (parseInt(prc_count) + 1) +'</div><div class="col-lg-4"><div class="form-group"><label for="form-field-select-3">Persons Num From</label><input class="form-control" placeholder="" name="cut_price['+prc_count+'][from]" type="number" value=""></div></div>';

        newHtml += '<div class="col-lg-4"><div class="form-group"><label for="form-field-select-3">Persons Num To</label><input class="form-control" placeholder="" name="cut_price['+prc_count+'][to]" type="number" value=""></div></div>';

        newHtml += '<div class="col-lg-3"><div class="form-group"><label for="form-field-select-3">Price</label><input class="form-control" placeholder="" name="cut_price['+prc_count+'][price]" type="text" value=""></div></div><div style="clear: both;"></div>';

        $("#custome_prices").append(newHtml);
        $("#prices_count").val(parseInt(prc_count) + 1);       
    }

    function addActivity(){
        let activities_count = $("#activities_count").val();
        let newHtml = '<div class="col-lg-1" style="padding-top: 25px;"># '+ (parseInt(activities_count) + 1) +'</div><div class="col-lg-5"><div class="form-group"><label for="form-field-select-3">Title (EN)</label><input class="form-control" placeholder="" name="opt_act['+activities_count+'][title][en]" type="text" value=""></div></div>';

        newHtml += '<div class="col-lg-5"><div class="form-group"><label for="form-field-select-3">Title (SP)</label><input class="form-control" placeholder="" name="opt_act['+activities_count+'][title][sp]" type="text" value=""></div></div>';

        newHtml += '<div class="col-lg-1"><div class="form-group"><label for="form-field-select-3">Price</label><input class="form-control" placeholder="" name="opt_act['+activities_count+'][price]" type="text" value=""></div></div><div style="clear: both;"></div>';

        $("#opt_activities").append(newHtml);
        $("#activities_count").val(parseInt(activities_count) + 1);       
    }

    function removeImage(imgid, pid){
        var r = confirm("Are you sure!");
        if (r == true) {
          $.ajax({
            type: "DELETE",
            url: "{{url('admin/file-delete' )}}",
            data: {'imgid' : imgid, 'pid' : pid},
            datatype: 'json',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function (data) {
                if(data.status == 'success')
                    $('#img-'+imgid).remove();
                else
                    alert('Failed to delete image');
            }
          });
        }
    }
</script>
@endsection