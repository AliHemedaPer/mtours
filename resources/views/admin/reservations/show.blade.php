@extends('admin.layouts.admin_master')

@section('style')
    <style type="text/css">
        .field{
            color: #4e80b5;
        }
    </style>
@endsection

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li>
        <i class="clip-list"></i>
        <a href="{{url('admin/reservations')}}">
            Reservations
        </a>
    </li>
    <li class="active">
        Details
    </li>

</ol>
<div class="page-header">
    <h1 class="col-md-6">Reservation Details: <span style="color: #007aff;">{{ $reservation->total_price }} $</span></h1>
    <div class="col-md-4 pull-right">
         @if($reservation->booking_type == 'tour' )  
            <label>Availability: </label>
                @if($reservation->products->check_avail)  
                    <select class="form-control" style="padding: 0 12px" onchange="setAvail('{{ $reservation->id }}', $(this))" {!! $reservation->payment_status ? 'disabled=""' : '' !!}>
                        <option value="">-- Select --</option>
                        <option value="1" @if($reservation->is_available) selected="" @endif>Available</option>
                        <option value="0" @if($reservation->is_available === 0) selected="" @endif>Not Available</option>
                    </select>
                @endif
        @endif 
    </div>
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body" style="position: relative;">
                <div id="loading" style="position: absolute; top: 0; left: 0; z-index: 111; background-color: #8196ad;width: 100%; height: 100%;  opacity: 0.6; display: none;">
                    <div style="padding-top: 30px;
    text-align: center;
    color: #fff;
    font-size: 16px;
    font-weight: bold;">Please Wait....</div>
                </div>
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                    <tbody>
                        <tr>                  
                            <th style="width: 200px" class="field">
                                {{ $btype == 'tours' ? 'Tour' : 'Airport Transportation' }}
                            </th>
                            <th >
                                @if($reservation->booking_type == 'tour')
                                <a href="{{url('tour/'.$reservation->product_id)}}" class="tooltips" data-placement="top" data-original-title="View main Tour" target="_blank">{{$reservation->products->translate('en')->title }}</a>
                                @else
                                {{$reservation->transportations->translate('en')->destination }}
                                @endif
                            </th>                     
                        </tr>
                        <tr> 
                            <th style="width: 200px" class="field">Adults</th>
                            <th >{{ $reservation->adults }}</th>                     
                        </tr> 
                        @if($reservation->booking_type == 'tour')
                        <tr> 
                            <th style="width: 200px" class="field">Childs</th>
                            <th >{{ $reservation->childs }}</th>                     
                        </tr>
                        @endif
                        <tr> 
                            <th style="width: 200px" class="field">
                                {{ ($btype == 'tours') ? 'Date/From' : 'Date' }}
                            </th>
                            <th >{{ Carbon\Carbon::parse($reservation->date_from)->format('d M Y') }} </th>                     
                        </tr>
                        @if($reservation->booking_type == 'tour' && $reservation->date_to)
                        <tr> 
                            <th style="width: 200px" class="field">Date To</th>
                            <th >{{ Carbon\Carbon::parse($reservation->date_to)->format('d M Y') }}</th>                     
                        </tr>
                        @endif
                        <tr> 
                            <th style="width: 200px" class="field">Guide Language</th>
                            <th >{{ $reservation->guide }}</th>                     
                        </tr> 
                        <tr> 
                            <th style="width: 200px" class="field">Name</th>
                            <th >{{ $reservation->name }}</th>                     
                        </tr>
                        <tr> 
                            <th style="width: 200px" class="field">Email</th>
                            <th >{{ $reservation->email }}</th>                     
                        </tr>
                        <tr> 
                            <th style="width: 200px" class="field">Phone</th>
                            <th >{{ $reservation->phone }}</th>                     
                        </tr> 
                        <tr> 
                            <th style="width: 200px" class="field">Nationality</th>
                            <th >{{ $reservation->nationality }}</th>                     
                        </tr> 
                        <tr> 
                            <th style="width: 200px" class="field">Passport ID</th>
                            <th >{{ $reservation->passport }}</th>                     
                        </tr> 
                        
                        @if($reservation->booking_type == 'tour')
                        <tr> 
                            <th style="width: 200px" class="field">Optional Activities</th>
                            <th >
                                @if( count($reservation->orderExtras) )
                                <ul>
                                    @foreach($reservation->orderExtras as $extra)
                                        <li>{{ $extra->productExtras->translate('en')->title ?? '' }}</li>
                                    @endforeach
                                </ul>
                                @else
                                - 
                                @endif  
                            </th>                     
                        </tr>
                        @endif
                        
                        <tr> 
                            <th class="field">Extra Note</th>
                            <th >{{ $reservation->note }}</th>                     
                        </tr> 
                        
                        <tr> 
                            <th class="field">Hotel Name</th>
                            <th >{{ $reservation->hotel ?? '-' }}</th>                     
                        </tr> 
                        
                        <tr> 
                            <th  class="field">Payment Type</th>
                            <th  style="color: #fd7514;">{{ $reservation->payment_type == 'cash' ? 'Pay Cash on Arrival' : 'Visa/Mastercard' }}</th>                     
                        </tr> 
                        <tr> 
                            <th  class="field" style="width: 200px">Payment Status</th>
                            <th >{!! $reservation->payment_status ? $reservation->payment_status == 2 ? '<span style="color:#ee8a1f">Pending</span>' : '<span style="color:#2bb78b">Success</span>' : '<span style="color:#ff1010">Failed</span>' !!}</th>                     
                        </tr> 

                        <tr style="color: #2bb78b;"> 
                            <th style="width: 200px">Total</th>
                            <th >{{ number_format($reservation->total_price,2) }}$</th>                     
                        </tr> 

                        <tr> 
                            <th style="width: 200px" class="field">Reservation Date</th>
                            <th >{{ Carbon\Carbon::parse($reservation->created_at)->format('d M Y g:i:s A') }}</th>                     
                        </tr> 
                        
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<!-- <script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script> -->
<!-- <script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script> -->
<script src="{{url('public/admin/')}}/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
jQuery(document).ready(function () {
    TableData.init();
});

function setAvail(id, slct){
    $("#loading").fadeIn();
    var slctVal = slct.val();

    $.ajax({
        type: "POST",
        url: "{{url('admin/updAvailability' )}}"+"/"+id,
        data: {'avail' : slctVal, 'id' : id},
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (data) {
            $("#loading").fadeOut();
        }
    });    
}
</script>
@endsection