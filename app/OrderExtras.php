<?php

namespace App;
use App\Admin\ProductExtras;

use Illuminate\Database\Eloquent\Model;

class OrderExtras extends Model
{
    protected $fillable = ['order_id', 'product_extras_id', 'adults', 'childs', 'total_price'];
    public function orders()
    {
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }

    public function productExtras()
    {
        return $this->belongsTo(ProductExtras::class, 'product_extras_id', 'id');
    }
}
