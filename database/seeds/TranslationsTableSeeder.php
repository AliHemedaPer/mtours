<?php

use Illuminate\Database\Seeder;

class TranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('translations')->delete();
        
        \DB::table('translations')->insert(array (
            0 => 
            array (
                'id' => 4,
                'group_id' => 3,
                'value' => 'Permanent design service',
                'locale' => 'en',
            ),
            1 => 
            array (
                'id' => 5,
                'group_id' => 3,
                'value' => 'Permanent design service',
                'locale' => 'sp',
            ),
            2 => 
            array (
                'id' => 6,
                'group_id' => 4,
                'value' => 'Sign up now with our Instant Design service and save your monthly expenses...',
                'locale' => 'en',
            ),
            3 => 
            array (
                'id' => 7,
                'group_id' => 4,
                'value' => 'Sign up now with our Instant Design service and save your monthly expenses...',
                'locale' => 'sp',
            ),
            4 => 
            array (
                'id' => 8,
                'group_id' => 5,
                'value' => 'MTours',
                'locale' => 'en',
            ),
            5 => 
            array (
                'id' => 9,
                'group_id' => 5,
                'value' => 'MTours',
                'locale' => 'sp',
            ),
            6 => 
            array (
                'id' => 10,
                'group_id' => 6,
                'value' => 'We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .',
                'locale' => 'en',
            ),
            7 => 
            array (
                'id' => 11,
                'group_id' => 6,
                'value' => 'We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .',
                'locale' => 'sp',
            ),
            8 => 
            array (
                'id' => 12,
                'group_id' => 7,
                'value' => '<p>We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .</p>',
                'locale' => 'en',
            ),
            9 => 
            array (
                'id' => 13,
                'group_id' => 7,
                'value' => '<p>We believe we can change cultures and make the world a better place. This objective is to be attained by producing quality products through which values are conveyed to and influencing the society .</p>',
                'locale' => 'sp',
            ),
            10 => 
            array (
                'id' => 14,
                'group_id' => 8,
                'value' => 'Marketing',
                'locale' => 'en',
            ),
            11 => 
            array (
                'id' => 15,
                'group_id' => 8,
                'value' => 'Marketing',
                'locale' => 'sp',
            ),
            12 => 
            array (
                'id' => 16,
                'group_id' => 9,
                'value' => 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.',
                'locale' => 'en',
            ),
            13 => 
            array (
                'id' => 17,
                'group_id' => 9,
                'value' => 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.',
                'locale' => 'sp',
            ),
            14 => 
            array (
                'id' => 18,
                'group_id' => 10,
                'value' => 'Marketing',
                'locale' => 'en',
            ),
            15 => 
            array (
                'id' => 19,
                'group_id' => 10,
                'value' => 'Marketing',
                'locale' => 'sp',
            ),
            16 => 
            array (
                'id' => 20,
                'group_id' => 11,
                'value' => 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.',
                'locale' => 'en',
            ),
            17 => 
            array (
                'id' => 21,
                'group_id' => 11,
                'value' => 'The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients. The aim of our work, in the first place, is about building strong, trustworthy relationships with our clients.',
                'locale' => 'sp',
            ),
        ));
        
        
    }
}