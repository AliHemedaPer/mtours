@extends('front.front')

@section('style')
<style type="text/css">
  .req{
    position: absolute;
    color: #ff0b0b;
    top: -18px;
    right: 0px;
    font-size: 13px;
    display: none;
  }
</style>
@endsection

@section('content')
<!-- main-cont -->
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
        <div class="page-head">
          <div class="page-title">@lang('fnt.tours') - <span>@lang('fnt.booking')</span></div>
          <div class="breadcrumbs">
            <!-- <a href="#">Home</a> / <a href="#">Tours</a> / <span>booking</span> -->
          </div>
          <div class="clear"></div>
        </div>

         @if (Request::session()->has('error')) 
          <div class="message-box-c">{{Request::session()->get('error')}}</div>
         @endif

      <form id="booking_form" method="POST" action="{{ url('/bookingaction/').'/'.$tour->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="booking_type" value="tour"> 
        <div class="sp-page">
        

        <div class="sp-page-r sp-page-r-book">
          
          <div class="checkout-coll">
            <div class="checkout-head">
              <div class="checkout-headl">
                <a href="{{ url('tour/'.$tour->id) }}"><img alt="" src="../{{ $tour->image }}" style="max-width: 100px"></a>
              </div>
              <div class="checkout-headr">
                  <div class="checkout-headrb">
                    <div class="checkout-headrp">
                      <div class="chk-left">
                        <div class="chk-lbl"><a href="{{ url('tour/'.$tour->id) }}">{{ $tour->translate($share_locale)->title }}</a></div>
                        <div class="chk-lbl-a"></div>
                        <!-- <nav class="chk-stars">
                          <ul>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-b.png"></li>
                            <li><img alt="" src="{{ asset('public/front/img/') }}/chk-star-a.png"></li>
                          </ul>
                          <div class="clear"></div>
                        </nav> -->
                        
                      </div>
                      <div class="chk-right">
                        <!-- <a href="#"><img alt="" src="{{ asset('public/front/img') }}/chk-edit.png"></a> -->
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="clear"></div>
              </div>
            </div>

            <div class="chk-lines">
              <div class="chk-line" style="font-size: 13px;    line-height: 20px;">
                @lang('fnt.pr_qutd') <span style="color: #f91818;font-weight: bold;">@lang('fnt.pr_prsn')</span> @lang('fnt.in') {{ $app_settings['app_currency'] }}:
                <br/>
                <br/>

                @foreach($tour->productPrices as $prod_price)
                <span style="color: #4687bb;">- {{ $prod_price->from }} @lang('fnt.to') {{ $prod_price->to }} @lang('fnt.persons'):</span>
                <span class="chk-dates"><strong>{{ number_format($prod_price->price, 2, '.', '') }} {{ $app_settings['app_currency'] }}</strong></span>
                <br/>
                @endforeach
                <span style="color: #4687bb;">- @lang('fnt.s_person'):</span>
                <span class="chk-dates"><strong>{{ number_format($tour->price, 2, '.', '') }} {{ $app_settings['app_currency'] }}</strong></span>
                <br/>

                @if($tour->child_from && $tour->child_to && $tour->child_price)
                <br/>
                <span style="color: #4687bb;">- @lang('fnt.child') (@lang('fnt.from') {{ $tour->child_from }} @lang('fnt.to') {{ $tour->child_to }} @lang('fnt.old')):</span>
                <span class="chk-dates"><strong>{{ number_format($tour->child_price, 2, '.', '') }} {{ $app_settings['app_currency'] }}/@lang('fnt.child')</strong></span>
                <br/>
                @endif

               
              </div>
              <div class="chk-line">
                @lang('fnt.adults'): <span class="chk-persons"><span id="total_adults_num">1</span> @lang('fnt.persons')</span>
              </div>
              <div class="chk-line">
                @lang('fnt.child'): <span class="chk-persons"><span id="total_childs_num">0</span></span>
              </div>
            </div>
            
            <div class="chk-details">
              <h2>@lang('fnt.details')</h2>
              <div class="chk-detais-row">
                <div class="chk-line">
                  <span class="chk-l">@lang('fnt.price'):</span>
                  <span class="chk-r"><span id="total_presons_price">{{ number_format($tour->price, 2, '.', '') }}</span> {{ $app_settings['app_currency'] }}</span>
                  <div class="clear"></div>
                </div>
                <div class="chk-line">
                  <span class="chk-l"></span>
                  <span class="chk-r"></span>
                  <div class="clear"></div>
                </div>
                
                @if($tour->productExtras->count())
                    <h2>@lang('fnt.opts')</h2>
                
                    @foreach($tour->productExtras as $extra)
                    <div class="chk-line">
                      <span class="chk-l">
                        <input type="checkbox" class="opts_inputs" name="tour_opts[]" value="{{ $extra->id }}" data-price="{{ $extra->price ?? 0 }}" onclick="opts_calc()" /> {{ $extra->translate($share_locale)->title }}</span>
                      <span class="chk-r">{{ $extra->price ? number_format($extra->price, 2, '.', '').' '.$app_settings['app_currency'].'/'.__('fnt.person') : __('fnt.prc_at_cost') }}</span>
                      <div class="clear"></div>
                    </div>
                    @endforeach
                @endif  
                <input type="hidden" id="total_options_price" value="0">            
                

              </div>
              <div class="chk-total">
                <div class="chk-total-l">@lang('fnt.total')</div>
                <div class="chk-total-r" ><span id="total">{{ number_format($tour->price, 2, '.', '') }}</span>$ + <span style="font-size: 12px">({{ $app_settings['app_fees'] }}% @lang('fnt.fees'))</span>: <span id="total_w_fees">{{ number_format( ( ( $app_settings['app_fees'] /100) * ($tour->price) + $tour->price ), 2, '.', '') }}</span> {{ $app_settings['app_currency'] }}</div>
                <div class="clear"></div>
              </div>          
            </div>
            
          </div>
        </div>

        
        <div class="sp-page-l sp-page-l-book">
            <div class="sp-page-lb">
              <div class="sp-page-p">
              {{-- @if (auth()->check() && auth()->user()->isClient()) --}}
              <div class="booking-left"> 

                {{-- <!-- <div class="complete-info">
                  <h2>Your Personal Information</h2>
                  <div class="complete-info-table">
                    <div class="complete-info-i">
                      <div class="complete-info-l">ID:</div>
                      <div class="complete-info-r">{{ auth()->user()->id }}</div>
                      <div class="clear"></div>
                    </div>
                    <div class="complete-info-i">
                      <div class="complete-info-l">Name:</div>
                      <div class="complete-info-r">{{ auth()->user()->name }}</div>
                      <div class="clear"></div>
                    </div>
                    <div class="complete-info-i">
                      <div class="complete-info-l">E-Mail Adress:</div>
                      <div class="complete-info-r">{{ auth()->user()->email }}</div>
                      <div class="clear"></div>
                    </div>
                    <div class="complete-info-i">
                      <div class="complete-info-l">Phone Number:</div>
                      <div class="complete-info-r">{{ auth()->user()->phone }}</div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  
                  <div class="complete-devider"></div>
                                
                </div> -->--}}

                <h2>@lang('fnt.tour_info')</h2>
                
                <div class="booking-form">
                  <div class="booking-form-i">
                    <label>@lang('fnt.f_nm'):</label>
                    <div class="input-a" style="position: relative;">
                      <input type="text" value="{{ $tourCashedData['first_name'] ?? '' }}" class="text-inpt" id="tour_first_name" name="first_name">
                      <div class="req" id="tour_first_name_req">@lang('fnt.req')</div>
                    </div>
                  </div>
                  <div class="booking-form-i">
                    <label>@lang('fnt.l_nm'):</label>
                    <div class="input-a" style="position: relative;">
                      <input type="text" value="{{ $tourCashedData['last_name'] ?? '' }}" class="text-inpt" id="tour_last_name" name="last_name">
                      <div class="req" id="tour_last_name_req">@lang('fnt.req')</div>
                    </div>
                  </div>
                  <div class="clear"></div>
                  
                  <div class="booking-form-i">
                    <label>@lang('fnt.phone'):</label>
                    <div class="input-a">
                      <input type="text" value="{{ $tourCashedData['phone'] ?? '' }}" class="text-inpt" id="tour_phone" name="phone">
                      <div class="req" id="tour_phone_req">@lang('fnt.req')</div>
                    </div>
                  </div>
                  <div class="booking-form-i">
                    <label>@lang('fnt.email'):</label>
                    <div class="input-a" style="position: relative;">
                      <input type="text" value="{{ $tourCashedData['email'] ?? '' }}" class="text-inpt" id="tour_email" name="email">
                      <div class="req" id="tour_email_req">@lang('fnt.req')</div>
                    </div>
                  </div>
                  <div class="booking-form-i">
                    <label>@lang('fnt.natio'):</label>
                    <div class="card-expiration" style="width: 100%; position: relative;">
                      <div class="req" id="tour_nationality_req">@lang('fnt.req')</div>
                      <select class="custom-select" name="nationality" id="tour_nationality">
                        <option value="">-----</option>
                        <option value="afghan">Afghan</option>
                        <option value="albanian">Albanian</option>
                        <option value="algerian">Algerian</option>
                        <option value="american">American</option>
                        <option value="andorran">Andorran</option>
                        <option value="angolan">Angolan</option>
                        <option value="antiguans">Antiguans</option>
                        <option value="argentinean">Argentinean</option>
                        <option value="armenian">Armenian</option>
                        <option value="australian">Australian</option>
                        <option value="austrian">Austrian</option>
                        <option value="azerbaijani">Azerbaijani</option>
                        <option value="bahamian">Bahamian</option>
                        <option value="bahraini">Bahraini</option>
                        <option value="bangladeshi">Bangladeshi</option>
                        <option value="barbadian">Barbadian</option>
                        <option value="barbudans">Barbudans</option>
                        <option value="batswana">Batswana</option>
                        <option value="belarusian">Belarusian</option>
                        <option value="belgian">Belgian</option>
                        <option value="belizean">Belizean</option>
                        <option value="beninese">Beninese</option>
                        <option value="bhutanese">Bhutanese</option>
                        <option value="bolivian">Bolivian</option>
                        <option value="bosnian">Bosnian</option>
                        <option value="brazilian">Brazilian</option>
                        <option value="british">British</option>
                        <option value="bruneian">Bruneian</option>
                        <option value="bulgarian">Bulgarian</option>
                        <option value="burkinabe">Burkinabe</option>
                        <option value="burmese">Burmese</option>
                        <option value="burundian">Burundian</option>
                        <option value="cambodian">Cambodian</option>
                        <option value="cameroonian">Cameroonian</option>
                        <option value="canadian">Canadian</option>
                        <option value="cape verdean">Cape Verdean</option>
                        <option value="central african">Central African</option>
                        <option value="chadian">Chadian</option>
                        <option value="chilean">Chilean</option>
                        <option value="chinese">Chinese</option>
                        <option value="colombian">Colombian</option>
                        <option value="comoran">Comoran</option>
                        <option value="congolese">Congolese</option>
                        <option value="costa rican">Costa Rican</option>
                        <option value="croatian">Croatian</option>
                        <option value="cuban">Cuban</option>
                        <option value="cypriot">Cypriot</option>
                        <option value="czech">Czech</option>
                        <option value="danish">Danish</option>
                        <option value="djibouti">Djibouti</option>
                        <option value="dominican">Dominican</option>
                        <option value="dutch">Dutch</option>
                        <option value="east timorese">East Timorese</option>
                        <option value="ecuadorean">Ecuadorean</option>
                        <option value="egyptian">Egyptian</option>
                        <option value="emirian">Emirian</option>
                        <option value="equatorial guinean">Equatorial Guinean</option>
                        <option value="eritrean">Eritrean</option>
                        <option value="estonian">Estonian</option>
                        <option value="ethiopian">Ethiopian</option>
                        <option value="fijian">Fijian</option>
                        <option value="filipino">Filipino</option>
                        <option value="finnish">Finnish</option>
                        <option value="french">French</option>
                        <option value="gabonese">Gabonese</option>
                        <option value="gambian">Gambian</option>
                        <option value="georgian">Georgian</option>
                        <option value="german">German</option>
                        <option value="ghanaian">Ghanaian</option>
                        <option value="greek">Greek</option>
                        <option value="grenadian">Grenadian</option>
                        <option value="guatemalan">Guatemalan</option>
                        <option value="guinea-bissauan">Guinea-Bissauan</option>
                        <option value="guinean">Guinean</option>
                        <option value="guyanese">Guyanese</option>
                        <option value="haitian">Haitian</option>
                        <option value="herzegovinian">Herzegovinian</option>
                        <option value="honduran">Honduran</option>
                        <option value="hungarian">Hungarian</option>
                        <option value="icelander">Icelander</option>
                        <option value="indian">Indian</option>
                        <option value="indonesian">Indonesian</option>
                        <option value="iranian">Iranian</option>
                        <option value="iraqi">Iraqi</option>
                        <option value="irish">Irish</option>
                        <option value="israeli">Israeli</option>
                        <option value="italian">Italian</option>
                        <option value="ivorian">Ivorian</option>
                        <option value="jamaican">Jamaican</option>
                        <option value="japanese">Japanese</option>
                        <option value="jordanian">Jordanian</option>
                        <option value="kazakhstani">Kazakhstani</option>
                        <option value="kenyan">Kenyan</option>
                        <option value="kittian and nevisian">Kittian and Nevisian</option>
                        <option value="kuwaiti">Kuwaiti</option>
                        <option value="kyrgyz">Kyrgyz</option>
                        <option value="laotian">Laotian</option>
                        <option value="latvian">Latvian</option>
                        <option value="lebanese">Lebanese</option>
                        <option value="liberian">Liberian</option>
                        <option value="libyan">Libyan</option>
                        <option value="liechtensteiner">Liechtensteiner</option>
                        <option value="lithuanian">Lithuanian</option>
                        <option value="luxembourger">Luxembourger</option>
                        <option value="macedonian">Macedonian</option>
                        <option value="malagasy">Malagasy</option>
                        <option value="malawian">Malawian</option>
                        <option value="malaysian">Malaysian</option>
                        <option value="maldivan">Maldivan</option>
                        <option value="malian">Malian</option>
                        <option value="maltese">Maltese</option>
                        <option value="marshallese">Marshallese</option>
                        <option value="mauritanian">Mauritanian</option>
                        <option value="mauritian">Mauritian</option>
                        <option value="mexican">Mexican</option>
                        <option value="micronesian">Micronesian</option>
                        <option value="moldovan">Moldovan</option>
                        <option value="monacan">Monacan</option>
                        <option value="mongolian">Mongolian</option>
                        <option value="moroccan">Moroccan</option>
                        <option value="mosotho">Mosotho</option>
                        <option value="motswana">Motswana</option>
                        <option value="mozambican">Mozambican</option>
                        <option value="namibian">Namibian</option>
                        <option value="nauruan">Nauruan</option>
                        <option value="nepalese">Nepalese</option>
                        <option value="new zealander">New Zealander</option>
                        <option value="ni-vanuatu">Ni-Vanuatu</option>
                        <option value="nicaraguan">Nicaraguan</option>
                        <option value="nigerien">Nigerien</option>
                        <option value="north korean">North Korean</option>
                        <option value="northern irish">Northern Irish</option>
                        <option value="norwegian">Norwegian</option>
                        <option value="omani">Omani</option>
                        <option value="pakistani">Pakistani</option>
                        <option value="palauan">Palauan</option>
                        <option value="panamanian">Panamanian</option>
                        <option value="papua new guinean">Papua New Guinean</option>
                        <option value="paraguayan">Paraguayan</option>
                        <option value="peruvian">Peruvian</option>
                        <option value="polish">Polish</option>
                        <option value="portuguese">Portuguese</option>
                        <option value="qatari">Qatari</option>
                        <option value="romanian">Romanian</option>
                        <option value="russian">Russian</option>
                        <option value="rwandan">Rwandan</option>
                        <option value="saint lucian">Saint Lucian</option>
                        <option value="salvadoran">Salvadoran</option>
                        <option value="samoan">Samoan</option>
                        <option value="san marinese">San Marinese</option>
                        <option value="sao tomean">Sao Tomean</option>
                        <option value="saudi">Saudi</option>
                        <option value="scottish">Scottish</option>
                        <option value="senegalese">Senegalese</option>
                        <option value="serbian">Serbian</option>
                        <option value="seychellois">Seychellois</option>
                        <option value="sierra leonean">Sierra Leonean</option>
                        <option value="singaporean">Singaporean</option>
                        <option value="slovakian">Slovakian</option>
                        <option value="slovenian">Slovenian</option>
                        <option value="solomon islander">Solomon Islander</option>
                        <option value="somali">Somali</option>
                        <option value="south african">South African</option>
                        <option value="south korean">South Korean</option>
                        <option value="spanish">Spanish</option>
                        <option value="sri lankan">Sri Lankan</option>
                        <option value="sudanese">Sudanese</option>
                        <option value="surinamer">Surinamer</option>
                        <option value="swazi">Swazi</option>
                        <option value="swedish">Swedish</option>
                        <option value="swiss">Swiss</option>
                        <option value="syrian">Syrian</option>
                        <option value="taiwanese">Taiwanese</option>
                        <option value="tajik">Tajik</option>
                        <option value="tanzanian">Tanzanian</option>
                        <option value="thai">Thai</option>
                        <option value="togolese">Togolese</option>
                        <option value="tongan">Tongan</option>
                        <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                        <option value="tunisian">Tunisian</option>
                        <option value="turkish">Turkish</option>
                        <option value="tuvaluan">Tuvaluan</option>
                        <option value="ugandan">Ugandan</option>
                        <option value="ukrainian">Ukrainian</option>
                        <option value="uruguayan">Uruguayan</option>
                        <option value="uzbekistani">Uzbekistani</option>
                        <option value="venezuelan">Venezuelan</option>
                        <option value="vietnamese">Vietnamese</option>
                        <option value="welsh">Welsh</option>
                        <option value="yemenite">Yemenite</option>
                        <option value="zambian">Zambian</option>
                        <option value="zimbabwean">Zimbabwean</option>
                    </select>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="booking-form-i">
                    <label>@lang('fnt.pssprt'):</label>
                    <div class="input-a" style="position: relative;">
                      <input type="text" value="{{ $tourCashedData['passport'] ?? '' }}" class="text-inpt" id="tour_passport" name="passport">
                      <div class="req" id="tour_passport_req">@lang('fnt.req')</div>
                    </div>
                  </div>
                  @if($tour->day_type == 'days')
                  <div class="booking-form-i">
                    <label>@lang('fnt.date_t'):</label>
                    <div class="input-a">
                      <input type="text" value="{{ $tourCashedData['date_to'] ?? '' }}" class="date-inpt" id="tour_to" name="date_to" autocomplete="off">
                      <span class="date-icon"></span>
                    </div>
                  </div>
                  @endif
                  <div class="booking-form-i">
                    <label>{{ $tour->day_type == 'days' ? __('fnt.date_f') : __('fnt.date') }}: </label>
                    <div class="input-a" style="position: relative;">
                      <div class="req" id="tour_from_req">@lang('fnt.req')</div>
                      <input type="text" value="{{ $tourCashedData['date_from'] ?? '' }}" class="date-inpt" id="tour_from" name="date_from" autocomplete="off">
                      <span class="date-icon"></span>
                    </div>
                  </div>
                  
                  @if($tour->child_from && $tour->child_to && $tour->child_price)
                  <div class="booking-form-i">
                    <div class="card-expiration" style="width: 100%">
                      <label>@lang('fnt.child') @lang('fnt.from') {{ $tour->child_from }} @lang('fnt.to') {{ $tour->child_to }} @lang('fnt.old'):</label>
                      <select class="custom-select" id="tour_childs" onchange="adults_child_select()" name="childs">
                       @for ($i = 0; $i <= 20; $i++)
                        <option value="{{ $i }}" @if(isset($tourCashedData['childs']) && $tourCashedData['childs'] == $i) selected="selected" @endif >{{ $i }}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  @endif

                  <div class="booking-form-i">
                    <label>@lang('fnt.adults'):</label>
                    <div class="card-expiration" style="position: relative;width: 100%">
                      <div class="req" id="tour_adults_req">@lang('fnt.req')</div>
                    <select class="custom-select" name="adults" id="tour_adults" onchange="adults_child_select()">
                      @for ($i = 1; $i <= 20; $i++)
                      <option value="{{ $i }}" @if(isset($tourCashedData['adults']) && $tourCashedData['adults'] == $i) selected="selected" @endif>{{ $i }}</option>
                      @endfor
                    </select>
                    </div>
                    <div class="clear"></div>
                  </div>

                 
                  <div class="booking-form-i" style="float: left;">
                    <label>@lang('fnt.guide'):</label>
                    <div class="card-expiration" style="position: relative;width: 100%">
                      <div class="req" id="tour_guide_req">@lang('fnt.req')</div>
                      <select class="custom-select" name="guide" id="tour_guide">
                         <option value="">-----</option>
                          <option value="English" @if(isset($tourCashedData['guide']) && $tourCashedData['guide'] == 'English') selected="selected" @endif>English</option>
                          <option value="Spanish" @if(isset($tourCashedData['guide']) && $tourCashedData['guide'] == 'Spanish') selected="selected" @endif>Spanish</option>
                          <option value="German" @if(isset($tourCashedData['guide']) && $tourCashedData['guide'] == 'German') selected="selected" @endif>German</option>
                          <option value="Russian" @if(isset($tourCashedData['guide']) && $tourCashedData['guide'] == 'Russian') selected="selected" @endif>French</option>
                      </select>
                    </div>
                    <div class="clear"></div>
                  </div>

                  <div class="booking-form-i" style="width: 100%">
                    <label>@lang('fnt.hotel'):</label>
                    <div class="input-a" style="position: relative;">
                      <div class="req" id="tour_hotel_req">@lang('fnt.req')</div>
                      <input type="text" value="{{ $tourCashedData['hotel'] ?? '' }}" class="text-inpt" id="tour_hotel" name="hotel">
                    </div>
                  </div>

                  <div class="booking-form-i" style="width: 100%">
                    <label>
                      @lang('fnt.note'):
                      <br/><br/>
                      <span>
                      <!-- ( @lang('fnt.exclude_childs') ) -->
                      </span>
                    </label>
                    <div class="textarea-wrapper">
                      <textarea id="tour_note" name="note" style="width: 100%;" rows="8">{{ $tourCashedData['note'] ?? '' }}</textarea>
                    </div>
                  </div>
                  
                  <div class="clear"></div>
                </div>
                <div class="booking-devider no-margin"></div> 
                <!-- <h2>Payament</h2> -->
                @if(!$tour->check_avail)
                <div class="payment-wrapper">
                  <div class="payment-tabs">
                    <a href="#" class="active">
                        @lang('fnt.payments')
                      <span></span></a>
                    <!-- <a href="#">Other<span></span></a> -->
                  </div>
                  <div class="clear"></div>
                  <div class="payment-tabs-content">
                    <!-- // -->
                    <div class="payment-tab">
                      <div class="payment-type">
                        <input type="radio" value="visa" name="payment_type" checked="" />
                        <img src="{{ asset('public/front/img/') }}/paymentt-01.png">
                        <img src="{{ asset('public/front/img/') }}/paymentt-02.png">
                      </div>
                      <div class="clear"></div>
                      @if($app_settings['show_cash'])
                      <div class="payment-type">
                        <input type="radio" value="cash" name="payment_type" @if(isset($tourCashedData['payment_type']) && $tourCashedData['payment_type'] == 'cash') checked="checked" @endif />
                        @lang('fnt.cash')
                      </div>
                      @endif
                      <div class="checkbox">
                        <div class="booking-devider no-margin"></div>
                        <label>
                            <input type="checkbox" value="1" name="tour_accept" id="tour_accept" />
                            @lang('fnt.iaccept') <a href="{{ url('/terms') }}">@lang('fnt.terms')</a>
                        </label>
                      </div> 
                      <div class="req" style="position: relative;padding-top: 10px;font-weight: bold;" id="tour_accept_req">@lang('fnt.accept') @lang('fnt.terms')!</div>
                    </div>
                    <!-- \\ -->
                    <!-- // -->
                    <!-- <div class="payment-tab">
                      <div class="payment-alert">
                        <span>You will be redirected to website to securely complete your payment.</span>
                        <div class="payment-alert-close"><a href="#"><img alt="" src="{{ asset('public/front/img/') }}/alert-close.png"></a></div>
                      </div>
                      <a href="#" class="paypal-btn">proceed</a>
                    </div> -->
                    <!-- \\ -->
                  </div>
                </div>
                @endif
                <div class="booking-complete">
                  @if(!$tour->check_avail)
                  <h2>@lang('fnt.review')</h2>
                  <p>
                    @lang('fnt.sure')
                  </p> 
                  @endif 
                  <button class="booking-complete-btn" type="button" onclick="booking_action()" style="float: right;">{{ $tour->check_avail ? __('fnt.ch_avail') : __('fnt.book') }}</button>

                  <img id="loading" src="{{ asset('public/front/img/') }}/89.gif" style="float: right; margin: 30px 10px 0 0px;display: none;" />

                  <div id="error_message" style="float: right; margin: 35px 10px 0 0px;color: rgb(253, 48, 48);display: none;font-weight: bold;"></div>

                  <div class="clear"></div>
                </div>
                
              </div>
              {{-- @else --}}
              <!-- <div class="booking-left"> 
                <div class="booking-complete">
                  <h2>Please, Login to Complete Booking.</h2>
                  <p>
                    Make Sure you are regustered and login in to be able to Book with us.
                  </p>  
                </div> 
              </div> -->
              {{-- @endif --}}
              </div>
            </div>
            <div class="clear"></div>
        </div>
        

        
        <div class="clear"></div>
        </div>
      </form>
    </div>  
  </div>  
</div>
<!-- /main-cont -->
@endsection
@section('javascript')
  <script>
    function opts_calc(){
      var tour_adults = $("#tour_adults").val() ? $("#tour_adults").val() : 1;
      var tour_childs = $("#tour_childs").val() ? $("#tour_childs").val() : 0;
      var total_opts = 0;
      var app_fees = parseInt("{{ $app_settings['app_fees'] }}");

      $('.opts_inputs').each(function () {
          var opt_price = $(this).attr('data-price');
           if (this.checked) {
               total_opts += (parseInt(tour_adults) * parseFloat(opt_price)) + (parseInt(tour_childs) * parseFloat(opt_price));
           }
      });
      
      $("#total_options_price").val(total_opts);

      //Calculate total
      var total_presons_price = parseFloat($("#total_presons_price").text());
      var total = total_presons_price + total_opts;
      $("#total").text( (total).toFixed(2) );

      var total_fees = ( ( app_fees / 100 ) * total + total);

      $("#total_w_fees").text( (total_fees).toFixed(2) );
    }

    function adults_child_select(){
      var prices = JSON.parse('<?php echo json_encode($pricesJson) ?>');
      

      var adults_price = {{ $tour->price }};
      var childs_price = {{ $tour->child_price ?? 0 }};

      var tour_adults = $("#tour_adults").val();
      var tour_childs = $("#tour_childs").val() ? $("#tour_childs").val() : 0;

      if(tour_adults > 1){
        Object.keys(prices).forEach(function(key) {
          if(tour_adults >= parseInt(prices[key]['from']) && tour_adults <= parseInt(prices[key]['to'])){
            adults_price = prices[key]['price'];
            return;
          }
        });
      }      

      var adults_total = ( parseFloat(adults_price) * parseInt(tour_adults) );
      var childs_total = ( parseFloat(childs_price) * parseInt(tour_childs) );

      $("#total_adults_num").text(tour_adults);
      $("#total_childs_num").text(tour_childs);

      $("#total_presons_price").text( (adults_total + childs_total).toFixed(2) );

      opts_calc();
    }

    function booking_action(){
      var vali_arr = ['tour_first_name', 'tour_last_name', 'tour_email', 'tour_phone', 'tour_passport', 'tour_nationality', 'tour_from', 'tour_adults', 'tour_guide' ,'tour_hotel'];

      var validate = true;
      for (var i = 0; i < vali_arr.length; i++) {
        if( !$('#'+vali_arr[i]).val() || 
            $('#'+vali_arr[i]).val() == '' || 
            $('#'+vali_arr[i]).val() == undefined || 
            $('#'+vali_arr[i]).val() == null )
        {
          $('#'+vali_arr[i]+'_req').fadeIn();
          validate = false;
        }else{
          $('#'+vali_arr[i]+'_req').fadeOut();
        }
      }

      @if(!$tour->check_avail)
        if (!$('#tour_accept').is(":checked")){
          $('#tour_accept_req').fadeIn();
          validate = false;
        }else{
          $('#tour_accept_req').fadeOut();
        }
      @endif
      
      if(validate){
        $('#loading').fadeIn();
        $('#error_message').fadeOut();

        var form=$("#booking_form").submit();

        // $.ajax({
        //     type:"POST",
        //     url:form.attr("action"),
        //     headers: {
        //         'X-CSRF-TOKEN': '{{ csrf_token() }}'
        //     },
        //     data: $("#booking_form").serializeArray(),//only input
        //     success: function(response){
        //       $('#loading').fadeOut();
        //       if(response.status == 'success'){
        //         window.location.href = "{{ url('/bookingsuccess') }}"
        //       }else{
        //         $('#error_message').text(response.message);
        //         $('#error_message').fadeIn();
        //       }
        //     }
        // });
      }else{
        $('#error_message').text('Please check required fields!');
        $('#error_message').fadeIn();
      }
    }
  </script>
  @endsection