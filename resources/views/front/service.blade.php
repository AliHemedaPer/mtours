@extends('front.front')

@section('content')
<section class="Design" style="margin-top: 50px;min-height: 400px;" >
      <div class="container" @if($share_locale == 'ar')
     style="direction: rtl;" @endif> 
        <div class="row"> 
          @if($service->page_image)
          <div class="col-lg-6 col-md-12 col-sm-12 " @if($share_locale == 'ar')
     style="text-align: right;" @endif>
          @else
          <div class="col-lg-12 col-md-12 col-sm-12 " @if($share_locale == 'ar')
     style="text-align: right;" @endif>
          @endif
            <h2>{{ $title[0] }} <span> @for ($i = 1; $i < count($title); $i++)
                                            {{ $title[$i] }}
                                        @endfor
                                </span></h2>
            <p>{{ $service->translate($share_locale)->description }}</p>
            
          </div>
          @if($service->page_image)
            <div class="col-lg-6 col-md-12 col-sm-12 text-center">
              <div class="image"><img class="img-thumbnail" src="{{url($service->page_image)}}"></div>
            </div>
          @endif
          
        </div>
      </div>
    </section>
@stop