<div class="col-lg-12"> 
        <div class="form-group">
        <label for="form-field-select-3">
            Single Person Price
        </label>
        {!! Form::text('price',($transportation)?$transportation->price:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
    </div>
</div>

<div style="clear: both;"></div>

<div class="tabbable">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example1" data-toggle="tab">
                Destination (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example1" data-toggle="tab">
                Destination (SP)
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example1">
            {!! Form::text('destination[en]',($transportation)?$transportation->translate('en')->destination:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example1">
            {!! Form::text('destination[sp]',($transportation)?$transportation->translate('sp')->destination:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>

<div style="clear: both;"></div>

<div class="tabbable">
    <ul id="myTab1" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example2" data-toggle="tab">
                Description (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example2" data-toggle="tab">
                Description (SP)
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example2">
            {!! Form::textarea('description[en]',($transportation)?$transportation->translate('en')->description:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example2">
            {!! Form::textarea('description[sp]',($transportation)?$transportation->translate('sp')->description:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>

<!-- ///////////////// Custome Prices Area -->
<hr/>
<div style="clear: both;"></div>
    <h4 style="color: #fdaa34;">Prices</h4>
    <div id="custome_prices">
        {{-- @if($custome_prices) --}}
            @php
                $prices_count = 0
            @endphp
            @foreach($custome_prices as $key => $cutome_price)
                <div class="col-lg-1" style="padding-top: 25px;"># {{ $key+1 }}</div> 
                <div class="col-lg-4"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Persons Num From
                        </label>
                        {!! Form::number('cut_price['.$key.'][from]', $cutome_price['from'] ?? null, ['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-4"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Persons Num To
                        </label>
                        {!! Form::number('cut_price['.$key.'][to]', $cutome_price['to'] ?? null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-3"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Price
                        </label>
                        {!! Form::text('cut_price['.$key.'][price]', $cutome_price['price'] ?? null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div style="clear: both;"></div>
                @php
                    $prices_count = $key+1
                @endphp
                
            @endforeach
        {{-- @endif --}}
    </div>
    <div style="text-align: center;">
        <a class="btn btn-warning center" href="javascript://" onclick="addPrice()"><i class="icon-plus2 mr-2"></i> Add new Price <i class="fa fa-plus"></i></a>
        <input type="hidden" id="prices_count" value="{{ $prices_count }}">
    </div>
<div style="clear: both;"></div>
<hr/>
<!-- ///////////////// End Custome Prices Area -->

<div class="row"> 
    <div class="col-lg-12" style="text-align: right;">       
        <a class="btn btn-danger" href="{{url('admin/transportations')}}"><i class="icon-plus2 mr-2"></i>Cancel <i class="fa fa-minus-circle"></i></a>
        <button type="submit" class="btn btn-success">{{$buttonAction}} <i class="fa fa-save"></i></button>
    </div>
</div>
