{!! Form::hidden('cat',$cat) !!}
{!! Form::hidden('parent',$parent) !!}

<div class="col-lg-6"> 
        <div class="form-group">
        <label for="form-field-select-3">
            Category
        </label>
        <select class="form-control" name="cat_id" id="cat_id"  style="padding: 0 12px">
            <option value="">-- Select --</option>
            @foreach($categories as $parent_)
                <option value="{{ $parent_->id }}" @if(isset($product) && $product->cat_id == $parent_->id) selected="" @endif>{{ $parent_->translate('en')->title }}</option>
                <
            @endforeach
        </select>
    </div>
</div>

<!-- <div class="col-lg-12"> 
        <div class="form-group">
        <label for="form-field-select-3">
            القسم الفرعي
        </label>
        <select  class="form-control " name="subcat_id" id="subcat_id" style="padding: 0 12px">
            <option value="0">-- إختيار --</option>
        </select>
    </div>
</div> -->
<div class="col-lg-6"> 
        <div class="form-group">
        <label for="form-field-select-3">
            Single Person Price
        </label>
        {!! Form::text('price',($product)?$product->price:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
    </div>
</div>

<div class="col-lg-6"> 
        <div class="form-group">
        <label for="form-field-select-3">
            Tour Type
        </label>
        <select class="form-control" name="day_type" style="padding: 0 12px">
                <option value="full" @if(isset($product) && $product->day_type == 'full') selected="" @endif>Full Day</option>
                <option value="half" @if(isset($product) && $product->day_type == 'half') selected="" @endif>Half Day</option>
                <option value="days" @if(isset($product) && $product->day_type == 'days') selected="" @endif>Days</option>
        </select>
    </div>
</div>


<div class="col-lg-6"> 
        <div class="form-group">
        <label for="form-field-select-3">
            Number of Days
        </label>
        {!! Form::number('days',($product)?$product->days:null,['class'=>'form-control' ,'placeholder'=>'']) !!}
    </div>
</div>

<div style="clear: both;"></div>

<div class="tabbable">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example1" data-toggle="tab">
                Title (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example1" data-toggle="tab">
                Title (SP)
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example1">
            {!! Form::text('title[en]',($product)?$product->translate('en')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example1">
            {!! Form::text('title[sp]',($product)?$product->translate('sp')->title:null,['class'=>'form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>

<div style="clear: both;"></div>
<hr/>
<div class="form-group row text-center">
    <label class="col-form-label col-lg-2" style="text-align:center">Check Availability?</label>
    <div class="col-lg-1">       
        {!! Form::checkbox('check_avail', null, ($product && $product->check_avail == 1)?true:false) !!}
    </div>
</div>

<div class="form-group row text-center">
    <label class="col-form-label col-lg-2" style="text-align:center">Is Hot Offer?</label>
    <div class="col-lg-1">       
        {!! Form::checkbox('hot_offer', null, ($product && $product->hot_offer == 1)?true:false) !!}
    </div>
</div>
<hr/>
<div style="clear: both;"></div>

<div class="tabbable">
    <ul id="myTab1" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#panel_tab1_example2" data-toggle="tab">
                Description (EN)
            </a>
        </li>
        <li class="">
            <a href="#panel_tab2_example2" data-toggle="tab">
                Description (SP)
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="panel_tab1_example2">
            {!! Form::textarea('description[en]',($product)?$product->translate('en')->description:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'']) !!}
        </div>
        <div class="tab-pane " id="panel_tab2_example2">
            {!! Form::textarea('description[sp]',($product)?$product->translate('sp')->description:null,['class'=>'ckeditor form-control','required'=>'true','placeholder'=>'']) !!}
        </div>      
    </div>
</div>

<!-- ///////////////// Custome Prices Area -->
<hr/>
<div style="clear: both;"></div>
    <h4 style="color: #fdaa34;">Prices</h4>
    <div id="custome_prices">
        {{-- @if($custome_prices) --}}
            @php
                $prices_count = 0
            @endphp
            @foreach($custome_prices as $key => $cutome_price)
                <div class="col-lg-1" style="padding-top: 25px;"># {{ $key+1 }}</div> 
                <div class="col-lg-4"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Persons Num From
                        </label>
                        {!! Form::number('cut_price['.$key.'][from]', $cutome_price['from'] ?? null, ['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-4"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Persons Num To
                        </label>
                        {!! Form::number('cut_price['.$key.'][to]', $cutome_price['to'] ?? null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-3"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Price
                        </label>
                        {!! Form::text('cut_price['.$key.'][price]', $cutome_price['price'] ?? null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div style="clear: both;"></div>
                @php
                    $prices_count = $key+1
                @endphp
                
            @endforeach
        {{-- @endif --}}
    </div>
    <div style="text-align: center;">
        <a class="btn btn-warning center" href="javascript://" onclick="addPrice()"><i class="icon-plus2 mr-2"></i> Add new Price <i class="fa fa-plus"></i></a>
        <input type="hidden" id="prices_count" value="{{ $prices_count }}">
    </div>
<div style="clear: both;"></div>
<hr/>
<!-- ///////////////// End Custome Prices Area -->

<!-- ///////////////// Child Prices Area -->
<div style="clear: both;"></div>
    <h4 style="color: #fdaa34;">Childs Price</h4>
    <div id="custome_prices">
        <div class="col-lg-1" style="padding-top: 25px;">#</div> 
        <div class="col-lg-4"> 
            <div class="form-group">
                <label for="form-field-select-3">
                    Child Age From
                </label>
                {!! Form::text('child_from', ($product)?$product->child_from:null, ['class'=>'form-control' ,'placeholder'=>'']) !!}
            </div>
        </div>
        <div class="col-lg-4"> 
            <div class="form-group">
                <label for="form-field-select-3">
                    Child Age To
                </label>
                {!! Form::text('child_to', ($product)?$product->child_to:null, ['class'=>'form-control' ,'placeholder'=>'']) !!}
            </div>
        </div>
        <div class="col-lg-3"> 
            <div class="form-group">
                <label for="form-field-select-3">
                    Price
                </label>
                {!! Form::text('child_price', ($product)?$product->child_price:null, ['class'=>'form-control' ,'placeholder'=>'']) !!}
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
<div style="clear: both;"></div>
<hr/>
<!-- ///////////////// End Custome Prices Area -->

<!-- ///////////////// Optional Activities Area -->
<div style="clear: both;"></div>
    <h4 style="color: #fdaa34;">Optional Activities</h4>
    <div id="opt_activities">
            @php
                $activities_count = 0
            @endphp
            @foreach($opt_activities as $activity)
                <div class="col-lg-1" style="padding-top: 25px;"># {{ $loop->index+1 }}</div> 
                <div class="col-lg-5"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Title (EN)
                        </label>
                        {!! Form::text('opt_act['.$loop->index.'][title][en]', isset($activity->title) ? $activity->translate('en')->title : null, ['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-5"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Title (SP)
                        </label>
                        {!! Form::text('opt_act['.$loop->index.'][title][sp]', isset($activity->title) ? $activity->translate('sp')->title : null, ['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div class="col-lg-1"> 
                    <div class="form-group">
                        <label for="form-field-select-3">
                            Price
                        </label>
                        {!! Form::text('opt_act['.$loop->index.'][price]', $activity->price ?? null,['class'=>'form-control' ,'placeholder'=>'']) !!}
                    </div>
                </div>
                <div style="clear: both;"></div>
                @php
                    $activities_count = $loop->index+1
                @endphp
                
            @endforeach
    </div>
    <div style="text-align: center;">
        <a class="btn btn-warning center" href="javascript://" onclick="addActivity()"><i class="icon-plus2 mr-2"></i> Add new Activity <i class="fa fa-plus"></i></a>
        <input type="hidden" id="activities_count" value="{{ $activities_count }}">
    </div>
<div style="clear: both;"></div>
<hr/>
<!-- ///////////////// End Optional Activities Area -->

<div class="form-group">
    <div class="col-sm-4">
        <label>
            Image <span style="direction: ltr;">(450X360)</span>
        </label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                @if($product && file_exists($product->image))
                <img src="{{url($product->image)}}" alt=""/>
                @else
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
                @endif
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                    <input type="file" name="image">
                </span>
                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                    <i class="fa fa-times"></i> Remove
                </a>
            </div>
        </div>                        
    </div>
</div>
 <hr/>



<div class="row"> 
    <div class="col-lg-12" style="text-align: right;">       
        <a class="btn btn-danger" href="{{url('admin/products')}}{{ $cat ? '?cat='.$cat : '' }}{{ $parent ? '?parent='.$parent : '' }}"><i class="icon-plus2 mr-2"></i>Cancel <i class="fa fa-minus-circle"></i></a>
        <button type="submit" class="btn btn-success">{{$buttonAction}} <i class="fa fa-save"></i></button>
    </div>
</div>
