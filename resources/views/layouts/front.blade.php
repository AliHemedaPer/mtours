<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--canvas canvas-styleArb-->
    <link rel="stylesheet" href="{{url('public/front/')}}/css/canvas-{{ $share_locale == 'ar' ? 'styleArb' : 'style' }}.css">
    <!--Icon Browser-->
    <!--link(rel="shortcut icon" href="images/RallyIcon.ico" type="image/x-icon")-->
    <!--link(rel="icon" href="images/RallyIcon.ico" type="image/x-icon")-->
    <!--Sarabun Font For Google-->
    <link rel="stylesheet" href="{{url('public/front/')}}/MoonTypeface/MoonBold.otf">
    <link rel="stylesheet" href="{{url('public/front/')}}/MoonTypeface/MoonLight.otf">
    <!--Sarabun Font For Google-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo:400,600,700">
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="{{url('public/front/')}}/css/aos.css">
    <!--Bootstrap CSS  bootstraprtl-->
    <link rel="stylesheet" href="{{url('public/front/')}}/css/{{ $share_locale == 'ar' ? 'bootstraprtl' : 'bootstrap' }}.min.css">
    <!--Fontawesome-->
    <link rel="stylesheet" href="{{url('public/front/')}}/css/fontawesome.min.css">
    <!--Main-Style-->
    <link rel="stylesheet" href="{{url('public/front/')}}/css/main-style.css">

    @if($share_locale == 'ar')
     <link rel="stylesheet" href="{{url('public/front/')}}/css/homepage-rtl.css">
    @endif
    <title>Alnasq Alarabi Co.</title>
  </head>
  <body oncontextmenu="return false" onselectstart="return false" ondragstart="return false" >

    @if(isset($ishome))
     <!--Start Loader -->
    <div class="loader"> 
      <div class="image"><img class="svg" src="{{url('public/front/')}}/images/loader/Alnask.svg" alt=""><img class="logo" src="{{url('public/front/')}}/images/loader/logo.png" alt=""></div>
    </div>
    <!--End Loader  -->
    @endif

    
    <!-- Start Nav Bar-->
    <nav class="navbar navbar-expand-lg " id="navbar">  
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent"> 
          <ul class="navbar-nav mr-auto">
            <li class="nav-item @if(strpos($_SERVER['REQUEST_URI'], 'home') !== false)) active @endif"><a class="nav-link" href="{{url('home')}}">@lang('frontend.home')</a></li>
            <li class="nav-item @if(strpos($_SERVER['REQUEST_URI'], 'about') !== false)) active @endif"><a class="nav-link" href="{{url('about')}}">@lang('frontend.about')</a></li>
            <li class="nav-item dropdown @if(strpos($_SERVER['REQUEST_URI'], 'services') !== false)) active @endif"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('frontend.services')</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                @foreach($share_services as $service)
                    <a class="dropdown-item" href="{{url('services/'.$service->id)}}">{{ $service->translate($share_locale)->title }}</a>
                @endforeach
              </div>
            </li>
            <li class="nav-item @if(strpos($_SERVER['REQUEST_URI'], 'ourworks') !== false)) active @endif"><a class="nav-link" href="{{url('ourworks')}}">@lang('frontend.works')</a></li>
            <!--li(class="nav-item")a(class="nav-link" href="#about.html") Customers
            -->
            <li class="nav-item"><a class="nav-link" href="#contactus">@lang('frontend.contact')</a></li>
            <li class="nav-item"><a class="nav-link" href="javascript:://" @if($share_locale == 'ar') onclick="changeLang('en') @else onclick="changeLang('ar') @endif">@lang('frontend.language') <i class="fas fa-globe"></i></a></li>
          </ul>
        </div>
      </div>
    </nav>

    @if(isset($top_slider_img))
      <section class="{{ $top_slider_img }}" @if(isset($bannerBkg)) style="background: url({{url($bannerBkg)}}) no-repeat center center;" @endif></section>
    @endif
    <!-- End Nav Bar -->
    
        <!--Start Design-->
        @yield('content')
        <!--End Works-->
    
    <!--Start Footer-->
    @if(!isset($ishome))
    <section class="Footer" id="contactus" style="position: relative;">

      <ul class="social-nav model-3d-0 footer-social social two" style="    position: absolute;
    {{ $share_locale == 'ar' ? 'right' : 'left' }}: -25px;
    bottom: -30px;
    z-index: 999;">
        <li><a class="facebook" href="#">
            <div class="front"><i class="fab fa-facebook-f"></i></div>
            <div class="back"><i class="fab fa-facebook-f"></i></div></a></li>
        <li><a class="twitter" href="#">
            <div class="front"><i class="fab fa-twitter"></i></div>
            <div class="back"><i class="fab fa-twitter">   </i></div></a></li>
        <li><a class="instagram" href="#">
            <div class="front"><i class="fab fa-instagram"></i></div>
            <div class="back"><i class="fab fa-instagram"> </i></div></a></li>
        <li><a class="behance" href="#">
            <div class="front"><i class="fab fa-behance"></i></div>
            <div class="back"><i class="fab fa-behance">           </i></div></a></li>
        <li><a class="linkedin" href="#">
            <div class="front"><i class="fab fa-linkedin-in"></i></div>
            <div class="back"><i class="fab fa-linkedin-in">                    </i></div></a></li>
      </ul>

      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-12 col-sm-12 text-center">
            <div class="title"> 
              <p>@lang('frontend.contact')<span> <i class="fas fa-angle-double-right"></i></span></p>
            </div>
          </div>
          <div class="col-lg-3 col-md-12 col-sm-12">
            <div class="Contact-call">
              <h4>@lang('frontend.ouraddress')</h4>
              <p> <i class="fas fa-map-marker-alt"></i>@lang('frontend.theaddress')</p>
              <h4>@lang('frontend.call')</h4>
              <p> <i class="fas fa-mobile-alt"></i>+966543741157</p>
              <p> <i class="fas fa-mobile-alt"></i>+966566635338</p>
              <h4>@lang('frontend.emailus')</h4>
              <p> <i class="fas fa-envelope"></i>info@alnasqarb.com</p>
              <h4>  <a href="{{ url('joinus') }}">@lang('frontend.join')<i class="fas fa-hand-point-left"></i></a></h4>
            </div>
          </div>
          <div class="col-lg-3 col-md-12 col-sm-12 text-center"><img src="{{url('public/front/')}}/images/logo.png" alt="" width="100%"></div>
          <div class="col-lg-3 col-md-12 col-sm-12 text-center">
            <div class="Services"> 
              <h4>@lang('frontend.services')</h4>
              @foreach($share_services as $service)
                  <a href="{{url('services/'.$service->id)}}">{{ $service->translate($share_locale)->title }}</a>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>
    @endif

    <!--Start Copy-right-->
    <div class="transform">
      <div class="rotate"></div>
    </div>
    <footer>
      <div class="container text-center">
        <p>@lang('frontend.poweredby')</p>
      </div>
    </footer>
    <!--End Footer-->
    <!--include include/-footer.pug-->
    <!--Optional JavaScript-->
    <!--jQuery first, then Popper.js, then Bootstrap JS-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="{{url('public/front/')}}/js/popper.min.js"></script>
    <script src="{{url('public/front/')}}/js/bootstrap.min.js"></script>
    <!--Canvas Move Mouse-->
    <script src="{{url('public/front/')}}/js/canvas-particles.min.js"></script>
    <script src="{{url('public/front/')}}/js/canvas-main.js"></script>
    <!--Counter Timer-->
    <script src="{{url('public/front/')}}/js/waypoints.min.js"></script>
    <script src="{{url('public/front/')}}/js/jquery.counterup.min.js"></script>
    <!--Animet-->
    <script src="{{url('public/front/')}}/js/aos.js"></script>
    <script src="{{url('public/front/')}}/js/main-Counter.js"></script>
    <script src="{{url('public/front/')}}/js/main.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {

        $(document)[0].oncontextmenu = function() { return false; }

        $(document).mousedown(function(e) {
            if( e.button == 2 ) {
                alert('Sorry, this functionality is disabled!');
                return false;
            } else {
                return true;
            }
        });
    });

    function changeLang(lang){
      $.ajax({
        url: "{{url('/')}}/locale/"+lang,
        type: 'GET',
        success: function (data) {
           location.reload();
          }
        });
    }

    function servicemail(){
      let title = $('#service_title').val();
      let uname = $('#service_uname').val();
      let email = $('#service_email').val();
      let phone = $('#service_phone').val();
      let message = $('#service_message').val();

      if(title == '' || uname == '' || email == '' || phone == '' || message == ''){
        alert('All fileds are required!');
        return;
      }
      $('#service_sending').fadeIn(100);

      $.ajax({
        url: "{{url('/')}}/servicemail",
        type: 'POST',
        data:{title:title, uname:uname, email:email, phone:phone, message:message },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
           $('#service_sending').fadeOut(100);
           $('#exampleModal').modal('toggle');
          }
        });
    }

    function viewwork(id){
      $('.works_tabs').removeClass('active');
      $('#worktab-'+id).addClass('active');

      $('.work_images').fadeOut(0);
      $('#work-'+id).fadeIn(300);
    }

    @if(isset($category) && $category)
        getSubcats({{ $category }}, {{ $subcategory }})
    @endif
    function getSubcats(cat_id, subcat_id = 0){
        let tit = "@lang('frontend.all')";
        let url = '{{ url('') }}' + '/subcategories/' + cat_id;
        jQuery('#subcat_id').html('<option value="0">--- '+tit+' ---</option>');
        //jQuery('#loader').fadeIn();
        jQuery.get(url, function(data) {

            jQuery.each(data,function(key, value) {
                let selected = '';
                if(subcat_id == value.id)
                    selected = 'selected';
                
                jQuery('#subcat_id').append('<option value="' + value.id + '"'+ selected +' >' + value.value + '</option>');
            });
             //jQuery('#loader').fadeOut(10);
        });
    }
    </script>
  </body>
</html>