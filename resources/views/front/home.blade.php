@extends("front.front")
@section("content")
<!-- main-cont -->
<div class="main-cont">
  <div class="mp-slider search-only">
    <!-- // slider // -->
    <div class="mp-slider-row slim-slider">
      <div class="swiper-container">
        <a href="#" class="arrow-left"></a>
        <a href="#" class="arrow-right"></a>
        <div class="swiper-pagination"></div>       
          <div class="swiper-wrapper">
            @foreach($banners as $banner)
              <div class="swiper-slide"> 
                <div class="slide-section" style="background:url({{ url($banner->image) }}) center top no-repeat;">
                  <div class="mp-slider-lbl">{{ $banner->translate($share_locale)->title }}</div>
                  <div class="mp-slider-lbl-a">{{ $banner->translate($share_locale)->description }}</div>
                  @if($banner->url)
                  <div class="mp-slider-btn"><a href="{{ url($banner->url) }}" target="_blank" class="btn-a">@lang('fnt.details')</a></div>
                  @endif
                </div>      
              </div>
              @endforeach     
              </div>            
          </div>
      </div>
    </div>
    <!-- \\ slider \\ -->
  </div>  
  
  <div class="wrapper-a-holder full-width-search">
  <div class="wrapper-a">
  
    <!-- // search // -->
    <div class="page-search full-width-search search-type-b">
      <div class="search-type-padding">
      <nav class="page-search-tabs">
        <div class="search-tab active">@lang('fnt.tu_search')</div>
        <!-- <div class="search-tab">Tours</div> -->
        <!-- <div class="search-tab nth">Tickets</div> -->
        <div class="clear"></div> 
      </nav>    
      <div class="page-search-content" style="border: 1px solid #c9d9e6;">
      
        <!-- // tab content Tours // -->
        <form method="get" action="{{ url('/tours') }}">
        <div class="search-tab-content">
          <div class="page-search-p">
          <!-- // -->
            <div class="search-large-i">
            <!-- // -->
            <div class="srch-tab-line no-margin-bottom">
              <label>@lang('fnt.ky')</label>
              <div class="input-a"><input type="text" name="key" value="" placeholder="Type Here..."></div>
            </div>
            <!-- // -->             
            </div>
          <!-- \\ -->
          <!-- // -->
            <div class="search-large-i">
            <!-- // -->
            <div class="srch-tab-line no-margin-bottom">
              <label>@lang('fnt.cat')</label>
                <div class="select-wrapper">
                <select class="custom-select" name='p' id="cat_id">
                  <option value="">--</option>
                  @foreach($share_categories as $category)
                    @if($category->parent_id == 0)
                      <option value="{{ $category->id }}" {{ (isset(request()->query()['p']) && request()->query()['p'] == $category->id) ? 'selected' : '' }}>{{ $category->translate($share_locale)->title }}</option>
                    @endif  
                  @endforeach
                </select>
                </div>  
            </div>
            <!-- \\ -->           
            </div>
          <!-- \\ -->
          <!-- // -->
            <div class="search-large-i">
            <!-- // -->
            <div class="srch-tab-line no-margin-bottom">
              <label>@lang('fnt.scat')</label>
                <div class="select-wrapper">
                <select class="custom-select" name='c' id="subcat_id">
                  <option value="">--</option>
                </select>
                </div>  
            </div>
            <!-- \\ -->           
            </div>
          <!-- \\ -->
          <div class="clear"></div>

          </div>
          <footer class="search-footer">
            <button type="submin" class="srch-btn">@lang('fnt.srch')</button> 
            <!-- <span class="srch-lbl">Advanced Search options</span> -->
            <div class="clear"></div>
          </footer>
        </div>
        </form>
        <!-- // tab content Tours // -->
                      
      </div>
    </div>
    </div>
    <!-- \\ search \\ -->

    <div class="clear"></div> 
   </div>
  </div>
    
  <div class="mp-pop">
    <div class="wrapper-padding-a">
      <div class="popular-slider">
        <header class="fly-in page-lbl">
          <b>@lang('fnt.hotoffers')</b>
          <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni<br />dolores eos qui.</p> -->
        </header>
        <div class="">
          <div id="offers" class="owl-slider">
          @foreach($hotoffers as $offer)
          <!-- // -->
            <div class="offer-slider-i">
              <a class="offer-slider-img" href="{{ url('tour/'.$offer->id)}}">
                <img alt="" src="{{ $offer->image }}" />
                <span class="offer-slider-overlay">
                  <span class="offer-slider-btn">@lang('fnt.details')</span>
                </span>
              </a>
              <div class="offer-slider-txt">
                <div class="offer-slider-link"><a href="{{ url('tour/'.$offer->id)}}">{{ $offer->translate($share_locale)->title }}</a></div>
                <div class="offer-slider-l">
                  @if($offer->categories)
                  <div class="offer-slider-location">Location: 
                    <a href="{{ url('tours/?c='.$offer->cat_id)}}">
                      {{ $offer->categories->translate($share_locale)->title }} 
                    </a>
                  </div>
                  @endif
                  <div class="offer-slider-location">@lang('fnt.views'): 
                    {{ $offer->views }} 
                  </div>
                  <!-- <nav class="stars">
                    <ul>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-b.png" /></a></li>
                      <li><a href="#"><img alt="" src="{{ asset('public/front')}}/img/star-a.png" /></a></li>
                    </ul>
                    <div class="clear"></div>
                  </nav> -->
                </div>
                <div class="offer-slider-r">
                  <b>{{ $offer->displayPrice() }} {{ $app_settings['app_currency'] }}</b>
                  <span>@lang('fnt.pricing')</span>
                </div>
                <div class="offer-slider-devider"></div>                
                <div class="clear"></div>
              </div>
            </div>
          <!-- \\ -->
          @endforeach
          </div>          
        </div>
      </div>
      
      <div class="mp-popular" style="padding-top: 35px;border-top: 3px solid #fff;">
        <header class="fly-in">
          <b>{{ $about->translate($share_locale)->title }}</b>
          <div class="columns-row">
            <div class="column mm-3">
              <img width="100%" src="{{ url($about->image) }}" class="attachment-large" alt="" />
            </div>
            <div class="column mm-6">
              <p style="width: 100%;text-align: left;">
                {{ $about->translate($share_locale)->short_description }}
              </p>
            </div>
          </div>

          
        </header>
        <!-- <div class="fly-in advantages-row flat">
          <div class="flat-adv">
            <div class="flat-adv-a">
              <div class="flat-adv-l">
                <img alt="" src="{{ asset('public/front')}}/img/adv-a-01.png" />
              </div>
              <div class="flat-adv-r">
                <div class="flat-adv-rb">
                  <div class="flat-adv-b">Awesome design</div>
                  <div class="flat-adv-c">Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium doloremque la dantiumeaque ipsa.</div>
                </div>
              </div>
            </div>
          </div>
          <div class="flat-adv">
            <div class="flat-adv-a">
              <div class="flat-adv-l">
                <img alt="" src="{{ asset('public/front')}}/img/adv-a-02.png" />
              </div>
              <div class="flat-adv-r">
                <div class="flat-adv-rb">
                  <div class="flat-adv-b">carefully handcrafted</div>
                  <div class="flat-adv-c">Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium doloremque la dantiumeaque ipsa.</div>
                </div>
              </div>
            </div>
          </div>
          <div class="flat-adv">
            <div class="flat-adv-a">
              <div class="flat-adv-l">
                <img alt="" src="{{ asset('public/front')}}/img/adv-a-03.png" />
              </div>
              <div class="flat-adv-r">
                <div class="flat-adv-rb">
                  <div class="flat-adv-b">fully responsive</div>
                  <div class="flat-adv-c">Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium doloremque la dantiumeaque ipsa.</div>
                </div>
              </div>
            </div>
          </div>
          <div class="flat-adv">
            <div class="flat-adv-a">
              <div class="flat-adv-l">
                <img alt="" src="{{ asset('public/front')}}/img/adv-a-04.png" />
              </div>
              <div class="flat-adv-r">
                <div class="flat-adv-rb">
                  <div class="flat-adv-b">customer support</div>
                  <div class="flat-adv-c">Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium doloremque la dantiumeaque ipsa.</div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <div class="clear"></div>
      </div>

    </div>
  </div>


  

  
  <div class="pop-destinations">
    <header class="fly-in page-lbl">
      <b>@lang('fnt.popular')</b>
      <!-- <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p> -->
    </header>
    <div class="mp-popular-row popular-flat">
      @foreach($populars as $popular)
      <!-- // -->
        <div class="fly-in offer-slider-i">
          <a class="offer-slider-img" href="{{ url('tours/?c='.$popular->id)}}">
            <img alt="" src="{{ $popular->image }}" />
            <span class="offer-slider-overlay">
              <span class="offer-slider-btn">@lang('fnt.view')</span>
            </span>
          </a>
          <div class="offer-slider-txt">
            <div class="offer-slider-link"><a href="{{ url('tours/?c='.$popular->id)}}">{{ $popular->translate($share_locale)->title }}</a></div>
            <div class="clear"></div>
          </div>
        </div>
      <!-- \\ -->
      @endforeach
    </div>
    <div class="clear"></div>       
  </div>

    <div class="inform-block" style="padding-top: 60px;">
    <div class="wrapper-padding">
      <header class="fly-in page-lbl">
        <b>@lang('fnt.rec_posts')</b>
        <!-- <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p> -->
      </header>
        <div class="fly-in advantages-row flat">
          @foreach($posts as $post)
          <div class="flat-adv large">
            <div class="flat-adv-a">
              <div class="flat-adv-l">
                <a class="" href="{{ url('post/'.$post->id) }}"><img alt="" src="{{ url($post->image) }}" style="max-width: 115px;border: 2px solid #cecece;" /></a>
              </div>
              <div class="flat-adv-r">
                <div class="flat-adv-rb">
                  <div class="flat-adv-b">{{ $post->translate($share_locale)->title }}</div>
                  <div class="flat-adv-c">{!! \Illuminate\Support\Str::words($post->translate($share_locale)->description, 20, ' ...') !!}</div>
                  <a class="flat-adv-btn" href="{{ url('post/'.$post->id) }}">@lang('fnt.more')</a>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <div class="clear"></div>       
    </div>    
  </div>

</div>
<!-- /main-cont -->
@endsection

@section('javascript')
<script src='{{ asset("public/front")}}/js/slideInit.js'></script>
 @endsection
