@extends('admin.layouts.admin_master')

@section('content')
<ol class="breadcrumb">
    <li>
        <i class="clip-home-3"></i>
        <a href="{{url('admin/dashboard')}}">
            Home
        </a>
    </li>
    <li class="active">
        Reservations
    </li>

</ol>
<div class="page-header">
    <h1 class="col-md-6">Reservations <span style="font-size: 20px;
    color: #007aff;">[ {{ $typ == 'd' ? 'Done' : 'Pending' }} ]</span></h1>
    <!-- <div class="col-md-6">
        <a class="btn btn-success pull-left" href="{{url("admin/works/create")}}"><i class="icon-plus2 mr-2"></i> إضافة أعمال <i class="fa fa-plus"></i></a>
    </div> -->
    <div class="clearfix"></div>
</div>
<!-- end: PAGE TITLE & BREADCRUMB -->
</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <div class="btn-group" style="padding-bottom: 16px;">
            @if($typ == 'p')
            <a class="btn btn-teal active" href="javascript:;">
                Pending Reservations
            </a>
            <a class="btn btn-teal hidden-xs" href="{{ url('admin/reservations/'.$btype.'?typ=d') }}">
                Done Reservations
            </a>
            @else
            <a class="btn btn-teal " href="{{ url('admin/reservations/'.$btype.'?typ=p') }}">
                Pending Reservations
            </a>
            <a class="btn btn-teal active" href="javascript:;">
                Done Reservations
            </a>
            @endif
        </div>

        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-default">

            <div class="panel-body" style="position: relative;">
                <div id="loading" style="position: absolute; top: 0; left: 0; z-index: 111; background-color: #8196ad;width: 100%; height: 100%;  opacity: 0.6; display: none;">
                    <div style="padding-top: 30px;
    text-align: center;
    color: #fff;
    font-size: 16px;
    font-weight: bold;">Please Wait....</div>
                </div>
                <table class="table table-bordered table-full-width" id="sample_1">
                    <thead>
                        <tr>        
                            <th >#</th>    
                            <th >Name</th>
                            <th >Hotel</th>   
                            <th >Date</th>      
                            <th >Title</th>   
                            <!-- <th >Guide</th>   -->
                            <th class="text-right">Total</th>     
                            <th >Payment</th>
                            <th >Payment Status</th>          
                            @if($btype == 'tours' )  
                                <th >Availability</th>  
                            @endif
                            <th class="text-center" style="width: 150px">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations as $reservation)
                        <tr id="record-{{ $reservation->id }}" @if(!$reservation->is_read) style="background-color: #b9cbd4" @endif>                  
                            <td>{{$reservation->id}}</td>   
                            <td> <a href="{{url('admin/reservations/'.$btype.'/'.$reservation->id)}}" class="tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank">{{$reservation->name}}</a></td>      
                            <td> {{$reservation->hotel ?? ''}}</td>       
                            <td> 
                                @if($reservation->booking_type == 'tour' && $reservation->date_to)
                                    From {{ Carbon\Carbon::parse($reservation->date_from)->format('d M Y') }} To {{ Carbon\Carbon::parse($reservation->date_to)->format('d M Y') }}
                                @else
                                    {{ Carbon\Carbon::parse($reservation->date_from)->format('d M Y') }}
                                @endif
                            </td>   
                            <td> 
                                @if($reservation->booking_type == 'tour')
                                <a href="{{url('tour/'.$reservation->product_id)}}" class="tooltips" data-placement="top" data-original-title="View main Tour" target="_blank">{{$reservation->products->translate('en')->title }}</a>
                                @else
                                {{$reservation->transportations->translate('en')->destination }}
                                @endif
                            </td>  
                            <!-- <td> {{$reservation->guide}}</td>        -->
                            <td style="text-align: right;"> {{ number_format($reservation->total_price,2) }}$</td>
                            <td> {{ $reservation->payment_type }}</td>
                            <td style="font-weight: bold;">{!! $reservation->payment_status ? $reservation->payment_status == 2 ? '<span style="color:#ee8a1f">Pending</span>' : '<span style="color:#2bb78b">Success</span>' : '<span style="color:#ff1010">Not Proccessed</span>' !!}</td>     
                            @if($reservation->booking_type == 'tour' )  
                                <td style="text-align: center;">
                                    @if($reservation->products->check_avail)  
                                        <select class="form-control" style="padding: 0 12px" onchange="setAvail('{{ $reservation->id }}', $(this))" {!! $reservation->payment_status ? 'disabled=""' : '' !!}>
                                            <option value="">-- Select --</option>
                                            <option value="1" @if($reservation->is_available) selected="" @endif>Available</option>
                                            <option value="0" @if($reservation->is_available === 0) selected="" @endif>Not Available</option>
                                        </select>
                                    @endif
                                </td>  
                            @endif                        
                            <td class="center">
                                <div class="visible-md visible-lg hidden-sm hidden-xs">
                                    
                                    @if($reservation->order_done == 0)
                                        <a href="javascript://" onclick="markAs('d', '{{ $reservation->id }}')" class="btn btn-xs btn-success tooltips" data-placement="top" data-original-title="Mark as Done"><i class="fa fa-check"></i></a>
                                    @else
                                        <a href="javascript://" onclick="markAs('p', '{{ $reservation->id }}')" class="btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Mark as Pending"><i class="fa fa-undo"></i></a>
                                    @endif

                                    <a href="{{url('admin/reservations/'.$btype.'/'.$reservation->id)}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Reservation Details" target="_blank"><i class="fa fa-eye"></i></a>

                                    {{ Form::open(array('url' => 'admin/reservations/' .$btype.'/'. $reservation->id, 'style' => 'display: inline-block;')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    <button type="submit" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove" onclick="return confirm('are you sure?')" ><i class="fa fa-times fa fa-white"></i></button>
                                    {{ Form::close() }}

                            </div>
                            </td>                          
                        </tr>
                        @endforeach         
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection
@section('page_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{url('public/admin/')}}/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="{{url('public/admin/')}}/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
jQuery(document).ready(function () {
    TableData.init();
});

function markAs(typ, id){
    $("#loading").fadeIn();
    $.ajax({
        type: "POST",
        url: "{{url('admin/updReservation' )}}",
        data: {'typ' : typ, 'id' : id},
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (data) {
            $("#loading").fadeOut();
            if(data.status == 'success')
                $("#record-"+id).remove();
            else
                alert('Action Failed!');
        }
      });    
}

function setAvail(id, slct){
    $("#loading").fadeIn();
    var slctVal = slct.val();

    $.ajax({
        type: "POST",
        url: "{{url('admin/updAvailability' )}}"+"/"+id,
        data: {'avail' : slctVal, 'id' : id},
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (data) {
            $("#loading").fadeOut();
        }
      });    
}
</script>
@endsection