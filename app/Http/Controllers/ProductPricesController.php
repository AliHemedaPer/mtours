<?php

namespace App\Http\Controllers;

use App\ProductPrices;
use Illuminate\Http\Request;

class ProductPricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductPrices  $productPrices
     * @return \Illuminate\Http\Response
     */
    public function show(ProductPrices $productPrices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductPrices  $productPrices
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductPrices $productPrices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductPrices  $productPrices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductPrices $productPrices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductPrices  $productPrices
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductPrices $productPrices)
    {
        //
    }
}
