@extends('front.front')

@section('content')
<div class="main-cont about-page">   
  
  <div class="inner-page">
    <div class="about-content">
      <div class="content-wrapper" style="padding-top: 40px">
        <header class="page-lbl fly-in">
          <div class="offer-slider-lbl">{{ $terms->translate($share_locale)->title }}</div>
          <div style="text-align: left;">
            {!! $terms->translate($share_locale)->content !!}
          </div>          
        </header>     
        
        <div class="clear"></div>
    </div>
</div>
</div>
</div>
@stop