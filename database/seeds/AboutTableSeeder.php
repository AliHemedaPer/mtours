<?php

use Illuminate\Database\Seeder;

class AboutTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('about')->delete();
        
        \DB::table('about')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => '5',
                'short_description' => '6',
                'content' => '7',
                'image' => 'uploads/AboutPage/1549658963.jpg'
            ),
        ));

        \DB::table('about')->insert(array (
            0 => 
            array (
                'id' => 2,
                'title' => '5',
                'short_description' => '6',
                'content' => '7',
                'image' => 'uploads/AboutPage/1549658963.jpg'
            ),
        ));

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'app_name' => 'M-Tours',
                'app_email' => 'support@mtours.com',
                'app_phone' => '0000',
                'app_currency' => '$',
                'app_fees' => 3,
                'image' => ''
            ),
        ));
        
        
    }
}