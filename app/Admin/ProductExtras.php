<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductExtras extends Model
{
	use \igaster\TranslateEloquent\TranslationTrait;

    protected static $translatable = ['title'];
	protected $fillable = ['product_id', 'title',  'price'];
    
	public function products()
    {
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }

    public function orderExtras()
    {
        return $this->hasMany(OrderExtras::class);
    }
}
