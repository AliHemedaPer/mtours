<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
	protected $fillable = ['product_id', 'image'];

    public function products()
    {
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }
}
